// $ANTLR 3.5.2 it.unibo.lpemc2014/FOOL.g 2015-02-13 14:18:05

package it.unibo.lpemc.implementation;
import java.util.ArrayList;
import java.util.HashMap;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "ARROW", "ASS", "BOOL", 
		"CLASS", "CLPAR", "COLON", "COMMA", "COMMENT", "CRPAR", "DIV", "DOT", 
		"ELSE", "EQ", "ERR", "EXTENDS", "FALSE", "FUN", "GR", "GREAT", "ID", "IF", 
		"IN", "INT", "INTEGER", "LE", "LESS", "LET", "LPAR", "MINUS", "MULT", 
		"NEW", "NOT", "NULL", "OR", "PLUS", "PRINT", "RPAR", "SEMIC", "THEN", 
		"TRUE", "VAR", "WHITESP"
	};
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ARROW=5;
	public static final int ASS=6;
	public static final int BOOL=7;
	public static final int CLASS=8;
	public static final int CLPAR=9;
	public static final int COLON=10;
	public static final int COMMA=11;
	public static final int COMMENT=12;
	public static final int CRPAR=13;
	public static final int DIV=14;
	public static final int DOT=15;
	public static final int ELSE=16;
	public static final int EQ=17;
	public static final int ERR=18;
	public static final int EXTENDS=19;
	public static final int FALSE=20;
	public static final int FUN=21;
	public static final int GR=22;
	public static final int GREAT=23;
	public static final int ID=24;
	public static final int IF=25;
	public static final int IN=26;
	public static final int INT=27;
	public static final int INTEGER=28;
	public static final int LE=29;
	public static final int LESS=30;
	public static final int LET=31;
	public static final int LPAR=32;
	public static final int MINUS=33;
	public static final int MULT=34;
	public static final int NEW=35;
	public static final int NOT=36;
	public static final int NULL=37;
	public static final int OR=38;
	public static final int PLUS=39;
	public static final int PRINT=40;
	public static final int RPAR=41;
	public static final int SEMIC=42;
	public static final int THEN=43;
	public static final int TRUE=44;
	public static final int VAR=45;
	public static final int WHITESP=46;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public FOOLParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return FOOLParser.tokenNames; }
	@Override public String getGrammarFileName() { return "it.unibo.lpemc2014/FOOL.g"; }


	private ArrayList<HashMap<String,STentry>>  symTable = new ArrayList<HashMap<String,STentry>>();
	private int nestingLevel = -1;



	// $ANTLR start "prog"
	// it.unibo.lpemc2014/FOOL.g:20:1: prog returns [Node ast] : (e= exp SEMIC | LET c= cllist d= declist IN e= exp SEMIC );
	public final Node prog() throws RecognitionException {
		Node ast = null;


		Node e =null;
		ArrayList<Node> c =null;
		ArrayList<Node> d =null;

		try {
			// it.unibo.lpemc2014/FOOL.g:21:2: (e= exp SEMIC | LET c= cllist d= declist IN e= exp SEMIC )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==FALSE||(LA1_0 >= ID && LA1_0 <= IF)||LA1_0==INTEGER||LA1_0==LPAR||(LA1_0 >= NEW && LA1_0 <= NULL)||LA1_0==PRINT||LA1_0==TRUE) ) {
				alt1=1;
			}
			else if ( (LA1_0==LET) ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// it.unibo.lpemc2014/FOOL.g:21:4: e= exp SEMIC
					{
					pushFollow(FOLLOW_exp_in_prog39);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog41); 
					ast = new ProgNode(e);
					}
					break;
				case 2 :
					// it.unibo.lpemc2014/FOOL.g:22:5: LET c= cllist d= declist IN e= exp SEMIC
					{
					match(input,LET,FOLLOW_LET_in_prog49); 
					 nestingLevel++;
					      HashMap<String,STentry> hm = new HashMap<String,STentry> ();
					      symTable.add(hm);
					    
					pushFollow(FOLLOW_cllist_in_prog63);
					c=cllist();
					state._fsp--;

					pushFollow(FOLLOW_declist_in_prog72);
					d=declist();
					state._fsp--;

					match(input,IN,FOLLOW_IN_in_prog74); 
					pushFollow(FOLLOW_exp_in_prog78);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog80); 
					 symTable.remove(nestingLevel--);
					      ast = new LetInNode(d,e);
					    
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "prog"



	// $ANTLR start "cllist"
	// it.unibo.lpemc2014/FOOL.g:34:1: cllist returns [ArrayList<Node> classList] : ( CLASS className= ID ( EXTENDS extClassName= ID )? LPAR (field= ID COLON fieldType= basic ( COMMA otherField= ID COLON otherFieldType= basic )* )? RPAR CLPAR ( FUN function= ID COLON functionReturnType= basic LPAR (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )? RPAR ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )? expression= exp SEMIC )* CRPAR )* ;
	public final ArrayList<Node> cllist() throws RecognitionException {
		ArrayList<Node> classList = null;


		Token className=null;
		Token extClassName=null;
		Token field=null;
		Token otherField=null;
		Token function=null;
		Token parameter=null;
		Token otherParameter=null;
		Token varId=null;
		Node fieldType =null;
		Node otherFieldType =null;
		Node functionReturnType =null;
		Node parameterType =null;
		Node otherParameterType =null;
		Node varType =null;
		Node varValue =null;
		Node expression =null;

		try {
			// it.unibo.lpemc2014/FOOL.g:35:3: ( ( CLASS className= ID ( EXTENDS extClassName= ID )? LPAR (field= ID COLON fieldType= basic ( COMMA otherField= ID COLON otherFieldType= basic )* )? RPAR CLPAR ( FUN function= ID COLON functionReturnType= basic LPAR (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )? RPAR ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )? expression= exp SEMIC )* CRPAR )* )
			// it.unibo.lpemc2014/FOOL.g:37:3: ( CLASS className= ID ( EXTENDS extClassName= ID )? LPAR (field= ID COLON fieldType= basic ( COMMA otherField= ID COLON otherFieldType= basic )* )? RPAR CLPAR ( FUN function= ID COLON functionReturnType= basic LPAR (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )? RPAR ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )? expression= exp SEMIC )* CRPAR )*
			{
			classList = new ArrayList<Node>(); int classOffset = 1;
			// it.unibo.lpemc2014/FOOL.g:39:3: ( CLASS className= ID ( EXTENDS extClassName= ID )? LPAR (field= ID COLON fieldType= basic ( COMMA otherField= ID COLON otherFieldType= basic )* )? RPAR CLPAR ( FUN function= ID COLON functionReturnType= basic LPAR (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )? RPAR ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )? expression= exp SEMIC )* CRPAR )*
			loop10:
			while (true) {
				int alt10=2;
				int LA10_0 = input.LA(1);
				if ( (LA10_0==CLASS) ) {
					alt10=1;
				}

				switch (alt10) {
				case 1 :
					// it.unibo.lpemc2014/FOOL.g:39:4: CLASS className= ID ( EXTENDS extClassName= ID )? LPAR (field= ID COLON fieldType= basic ( COMMA otherField= ID COLON otherFieldType= basic )* )? RPAR CLPAR ( FUN function= ID COLON functionReturnType= basic LPAR (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )? RPAR ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )? expression= exp SEMIC )* CRPAR
					{
					match(input,CLASS,FOLLOW_CLASS_in_cllist114); 
					className=(Token)match(input,ID,FOLLOW_ID_in_cllist118); 
						classOffset--;
					  		ClassNode classNode = new ClassNode((className!=null?className.getText():null));
					  		classList.add(classNode);

					      HashMap<String,Node> hm = new HashMap<String,Node>();
							  if (hm.put((className!=null?className.getText():null),classNode) != null) {
					        		System.out.println("Class id "+(className!=null?className.getText():null)+" @ line "+(className!=null?className.getLine():0)+" already declared");
					           		System.exit(0);
					      }
					      // ora inserisco un nuovo nesting level per i campi e i metodi interni alla classe
					      nestingLevel++;
					      // creo una nuova hashmap e la inserisco nella lista
					      HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
					      symTable.add(hmn);
					  	
					// it.unibo.lpemc2014/FOOL.g:56:3: ( EXTENDS extClassName= ID )?
					int alt2=2;
					int LA2_0 = input.LA(1);
					if ( (LA2_0==EXTENDS) ) {
						alt2=1;
					}
					switch (alt2) {
						case 1 :
							// it.unibo.lpemc2014/FOOL.g:56:4: EXTENDS extClassName= ID
							{
							match(input,EXTENDS,FOLLOW_EXTENDS_in_cllist131); 
							extClassName=(Token)match(input,ID,FOLLOW_ID_in_cllist135); 
							classNode.setSuperClass((extClassName!=null?extClassName.getText():null));
							}
							break;

					}

					ArrayList<Node> fieldTypes = new ArrayList<Node>();
					match(input,LPAR,FOLLOW_LPAR_in_cllist150); 
					// it.unibo.lpemc2014/FOOL.g:59:8: (field= ID COLON fieldType= basic ( COMMA otherField= ID COLON otherFieldType= basic )* )?
					int alt4=2;
					int LA4_0 = input.LA(1);
					if ( (LA4_0==ID) ) {
						alt4=1;
					}
					switch (alt4) {
						case 1 :
							// it.unibo.lpemc2014/FOOL.g:59:9: field= ID COLON fieldType= basic ( COMMA otherField= ID COLON otherFieldType= basic )*
							{
							field=(Token)match(input,ID,FOLLOW_ID_in_cllist155); 
							match(input,COLON,FOLLOW_COLON_in_cllist157); 
							pushFollow(FOLLOW_basic_in_cllist161);
							fieldType=basic();
							state._fsp--;

							 fieldTypes.add(fieldType);
									  ParNode fieldPar = new ParNode((field!=null?field.getText():null),fieldType);
									  classNode.addField(fieldPar);
									  // inserisco nel nuovo nesting level i campi
									  if (hmn.put((field!=null?field.getText():null),new STentry(fieldPar, fieldType)) != null) {
									  	System.out.println("Field id "+(field!=null?field.getText():null)+" @ line "+(field!=null?field.getLine():0)+" already declared");
							              		System.exit(0);
							      }
							    
							// it.unibo.lpemc2014/FOOL.g:69:4: ( COMMA otherField= ID COLON otherFieldType= basic )*
							loop3:
							while (true) {
								int alt3=2;
								int LA3_0 = input.LA(1);
								if ( (LA3_0==COMMA) ) {
									alt3=1;
								}

								switch (alt3) {
								case 1 :
									// it.unibo.lpemc2014/FOOL.g:69:5: COMMA otherField= ID COLON otherFieldType= basic
									{
									match(input,COMMA,FOLLOW_COMMA_in_cllist172); 
									otherField=(Token)match(input,ID,FOLLOW_ID_in_cllist176); 
									match(input,COLON,FOLLOW_COLON_in_cllist178); 
									pushFollow(FOLLOW_basic_in_cllist182);
									otherFieldType=basic();
									state._fsp--;

									 fieldTypes.add(otherFieldType);
											  	ParNode otherFieldPar = new ParNode((otherField!=null?otherField.getText():null),otherFieldType);
											  	classNode.addField(otherFieldPar);
											  	if (hmn.put((otherField!=null?otherField.getText():null),new STentry(otherFieldPar, otherFieldType)) != null) {
											  		System.out.println("Field id "+(otherField!=null?otherField.getText():null)+" @ line "+(otherField!=null?otherField.getLine():0)+" already declared");
										        System.exit(0);
										        }
										    
									}
									break;

								default :
									break loop3;
								}
							}

							}
							break;

					}

					match(input,RPAR,FOLLOW_RPAR_in_cllist201); 
					match(input,CLPAR,FOLLOW_CLPAR_in_cllist207); 
					int classMemberOffset = -2;
					// it.unibo.lpemc2014/FOOL.g:81:4: ( FUN function= ID COLON functionReturnType= basic LPAR (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )? RPAR ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )? expression= exp SEMIC )*
					loop9:
					while (true) {
						int alt9=2;
						int LA9_0 = input.LA(1);
						if ( (LA9_0==FUN) ) {
							alt9=1;
						}

						switch (alt9) {
						case 1 :
							// it.unibo.lpemc2014/FOOL.g:83:4: FUN function= ID COLON functionReturnType= basic LPAR (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )? RPAR ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )? expression= exp SEMIC
							{
							match(input,FUN,FOLLOW_FUN_in_cllist223); 
							function=(Token)match(input,ID,FOLLOW_ID_in_cllist227); 
							match(input,COLON,FOLLOW_COLON_in_cllist229); 
							pushFollow(FOLLOW_basic_in_cllist233);
							functionReturnType=basic();
							state._fsp--;

								FunNode method = new FunNode((function!=null?function.getText():null),functionReturnType);
							      classNode.addMethod(method);
							      STentry functionEntry = new STentry(method,nestingLevel, classMemberOffset-- + classOffset);
							      if (hmn.put((function!=null?function.getText():null),functionEntry) != null) {
							      	System.out.println("Method id "+(function!=null?function.getText():null)+" @ line "+(function!=null?function.getLine():0)+" already declared");
							      	System.exit(0);
							      }
							      // creo un nuovo nesting level per la funzione
							      nestingLevel++;
							      HashMap<String,STentry> hmnn = new HashMap<String,STentry> ();
							      symTable.add(hmnn);
							   	
							match(input,LPAR,FOLLOW_LPAR_in_cllist247); 
							ArrayList<Node> parTypes = new ArrayList<Node>(); int parOffset=1;
							// it.unibo.lpemc2014/FOOL.g:98:4: (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )?
							int alt6=2;
							int LA6_0 = input.LA(1);
							if ( (LA6_0==ID) ) {
								alt6=1;
							}
							switch (alt6) {
								case 1 :
									// it.unibo.lpemc2014/FOOL.g:98:5: parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )*
									{
									parameter=(Token)match(input,ID,FOLLOW_ID_in_cllist257); 
									match(input,COLON,FOLLOW_COLON_in_cllist259); 
									pushFollow(FOLLOW_type_in_cllist263);
									parameterType=type();
									state._fsp--;

									 parTypes.add(parameterType);
									        ParNode fpar = new ParNode((parameter!=null?parameter.getText():null),parameterType);
									        method.addPar(fpar);
									        if (hmnn.put((parameter!=null?parameter.getText():null),new STentry(fpar,nestingLevel,parameterType,parOffset++)) != null) {
									        	System.out.println("Parameter id "+(parameter!=null?parameter.getText():null)+" @ line "+(parameter!=null?parameter.getLine():0)+" already declared");
									        	System.exit(0);
									        }
									    	
									// it.unibo.lpemc2014/FOOL.g:107:4: ( COMMA otherParameter= ID COLON otherParameterType= type )*
									loop5:
									while (true) {
										int alt5=2;
										int LA5_0 = input.LA(1);
										if ( (LA5_0==COMMA) ) {
											alt5=1;
										}

										switch (alt5) {
										case 1 :
											// it.unibo.lpemc2014/FOOL.g:107:5: COMMA otherParameter= ID COLON otherParameterType= type
											{
											match(input,COMMA,FOLLOW_COMMA_in_cllist275); 
											otherParameter=(Token)match(input,ID,FOLLOW_ID_in_cllist279); 
											match(input,COLON,FOLLOW_COLON_in_cllist281); 
											pushFollow(FOLLOW_type_in_cllist285);
											otherParameterType=type();
											state._fsp--;

											 parTypes.add(otherParameterType);
											        ParNode par = new ParNode((otherParameter!=null?otherParameter.getText():null),otherParameterType);
											        method.addPar(par);
											        if (hmnn.put((otherParameter!=null?otherParameter.getText():null),new STentry(fpar,nestingLevel,otherParameterType,parOffset++)) != null) {
											        	System.out.println("Parameter id "+(otherParameter!=null?otherParameter.getText():null)+" @ line "+(otherParameter!=null?otherParameter.getLine():0)+" already declared");
											        	System.exit(0);
											        }
											    	
											}
											break;

										default :
											break loop5;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_cllist307); 
							functionEntry.setType(new ArrowTypeNode(parTypes, functionReturnType) );
							ArrayList<Node> decList = new ArrayList<Node>(); int varOffset=-2;
							// it.unibo.lpemc2014/FOOL.g:119:4: ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )?
							int alt8=2;
							int LA8_0 = input.LA(1);
							if ( (LA8_0==LET) ) {
								alt8=1;
							}
							switch (alt8) {
								case 1 :
									// it.unibo.lpemc2014/FOOL.g:119:5: LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN
									{
									match(input,LET,FOLLOW_LET_in_cllist320); 
									// it.unibo.lpemc2014/FOOL.g:120:5: ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )*
									loop7:
									while (true) {
										int alt7=2;
										int LA7_0 = input.LA(1);
										if ( (LA7_0==VAR) ) {
											alt7=1;
										}

										switch (alt7) {
										case 1 :
											// it.unibo.lpemc2014/FOOL.g:120:6: VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC
											{
											match(input,VAR,FOLLOW_VAR_in_cllist327); 
											varId=(Token)match(input,ID,FOLLOW_ID_in_cllist331); 
											match(input,COLON,FOLLOW_COLON_in_cllist333); 
											pushFollow(FOLLOW_basic_in_cllist337);
											varType=basic();
											state._fsp--;

											match(input,ASS,FOLLOW_ASS_in_cllist339); 
											pushFollow(FOLLOW_exp_in_cllist343);
											varValue=exp();
											state._fsp--;

											match(input,SEMIC,FOLLOW_SEMIC_in_cllist345); 
											 VarNode v = new VarNode((varId!=null?varId.getText():null),varType,varValue);
												        decList.add(v);
												        if (hmnn.put((varId!=null?varId.getText():null),new STentry(v,nestingLevel,varType,varOffset--)) != null) {
												        	System.out.println("Var id "+(varId!=null?varId.getText():null)+" @ line "+(varId!=null?varId.getLine():0)+" already declared");
												        	System.exit(0);
												        }
												      
											}
											break;

										default :
											break loop7;
										}
									}

									match(input,IN,FOLLOW_IN_in_cllist367); 
									}
									break;

							}

							pushFollow(FOLLOW_exp_in_cllist374);
							expression=exp();
							state._fsp--;

							match(input,SEMIC,FOLLOW_SEMIC_in_cllist376); 
								//chiudere scope
								  		symTable.remove(nestingLevel--);
								  		method.addDecBody(decList,expression);
								  	
							}
							break;

						default :
							break loop9;
						}
					}

					match(input,CRPAR,FOLLOW_CRPAR_in_cllist397); 
					}
					break;

				default :
					break loop10;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return classList;
	}
	// $ANTLR end "cllist"



	// $ANTLR start "declist"
	// it.unibo.lpemc2014/FOOL.g:140:1: declist returns [ArrayList<Node> astlist] : ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp SEMIC )* ;
	public final ArrayList<Node> declist() throws RecognitionException {
		ArrayList<Node> astlist = null;


		Token i=null;
		Token fid=null;
		Token id=null;
		Node t =null;
		Node e =null;
		Node fty =null;
		Node ty =null;
		ArrayList<Node> d =null;

		try {
			// it.unibo.lpemc2014/FOOL.g:141:2: ( ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp SEMIC )* )
			// it.unibo.lpemc2014/FOOL.g:141:4: ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp SEMIC )*
			{
			astlist = new ArrayList<Node>(); int varOffset=-2;
			// it.unibo.lpemc2014/FOOL.g:142:4: ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp SEMIC )*
			loop14:
			while (true) {
				int alt14=3;
				int LA14_0 = input.LA(1);
				if ( (LA14_0==VAR) ) {
					alt14=1;
				}
				else if ( (LA14_0==FUN) ) {
					alt14=2;
				}

				switch (alt14) {
				case 1 :
					// it.unibo.lpemc2014/FOOL.g:142:6: VAR i= ID COLON t= type ASS e= exp SEMIC
					{
					match(input,VAR,FOLLOW_VAR_in_declist426); 
					i=(Token)match(input,ID,FOLLOW_ID_in_declist430); 
					match(input,COLON,FOLLOW_COLON_in_declist432); 
					pushFollow(FOLLOW_type_in_declist436);
					t=type();
					state._fsp--;

					match(input,ASS,FOLLOW_ASS_in_declist438); 
					pushFollow(FOLLOW_exp_in_declist442);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_declist444); 
					 	VarNode v = new VarNode((i!=null?i.getText():null),t,e);
					        	astlist.add(v);
					        	HashMap<String,STentry> hm = symTable.get(nestingLevel);
					        	STentry entry = new STentry(v,nestingLevel,t,varOffset--);

					        	if (hm.put((i!=null?i.getText():null), entry) != null) {
					        		System.out.println("Var id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared");
					         		System.exit(0);
					         	}
					      
					}
					break;
				case 2 :
					// it.unibo.lpemc2014/FOOL.g:153:9: FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp SEMIC
					{
					match(input,FUN,FOLLOW_FUN_in_declist462); 
					i=(Token)match(input,ID,FOLLOW_ID_in_declist466); 
					match(input,COLON,FOLLOW_COLON_in_declist468); 
					pushFollow(FOLLOW_type_in_declist472);
					t=type();
					state._fsp--;

					 	//inserimento di ID nella symtable
					          	FunNode f = new FunNode((i!=null?i.getText():null),t);
					          	astlist.add(f);
					          	HashMap<String,STentry> hm = symTable.get(nestingLevel);
					          	STentry entry = new STentry(f,nestingLevel,varOffset--);
					          	if (hm.put((i!=null?i.getText():null),entry) != null) {
					          		System.out.println("Fun id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared");
					           		System.exit(0);
					           	}
					          	//creare una nuova hashmap per la symTable
					          	nestingLevel++;
					          	HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
					          	symTable.add(hmn);
					        
					match(input,LPAR,FOLLOW_LPAR_in_declist492); 
					ArrayList<Node> parTypes = new ArrayList<Node>(); int parOffset=1;
					// it.unibo.lpemc2014/FOOL.g:169:9: (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )?
					int alt12=2;
					int LA12_0 = input.LA(1);
					if ( (LA12_0==ID) ) {
						alt12=1;
					}
					switch (alt12) {
						case 1 :
							// it.unibo.lpemc2014/FOOL.g:169:11: fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )*
							{
							fid=(Token)match(input,ID,FOLLOW_ID_in_declist508); 
							match(input,COLON,FOLLOW_COLON_in_declist510); 
							pushFollow(FOLLOW_type_in_declist514);
							fty=type();
							state._fsp--;

							 	parTypes.add(fty);
							            	ParNode fpar = new ParNode((fid!=null?fid.getText():null),fty);
							            	f.addPar(fpar);
							            	if (hmn.put((fid!=null?fid.getText():null),new STentry(fpar,nestingLevel,fty,parOffset++)) != null) {
							            		System.out.println("Parameter id "+(fid!=null?fid.getText():null)+" at line "+(fid!=null?fid.getLine():0)+" already declared");
							             		System.exit(0);
							             	}
							          
							// it.unibo.lpemc2014/FOOL.g:178:11: ( COMMA id= ID COLON ty= type )*
							loop11:
							while (true) {
								int alt11=2;
								int LA11_0 = input.LA(1);
								if ( (LA11_0==COMMA) ) {
									alt11=1;
								}

								switch (alt11) {
								case 1 :
									// it.unibo.lpemc2014/FOOL.g:178:13: COMMA id= ID COLON ty= type
									{
									match(input,COMMA,FOLLOW_COMMA_in_declist540); 
									id=(Token)match(input,ID,FOLLOW_ID_in_declist544); 
									match(input,COLON,FOLLOW_COLON_in_declist546); 
									pushFollow(FOLLOW_type_in_declist550);
									ty=type();
									state._fsp--;

									 	parTypes.add(ty);
									              	ParNode par = new ParNode((id!=null?id.getText():null),ty);
									              	f.addPar(par);
									              	if (hmn.put((id!=null?id.getText():null),new STentry(par,nestingLevel,ty,parOffset++)) != null) {
									              		System.out.println("Parameter id "+(id!=null?id.getText():null)+" at line "+(id!=null?id.getLine():0)+" already declared");
									               		System.exit(0);
									               	}
									            
									}
									break;

								default :
									break loop11;
								}
							}

							}
							break;

					}

					match(input,RPAR,FOLLOW_RPAR_in_declist598); 
					entry.setType( new ArrowTypeNode(parTypes , t) );
					ArrayList<Node> decList = new ArrayList<Node>();
					// it.unibo.lpemc2014/FOOL.g:191:9: ( LET d= declist IN )?
					int alt13=2;
					int LA13_0 = input.LA(1);
					if ( (LA13_0==LET) ) {
						alt13=1;
					}
					switch (alt13) {
						case 1 :
							// it.unibo.lpemc2014/FOOL.g:191:10: LET d= declist IN
							{
							match(input,LET,FOLLOW_LET_in_declist628); 
							pushFollow(FOLLOW_declist_in_declist632);
							d=declist();
							state._fsp--;

							match(input,IN,FOLLOW_IN_in_declist634); 
							decList=d;
							}
							break;

					}

					pushFollow(FOLLOW_exp_in_declist642);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_declist644); 
						//chiudere scope
					        		symTable.remove(nestingLevel--);
					        		f.addDecBody(decList,e);
					        	
					}
					break;

				default :
					break loop14;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return astlist;
	}
	// $ANTLR end "declist"



	// $ANTLR start "exp"
	// it.unibo.lpemc2014/FOOL.g:198:1: exp returns [Node ast] : f= term ( PLUS l= term | MINUS l= term | OR l= term )* ;
	public final Node exp() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// it.unibo.lpemc2014/FOOL.g:199:3: (f= term ( PLUS l= term | MINUS l= term | OR l= term )* )
			// it.unibo.lpemc2014/FOOL.g:199:5: f= term ( PLUS l= term | MINUS l= term | OR l= term )*
			{
			pushFollow(FOLLOW_term_in_exp679);
			f=term();
			state._fsp--;

			ast = f;
			// it.unibo.lpemc2014/FOOL.g:200:5: ( PLUS l= term | MINUS l= term | OR l= term )*
			loop15:
			while (true) {
				int alt15=4;
				switch ( input.LA(1) ) {
				case PLUS:
					{
					alt15=1;
					}
					break;
				case MINUS:
					{
					alt15=2;
					}
					break;
				case OR:
					{
					alt15=3;
					}
					break;
				}
				switch (alt15) {
				case 1 :
					// it.unibo.lpemc2014/FOOL.g:200:9: PLUS l= term
					{
					match(input,PLUS,FOLLOW_PLUS_in_exp691); 
					pushFollow(FOLLOW_term_in_exp696);
					l=term();
					state._fsp--;

					ast = new AddNode(ast,l);
					}
					break;
				case 2 :
					// it.unibo.lpemc2014/FOOL.g:201:9: MINUS l= term
					{
					match(input,MINUS,FOLLOW_MINUS_in_exp708); 
					pushFollow(FOLLOW_term_in_exp712);
					l=term();
					state._fsp--;

					ast = new SubNode(ast,l);
					}
					break;
				case 3 :
					// it.unibo.lpemc2014/FOOL.g:202:9: OR l= term
					{
					match(input,OR,FOLLOW_OR_in_exp724); 
					pushFollow(FOLLOW_term_in_exp731);
					l=term();
					state._fsp--;

					ast = new OrNode(ast,l);
					}
					break;

				default :
					break loop15;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "exp"



	// $ANTLR start "term"
	// it.unibo.lpemc2014/FOOL.g:206:1: term returns [Node ast] : f= factor ( MULT l= factor | DIV l= factor | AND l= factor )* ;
	public final Node term() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// it.unibo.lpemc2014/FOOL.g:207:2: (f= factor ( MULT l= factor | DIV l= factor | AND l= factor )* )
			// it.unibo.lpemc2014/FOOL.g:207:4: f= factor ( MULT l= factor | DIV l= factor | AND l= factor )*
			{
			pushFollow(FOLLOW_factor_in_term758);
			f=factor();
			state._fsp--;

			ast = f;
			// it.unibo.lpemc2014/FOOL.g:208:4: ( MULT l= factor | DIV l= factor | AND l= factor )*
			loop16:
			while (true) {
				int alt16=4;
				switch ( input.LA(1) ) {
				case MULT:
					{
					alt16=1;
					}
					break;
				case DIV:
					{
					alt16=2;
					}
					break;
				case AND:
					{
					alt16=3;
					}
					break;
				}
				switch (alt16) {
				case 1 :
					// it.unibo.lpemc2014/FOOL.g:208:8: MULT l= factor
					{
					match(input,MULT,FOLLOW_MULT_in_term769); 
					pushFollow(FOLLOW_factor_in_term773);
					l=factor();
					state._fsp--;

					ast = new MultNode(ast,l);
					}
					break;
				case 2 :
					// it.unibo.lpemc2014/FOOL.g:209:9: DIV l= factor
					{
					match(input,DIV,FOLLOW_DIV_in_term785); 
					pushFollow(FOLLOW_factor_in_term790);
					l=factor();
					state._fsp--;

					ast = new DivNode(ast,l);
					}
					break;
				case 3 :
					// it.unibo.lpemc2014/FOOL.g:210:8: AND l= factor
					{
					match(input,AND,FOLLOW_AND_in_term801); 
					pushFollow(FOLLOW_factor_in_term806);
					l=factor();
					state._fsp--;

					ast = new AndNode(ast,l);
					}
					break;

				default :
					break loop16;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "term"



	// $ANTLR start "factor"
	// it.unibo.lpemc2014/FOOL.g:214:1: factor returns [Node ast] : f= value ( EQ l= value | GR l= value | LE l= value | GREAT l= value | LESS l= value )* ;
	public final Node factor() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// it.unibo.lpemc2014/FOOL.g:215:2: (f= value ( EQ l= value | GR l= value | LE l= value | GREAT l= value | LESS l= value )* )
			// it.unibo.lpemc2014/FOOL.g:215:4: f= value ( EQ l= value | GR l= value | LE l= value | GREAT l= value | LESS l= value )*
			{
			pushFollow(FOLLOW_value_in_factor831);
			f=value();
			state._fsp--;

			ast = f;
			// it.unibo.lpemc2014/FOOL.g:216:4: ( EQ l= value | GR l= value | LE l= value | GREAT l= value | LESS l= value )*
			loop17:
			while (true) {
				int alt17=6;
				switch ( input.LA(1) ) {
				case EQ:
					{
					alt17=1;
					}
					break;
				case GR:
					{
					alt17=2;
					}
					break;
				case LE:
					{
					alt17=3;
					}
					break;
				case GREAT:
					{
					alt17=4;
					}
					break;
				case LESS:
					{
					alt17=5;
					}
					break;
				}
				switch (alt17) {
				case 1 :
					// it.unibo.lpemc2014/FOOL.g:216:8: EQ l= value
					{
					match(input,EQ,FOLLOW_EQ_in_factor842); 
					pushFollow(FOLLOW_value_in_factor846);
					l=value();
					state._fsp--;

					ast = new EqualNode (ast,l);
					}
					break;
				case 2 :
					// it.unibo.lpemc2014/FOOL.g:217:8: GR l= value
					{
					match(input,GR,FOLLOW_GR_in_factor857); 
					pushFollow(FOLLOW_value_in_factor861);
					l=value();
					state._fsp--;

					ast = new GreaterEqualNode (ast,l);
					}
					break;
				case 3 :
					// it.unibo.lpemc2014/FOOL.g:218:8: LE l= value
					{
					match(input,LE,FOLLOW_LE_in_factor872); 
					pushFollow(FOLLOW_value_in_factor876);
					l=value();
					state._fsp--;

					ast = new LessEqualNode (ast,l);
					}
					break;
				case 4 :
					// it.unibo.lpemc2014/FOOL.g:219:9: GREAT l= value
					{
					match(input,GREAT,FOLLOW_GREAT_in_factor888); 
					pushFollow(FOLLOW_value_in_factor892);
					l=value();
					state._fsp--;

					ast = new GreatNode (ast,l);
					}
					break;
				case 5 :
					// it.unibo.lpemc2014/FOOL.g:220:9: LESS l= value
					{
					match(input,LESS,FOLLOW_LESS_in_factor904); 
					pushFollow(FOLLOW_value_in_factor908);
					l=value();
					state._fsp--;

					ast = new LessNode (ast,l);
					}
					break;

				default :
					break loop17;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "factor"



	// $ANTLR start "value"
	// it.unibo.lpemc2014/FOOL.g:224:1: value returns [Node ast] : (n= INTEGER | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | PRINT LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT identifier= ID ( LPAR (firstArgument= exp ( COMMA otherArgument= exp )* )? RPAR )? )? | NULL | NEW className= ID LPAR (paramater= exp ( COMMA otherParameter= exp )* )? RPAR | NOT LPAR e= exp RPAR );
	public final Node value() throws RecognitionException {
		Node ast = null;


		Token n=null;
		Token i=null;
		Token identifier=null;
		Token className=null;
		Node e =null;
		Node x =null;
		Node y =null;
		Node z =null;
		Node fa =null;
		Node a =null;
		Node firstArgument =null;
		Node otherArgument =null;
		Node paramater =null;
		Node otherParameter =null;

		try {
			// it.unibo.lpemc2014/FOOL.g:225:2: (n= INTEGER | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | PRINT LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT identifier= ID ( LPAR (firstArgument= exp ( COMMA otherArgument= exp )* )? RPAR )? )? | NULL | NEW className= ID LPAR (paramater= exp ( COMMA otherParameter= exp )* )? RPAR | NOT LPAR e= exp RPAR )
			int alt26=10;
			switch ( input.LA(1) ) {
			case INTEGER:
				{
				alt26=1;
				}
				break;
			case TRUE:
				{
				alt26=2;
				}
				break;
			case FALSE:
				{
				alt26=3;
				}
				break;
			case LPAR:
				{
				alt26=4;
				}
				break;
			case IF:
				{
				alt26=5;
				}
				break;
			case PRINT:
				{
				alt26=6;
				}
				break;
			case ID:
				{
				alt26=7;
				}
				break;
			case NULL:
				{
				alt26=8;
				}
				break;
			case NEW:
				{
				alt26=9;
				}
				break;
			case NOT:
				{
				alt26=10;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 26, 0, input);
				throw nvae;
			}
			switch (alt26) {
				case 1 :
					// it.unibo.lpemc2014/FOOL.g:225:4: n= INTEGER
					{
					n=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_value935); 
					ast = new NatNode(Integer.parseInt((n!=null?n.getText():null)));
					}
					break;
				case 2 :
					// it.unibo.lpemc2014/FOOL.g:227:4: TRUE
					{
					match(input,TRUE,FOLLOW_TRUE_in_value945); 
					ast = new BoolNode(true);
					}
					break;
				case 3 :
					// it.unibo.lpemc2014/FOOL.g:229:4: FALSE
					{
					match(input,FALSE,FOLLOW_FALSE_in_value955); 
					ast = new BoolNode(false);
					}
					break;
				case 4 :
					// it.unibo.lpemc2014/FOOL.g:231:4: LPAR e= exp RPAR
					{
					match(input,LPAR,FOLLOW_LPAR_in_value965); 
					pushFollow(FOLLOW_exp_in_value969);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value971); 
					ast = e;
					}
					break;
				case 5 :
					// it.unibo.lpemc2014/FOOL.g:233:4: IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR
					{
					match(input,IF,FOLLOW_IF_in_value981); 
					pushFollow(FOLLOW_exp_in_value985);
					x=exp();
					state._fsp--;

					match(input,THEN,FOLLOW_THEN_in_value987); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value989); 
					pushFollow(FOLLOW_exp_in_value993);
					y=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value995); 
					match(input,ELSE,FOLLOW_ELSE_in_value1002); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value1004); 
					pushFollow(FOLLOW_exp_in_value1008);
					z=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value1010); 
					ast = new IfNode(x,y,z);
					}
					break;
				case 6 :
					// it.unibo.lpemc2014/FOOL.g:236:4: PRINT LPAR e= exp RPAR
					{
					match(input,PRINT,FOLLOW_PRINT_in_value1020); 
					match(input,LPAR,FOLLOW_LPAR_in_value1022); 
					pushFollow(FOLLOW_exp_in_value1026);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value1028); 
					ast = new PrintNode(e);
					}
					break;
				case 7 :
					// it.unibo.lpemc2014/FOOL.g:238:4: i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT identifier= ID ( LPAR (firstArgument= exp ( COMMA otherArgument= exp )* )? RPAR )? )?
					{
					i=(Token)match(input,ID,FOLLOW_ID_in_value1040); 
					 //cercare la dichiarazione
							  int j = nestingLevel;
							  STentry entry = null;
							  while (j>=0 && entry == null)
							  	entry = symTable.get(j--).get((i!=null?i.getText():null));
							  if (entry == null) {
							  	System.out.println("Id "+(i!=null?i.getText():null)+" @ line "+(i!=null?i.getLine():0)+" not declared");
							  	System.exit(0);
							  }
								ast = new IdNode((i!=null?i.getText():null),entry,nestingLevel-j-1);
						  
					// it.unibo.lpemc2014/FOOL.g:250:4: ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT identifier= ID ( LPAR (firstArgument= exp ( COMMA otherArgument= exp )* )? RPAR )? )?
					int alt23=3;
					int LA23_0 = input.LA(1);
					if ( (LA23_0==LPAR) ) {
						alt23=1;
					}
					else if ( (LA23_0==DOT) ) {
						alt23=2;
					}
					switch (alt23) {
						case 1 :
							// it.unibo.lpemc2014/FOOL.g:250:5: LPAR (fa= exp ( COMMA a= exp )* )? RPAR
							{
							match(input,LPAR,FOLLOW_LPAR_in_value1050); 
							ArrayList<Node> argList = new ArrayList<Node>();
							// it.unibo.lpemc2014/FOOL.g:251:5: (fa= exp ( COMMA a= exp )* )?
							int alt19=2;
							int LA19_0 = input.LA(1);
							if ( (LA19_0==FALSE||(LA19_0 >= ID && LA19_0 <= IF)||LA19_0==INTEGER||LA19_0==LPAR||(LA19_0 >= NEW && LA19_0 <= NULL)||LA19_0==PRINT||LA19_0==TRUE) ) {
								alt19=1;
							}
							switch (alt19) {
								case 1 :
									// it.unibo.lpemc2014/FOOL.g:251:6: fa= exp ( COMMA a= exp )*
									{
									pushFollow(FOLLOW_exp_in_value1061);
									fa=exp();
									state._fsp--;

									argList.add(fa);
									// it.unibo.lpemc2014/FOOL.g:251:37: ( COMMA a= exp )*
									loop18:
									while (true) {
										int alt18=2;
										int LA18_0 = input.LA(1);
										if ( (LA18_0==COMMA) ) {
											alt18=1;
										}

										switch (alt18) {
										case 1 :
											// it.unibo.lpemc2014/FOOL.g:251:38: COMMA a= exp
											{
											match(input,COMMA,FOLLOW_COMMA_in_value1066); 
											pushFollow(FOLLOW_exp_in_value1070);
											a=exp();
											state._fsp--;

											argList.add(a);
											}
											break;

										default :
											break loop18;
										}
									}

									}
									break;

							}

							ast = new CallNode((i!=null?i.getText():null),entry,argList,nestingLevel-j-1);
							match(input,RPAR,FOLLOW_RPAR_in_value1092); 
							}
							break;
						case 2 :
							// it.unibo.lpemc2014/FOOL.g:254:9: DOT identifier= ID ( LPAR (firstArgument= exp ( COMMA otherArgument= exp )* )? RPAR )?
							{
							match(input,DOT,FOLLOW_DOT_in_value1102); 
							identifier=(Token)match(input,ID,FOLLOW_ID_in_value1106); 

										        ast = ((ClassNode) entry.getNode().typeCheck().typeCheck()).getField((identifier!=null?identifier.getText():null), entry.getOffset());
										    
							// it.unibo.lpemc2014/FOOL.g:258:8: ( LPAR (firstArgument= exp ( COMMA otherArgument= exp )* )? RPAR )?
							int alt22=2;
							int LA22_0 = input.LA(1);
							if ( (LA22_0==LPAR) ) {
								alt22=1;
							}
							switch (alt22) {
								case 1 :
									// it.unibo.lpemc2014/FOOL.g:258:9: LPAR (firstArgument= exp ( COMMA otherArgument= exp )* )? RPAR
									{
									match(input,LPAR,FOLLOW_LPAR_in_value1125); 
									ArrayList<Node> argList = new ArrayList<Node>();
									                  argList.add(new IdNode((i!=null?i.getText():null),entry,nestingLevel-j-1); 
									// it.unibo.lpemc2014/FOOL.g:260:10: (firstArgument= exp ( COMMA otherArgument= exp )* )?
									int alt21=2;
									int LA21_0 = input.LA(1);
									if ( (LA21_0==FALSE||(LA21_0 >= ID && LA21_0 <= IF)||LA21_0==INTEGER||LA21_0==LPAR||(LA21_0 >= NEW && LA21_0 <= NULL)||LA21_0==PRINT||LA21_0==TRUE) ) {
										alt21=1;
									}
									switch (alt21) {
										case 1 :
											// it.unibo.lpemc2014/FOOL.g:260:11: firstArgument= exp ( COMMA otherArgument= exp )*
											{
											pushFollow(FOLLOW_exp_in_value1141);
											firstArgument=exp();
											state._fsp--;

											argList.add(firstArgument);
											// it.unibo.lpemc2014/FOOL.g:260:64: ( COMMA otherArgument= exp )*
											loop20:
											while (true) {
												int alt20=2;
												int LA20_0 = input.LA(1);
												if ( (LA20_0==COMMA) ) {
													alt20=1;
												}

												switch (alt20) {
												case 1 :
													// it.unibo.lpemc2014/FOOL.g:260:65: COMMA otherArgument= exp
													{
													match(input,COMMA,FOLLOW_COMMA_in_value1146); 
													pushFollow(FOLLOW_exp_in_value1150);
													otherArgument=exp();
													state._fsp--;

													argList.add(otherArgument);
													}
													break;

												default :
													break loop20;
												}
											}

											}
											break;

									}

									 ast = ((ClassNode) entry.getNode().typeCheck().typeCheck()).callMethod((identifier!=null?identifier.getText():null), entry, argList, nestingLevel-j-1); 
									match(input,RPAR,FOLLOW_RPAR_in_value1179); 
									}
									break;

							}


										   	  if (ast == null) {
							      		    System.out.println((i!=null?i.getText():null)+"."+(identifier!=null?identifier.getText():null)+" @ line "+(identifier!=null?identifier.getLine():0)+", "+(identifier!=null?identifier.getText():null)+" not declared");
							       		    System.exit(0);
							      		  }
										    
							}
							break;

					}

					}
					break;
				case 8 :
					// it.unibo.lpemc2014/FOOL.g:272:6: NULL
					{
					match(input,NULL,FOLLOW_NULL_in_value1216); 
					ast =new NullNode();
					}
					break;
				case 9 :
					// it.unibo.lpemc2014/FOOL.g:273:5: NEW className= ID LPAR (paramater= exp ( COMMA otherParameter= exp )* )? RPAR
					{
					ArrayList<Node> argList = new ArrayList<Node>();
					match(input,NEW,FOLLOW_NEW_in_value1228); 
					className=(Token)match(input,ID,FOLLOW_ID_in_value1232); 
					match(input,LPAR,FOLLOW_LPAR_in_value1234); 
					// it.unibo.lpemc2014/FOOL.g:274:25: (paramater= exp ( COMMA otherParameter= exp )* )?
					int alt25=2;
					int LA25_0 = input.LA(1);
					if ( (LA25_0==FALSE||(LA25_0 >= ID && LA25_0 <= IF)||LA25_0==INTEGER||LA25_0==LPAR||(LA25_0 >= NEW && LA25_0 <= NULL)||LA25_0==PRINT||LA25_0==TRUE) ) {
						alt25=1;
					}
					switch (alt25) {
						case 1 :
							// it.unibo.lpemc2014/FOOL.g:274:26: paramater= exp ( COMMA otherParameter= exp )*
							{
							pushFollow(FOLLOW_exp_in_value1239);
							paramater=exp();
							state._fsp--;

							argList.add(paramater);
							// it.unibo.lpemc2014/FOOL.g:275:3: ( COMMA otherParameter= exp )*
							loop24:
							while (true) {
								int alt24=2;
								int LA24_0 = input.LA(1);
								if ( (LA24_0==COMMA) ) {
									alt24=1;
								}

								switch (alt24) {
								case 1 :
									// it.unibo.lpemc2014/FOOL.g:275:4: COMMA otherParameter= exp
									{
									match(input,COMMA,FOLLOW_COMMA_in_value1246); 
									pushFollow(FOLLOW_exp_in_value1250);
									otherParameter=exp();
									state._fsp--;

									argList.add(otherParameter);
									}
									break;

								default :
									break loop24;
								}
							}

							}
							break;

					}

					ast =new ObjectNode((className!=null?className.getText():null),argList);
					match(input,RPAR,FOLLOW_RPAR_in_value1263); 
					}
					break;
				case 10 :
					// it.unibo.lpemc2014/FOOL.g:278:4: NOT LPAR e= exp RPAR
					{
					match(input,NOT,FOLLOW_NOT_in_value1270); 
					match(input,LPAR,FOLLOW_LPAR_in_value1272); 
					pushFollow(FOLLOW_exp_in_value1276);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value1278); 
					ast = new NotNode(e);
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "value"



	// $ANTLR start "type"
	// it.unibo.lpemc2014/FOOL.g:282:1: type returns [Node ast] : (typ= basic |arw= arrow );
	public final Node type() throws RecognitionException {
		Node ast = null;


		Node typ =null;
		Node arw =null;

		try {
			// it.unibo.lpemc2014/FOOL.g:283:3: (typ= basic |arw= arrow )
			int alt27=2;
			int LA27_0 = input.LA(1);
			if ( (LA27_0==BOOL||LA27_0==ID||LA27_0==INT) ) {
				alt27=1;
			}
			else if ( (LA27_0==LPAR) ) {
				alt27=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 27, 0, input);
				throw nvae;
			}

			switch (alt27) {
				case 1 :
					// it.unibo.lpemc2014/FOOL.g:283:5: typ= basic
					{
					pushFollow(FOLLOW_basic_in_type1300);
					typ=basic();
					state._fsp--;

					ast =typ;
					}
					break;
				case 2 :
					// it.unibo.lpemc2014/FOOL.g:284:5: arw= arrow
					{
					pushFollow(FOLLOW_arrow_in_type1310);
					arw=arrow();
					state._fsp--;

					ast =arw;
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "type"



	// $ANTLR start "basic"
	// it.unibo.lpemc2014/FOOL.g:287:1: basic returns [Node ast] : ( INT | BOOL |i= ID );
	public final Node basic() throws RecognitionException {
		Node ast = null;


		Token i=null;

		try {
			// it.unibo.lpemc2014/FOOL.g:288:3: ( INT | BOOL |i= ID )
			int alt28=3;
			switch ( input.LA(1) ) {
			case INT:
				{
				alt28=1;
				}
				break;
			case BOOL:
				{
				alt28=2;
				}
				break;
			case ID:
				{
				alt28=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 28, 0, input);
				throw nvae;
			}
			switch (alt28) {
				case 1 :
					// it.unibo.lpemc2014/FOOL.g:288:5: INT
					{
					match(input,INT,FOLLOW_INT_in_basic1329); 
					ast =new IntTypeNode();
					}
					break;
				case 2 :
					// it.unibo.lpemc2014/FOOL.g:289:5: BOOL
					{
					match(input,BOOL,FOLLOW_BOOL_in_basic1338); 
					ast =new BoolTypeNode();
					}
					break;
				case 3 :
					// it.unibo.lpemc2014/FOOL.g:290:5: i= ID
					{
					i=(Token)match(input,ID,FOLLOW_ID_in_basic1348); 
					ast =new IdType((i!=null?i.getText():null));
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "basic"



	// $ANTLR start "arrow"
	// it.unibo.lpemc2014/FOOL.g:293:1: arrow returns [Node ast] : LPAR (fty= type ( COMMA ty= type )* )? RPAR ARROW returnType= basic ;
	public final Node arrow() throws RecognitionException {
		Node ast = null;


		Node fty =null;
		Node ty =null;
		Node returnType =null;

		try {
			// it.unibo.lpemc2014/FOOL.g:294:3: ( LPAR (fty= type ( COMMA ty= type )* )? RPAR ARROW returnType= basic )
			// it.unibo.lpemc2014/FOOL.g:294:5: LPAR (fty= type ( COMMA ty= type )* )? RPAR ARROW returnType= basic
			{
			match(input,LPAR,FOLLOW_LPAR_in_arrow1367); 
			 ArrayList<Node> parTypes = new ArrayList<Node>();
			           int parOffset=1;
			         
			// it.unibo.lpemc2014/FOOL.g:297:9: (fty= type ( COMMA ty= type )* )?
			int alt30=2;
			int LA30_0 = input.LA(1);
			if ( (LA30_0==BOOL||LA30_0==ID||LA30_0==INT||LA30_0==LPAR) ) {
				alt30=1;
			}
			switch (alt30) {
				case 1 :
					// it.unibo.lpemc2014/FOOL.g:297:11: fty= type ( COMMA ty= type )*
					{
					pushFollow(FOLLOW_type_in_arrow1383);
					fty=type();
					state._fsp--;


					            parTypes.add(fty);
					          
					// it.unibo.lpemc2014/FOOL.g:301:11: ( COMMA ty= type )*
					loop29:
					while (true) {
						int alt29=2;
						int LA29_0 = input.LA(1);
						if ( (LA29_0==COMMA) ) {
							alt29=1;
						}

						switch (alt29) {
						case 1 :
							// it.unibo.lpemc2014/FOOL.g:301:12: COMMA ty= type
							{
							match(input,COMMA,FOLLOW_COMMA_in_arrow1408); 
							pushFollow(FOLLOW_type_in_arrow1412);
							ty=type();
							state._fsp--;


							              parTypes.add(ty);
							            
							}
							break;

						default :
							break loop29;
						}
					}

					}
					break;

			}

			match(input,RPAR,FOLLOW_RPAR_in_arrow1452); 
			match(input,ARROW,FOLLOW_ARROW_in_arrow1454); 
			pushFollow(FOLLOW_basic_in_arrow1458);
			returnType=basic();
			state._fsp--;


			            ast = new ArrowTypeNode(parTypes , returnType);
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "arrow"

	// Delegated rules



	public static final BitSet FOLLOW_exp_in_prog39 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog41 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LET_in_prog49 = new BitSet(new long[]{0x0000200004200100L});
	public static final BitSet FOLLOW_cllist_in_prog63 = new BitSet(new long[]{0x0000200004200000L});
	public static final BitSet FOLLOW_declist_in_prog72 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_IN_in_prog74 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_exp_in_prog78 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog80 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CLASS_in_cllist114 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_ID_in_cllist118 = new BitSet(new long[]{0x0000000100080000L});
	public static final BitSet FOLLOW_EXTENDS_in_cllist131 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_ID_in_cllist135 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_LPAR_in_cllist150 = new BitSet(new long[]{0x0000020001000000L});
	public static final BitSet FOLLOW_ID_in_cllist155 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist157 = new BitSet(new long[]{0x0000000009000080L});
	public static final BitSet FOLLOW_basic_in_cllist161 = new BitSet(new long[]{0x0000020000000800L});
	public static final BitSet FOLLOW_COMMA_in_cllist172 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_ID_in_cllist176 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist178 = new BitSet(new long[]{0x0000000009000080L});
	public static final BitSet FOLLOW_basic_in_cllist182 = new BitSet(new long[]{0x0000020000000800L});
	public static final BitSet FOLLOW_RPAR_in_cllist201 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_cllist207 = new BitSet(new long[]{0x0000000000202000L});
	public static final BitSet FOLLOW_FUN_in_cllist223 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_ID_in_cllist227 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist229 = new BitSet(new long[]{0x0000000009000080L});
	public static final BitSet FOLLOW_basic_in_cllist233 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_LPAR_in_cllist247 = new BitSet(new long[]{0x0000020001000000L});
	public static final BitSet FOLLOW_ID_in_cllist257 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist259 = new BitSet(new long[]{0x0000000109000080L});
	public static final BitSet FOLLOW_type_in_cllist263 = new BitSet(new long[]{0x0000020000000800L});
	public static final BitSet FOLLOW_COMMA_in_cllist275 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_ID_in_cllist279 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist281 = new BitSet(new long[]{0x0000000109000080L});
	public static final BitSet FOLLOW_type_in_cllist285 = new BitSet(new long[]{0x0000020000000800L});
	public static final BitSet FOLLOW_RPAR_in_cllist307 = new BitSet(new long[]{0x0000113993100000L});
	public static final BitSet FOLLOW_LET_in_cllist320 = new BitSet(new long[]{0x0000200004000000L});
	public static final BitSet FOLLOW_VAR_in_cllist327 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_ID_in_cllist331 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist333 = new BitSet(new long[]{0x0000000009000080L});
	public static final BitSet FOLLOW_basic_in_cllist337 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASS_in_cllist339 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_exp_in_cllist343 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_SEMIC_in_cllist345 = new BitSet(new long[]{0x0000200004000000L});
	public static final BitSet FOLLOW_IN_in_cllist367 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_exp_in_cllist374 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_SEMIC_in_cllist376 = new BitSet(new long[]{0x0000000000202000L});
	public static final BitSet FOLLOW_CRPAR_in_cllist397 = new BitSet(new long[]{0x0000000000000102L});
	public static final BitSet FOLLOW_VAR_in_declist426 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_ID_in_declist430 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist432 = new BitSet(new long[]{0x0000000109000080L});
	public static final BitSet FOLLOW_type_in_declist436 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASS_in_declist438 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_exp_in_declist442 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_SEMIC_in_declist444 = new BitSet(new long[]{0x0000200000200002L});
	public static final BitSet FOLLOW_FUN_in_declist462 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_ID_in_declist466 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist468 = new BitSet(new long[]{0x0000000109000080L});
	public static final BitSet FOLLOW_type_in_declist472 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_LPAR_in_declist492 = new BitSet(new long[]{0x0000020001000000L});
	public static final BitSet FOLLOW_ID_in_declist508 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist510 = new BitSet(new long[]{0x0000000109000080L});
	public static final BitSet FOLLOW_type_in_declist514 = new BitSet(new long[]{0x0000020000000800L});
	public static final BitSet FOLLOW_COMMA_in_declist540 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_ID_in_declist544 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist546 = new BitSet(new long[]{0x0000000109000080L});
	public static final BitSet FOLLOW_type_in_declist550 = new BitSet(new long[]{0x0000020000000800L});
	public static final BitSet FOLLOW_RPAR_in_declist598 = new BitSet(new long[]{0x0000113993100000L});
	public static final BitSet FOLLOW_LET_in_declist628 = new BitSet(new long[]{0x0000200004200000L});
	public static final BitSet FOLLOW_declist_in_declist632 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_IN_in_declist634 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_exp_in_declist642 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_SEMIC_in_declist644 = new BitSet(new long[]{0x0000200000200002L});
	public static final BitSet FOLLOW_term_in_exp679 = new BitSet(new long[]{0x000000C200000002L});
	public static final BitSet FOLLOW_PLUS_in_exp691 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_term_in_exp696 = new BitSet(new long[]{0x000000C200000002L});
	public static final BitSet FOLLOW_MINUS_in_exp708 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_term_in_exp712 = new BitSet(new long[]{0x000000C200000002L});
	public static final BitSet FOLLOW_OR_in_exp724 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_term_in_exp731 = new BitSet(new long[]{0x000000C200000002L});
	public static final BitSet FOLLOW_factor_in_term758 = new BitSet(new long[]{0x0000000400004012L});
	public static final BitSet FOLLOW_MULT_in_term769 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_factor_in_term773 = new BitSet(new long[]{0x0000000400004012L});
	public static final BitSet FOLLOW_DIV_in_term785 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_factor_in_term790 = new BitSet(new long[]{0x0000000400004012L});
	public static final BitSet FOLLOW_AND_in_term801 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_factor_in_term806 = new BitSet(new long[]{0x0000000400004012L});
	public static final BitSet FOLLOW_value_in_factor831 = new BitSet(new long[]{0x0000000060C20002L});
	public static final BitSet FOLLOW_EQ_in_factor842 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_value_in_factor846 = new BitSet(new long[]{0x0000000060C20002L});
	public static final BitSet FOLLOW_GR_in_factor857 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_value_in_factor861 = new BitSet(new long[]{0x0000000060C20002L});
	public static final BitSet FOLLOW_LE_in_factor872 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_value_in_factor876 = new BitSet(new long[]{0x0000000060C20002L});
	public static final BitSet FOLLOW_GREAT_in_factor888 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_value_in_factor892 = new BitSet(new long[]{0x0000000060C20002L});
	public static final BitSet FOLLOW_LESS_in_factor904 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_value_in_factor908 = new BitSet(new long[]{0x0000000060C20002L});
	public static final BitSet FOLLOW_INTEGER_in_value935 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TRUE_in_value945 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FALSE_in_value955 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_value965 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_exp_in_value969 = new BitSet(new long[]{0x0000020000000000L});
	public static final BitSet FOLLOW_RPAR_in_value971 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_value981 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_exp_in_value985 = new BitSet(new long[]{0x0000080000000000L});
	public static final BitSet FOLLOW_THEN_in_value987 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_value989 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_exp_in_value993 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_CRPAR_in_value995 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ELSE_in_value1002 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_value1004 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_exp_in_value1008 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_CRPAR_in_value1010 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PRINT_in_value1020 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_LPAR_in_value1022 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_exp_in_value1026 = new BitSet(new long[]{0x0000020000000000L});
	public static final BitSet FOLLOW_RPAR_in_value1028 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_value1040 = new BitSet(new long[]{0x0000000100008002L});
	public static final BitSet FOLLOW_LPAR_in_value1050 = new BitSet(new long[]{0x0000133913100000L});
	public static final BitSet FOLLOW_exp_in_value1061 = new BitSet(new long[]{0x0000020000000800L});
	public static final BitSet FOLLOW_COMMA_in_value1066 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_exp_in_value1070 = new BitSet(new long[]{0x0000020000000800L});
	public static final BitSet FOLLOW_RPAR_in_value1092 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOT_in_value1102 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_ID_in_value1106 = new BitSet(new long[]{0x0000000100000002L});
	public static final BitSet FOLLOW_LPAR_in_value1125 = new BitSet(new long[]{0x0000133913100000L});
	public static final BitSet FOLLOW_exp_in_value1141 = new BitSet(new long[]{0x0000020000000800L});
	public static final BitSet FOLLOW_COMMA_in_value1146 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_exp_in_value1150 = new BitSet(new long[]{0x0000020000000800L});
	public static final BitSet FOLLOW_RPAR_in_value1179 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NULL_in_value1216 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NEW_in_value1228 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_ID_in_value1232 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_LPAR_in_value1234 = new BitSet(new long[]{0x0000133913100000L});
	public static final BitSet FOLLOW_exp_in_value1239 = new BitSet(new long[]{0x0000020000000800L});
	public static final BitSet FOLLOW_COMMA_in_value1246 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_exp_in_value1250 = new BitSet(new long[]{0x0000020000000800L});
	public static final BitSet FOLLOW_RPAR_in_value1263 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOT_in_value1270 = new BitSet(new long[]{0x0000000100000000L});
	public static final BitSet FOLLOW_LPAR_in_value1272 = new BitSet(new long[]{0x0000113913100000L});
	public static final BitSet FOLLOW_exp_in_value1276 = new BitSet(new long[]{0x0000020000000000L});
	public static final BitSet FOLLOW_RPAR_in_value1278 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_basic_in_type1300 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arrow_in_type1310 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_INT_in_basic1329 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BOOL_in_basic1338 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_basic1348 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_arrow1367 = new BitSet(new long[]{0x0000020109000080L});
	public static final BitSet FOLLOW_type_in_arrow1383 = new BitSet(new long[]{0x0000020000000800L});
	public static final BitSet FOLLOW_COMMA_in_arrow1408 = new BitSet(new long[]{0x0000000109000080L});
	public static final BitSet FOLLOW_type_in_arrow1412 = new BitSet(new long[]{0x0000020000000800L});
	public static final BitSet FOLLOW_RPAR_in_arrow1452 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ARROW_in_arrow1454 = new BitSet(new long[]{0x0000000009000080L});
	public static final BitSet FOLLOW_basic_in_arrow1458 = new BitSet(new long[]{0x0000000000000002L});
}
