// $ANTLR 3.5.2 it.unibo.lpemc2014/FOOL.g 2015-02-13 14:18:06
package it.unibo.lpemc.implementation;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLLexer extends Lexer {
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ARROW=5;
	public static final int ASS=6;
	public static final int BOOL=7;
	public static final int CLASS=8;
	public static final int CLPAR=9;
	public static final int COLON=10;
	public static final int COMMA=11;
	public static final int COMMENT=12;
	public static final int CRPAR=13;
	public static final int DIV=14;
	public static final int DOT=15;
	public static final int ELSE=16;
	public static final int EQ=17;
	public static final int ERR=18;
	public static final int EXTENDS=19;
	public static final int FALSE=20;
	public static final int FUN=21;
	public static final int GR=22;
	public static final int GREAT=23;
	public static final int ID=24;
	public static final int IF=25;
	public static final int IN=26;
	public static final int INT=27;
	public static final int INTEGER=28;
	public static final int LE=29;
	public static final int LESS=30;
	public static final int LET=31;
	public static final int LPAR=32;
	public static final int MINUS=33;
	public static final int MULT=34;
	public static final int NEW=35;
	public static final int NOT=36;
	public static final int NULL=37;
	public static final int OR=38;
	public static final int PLUS=39;
	public static final int PRINT=40;
	public static final int RPAR=41;
	public static final int SEMIC=42;
	public static final int THEN=43;
	public static final int TRUE=44;
	public static final int VAR=45;
	public static final int WHITESP=46;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public FOOLLexer() {} 
	public FOOLLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "it.unibo.lpemc2014/FOOL.g"; }

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:314:9: ( '+' )
			// it.unibo.lpemc2014/FOOL.g:314:11: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:315:9: ( '-' )
			// it.unibo.lpemc2014/FOOL.g:315:11: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "MULT"
	public final void mMULT() throws RecognitionException {
		try {
			int _type = MULT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:316:9: ( '*' )
			// it.unibo.lpemc2014/FOOL.g:316:11: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MULT"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:317:9: ( '/' )
			// it.unibo.lpemc2014/FOOL.g:317:11: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	// $ANTLR start "LPAR"
	public final void mLPAR() throws RecognitionException {
		try {
			int _type = LPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:318:9: ( '(' )
			// it.unibo.lpemc2014/FOOL.g:318:11: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LPAR"

	// $ANTLR start "RPAR"
	public final void mRPAR() throws RecognitionException {
		try {
			int _type = RPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:319:9: ( ')' )
			// it.unibo.lpemc2014/FOOL.g:319:11: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RPAR"

	// $ANTLR start "CLPAR"
	public final void mCLPAR() throws RecognitionException {
		try {
			int _type = CLPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:320:9: ( '{' )
			// it.unibo.lpemc2014/FOOL.g:320:11: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CLPAR"

	// $ANTLR start "CRPAR"
	public final void mCRPAR() throws RecognitionException {
		try {
			int _type = CRPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:321:9: ( '}' )
			// it.unibo.lpemc2014/FOOL.g:321:11: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CRPAR"

	// $ANTLR start "SEMIC"
	public final void mSEMIC() throws RecognitionException {
		try {
			int _type = SEMIC;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:322:9: ( ';' )
			// it.unibo.lpemc2014/FOOL.g:322:11: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SEMIC"

	// $ANTLR start "COLON"
	public final void mCOLON() throws RecognitionException {
		try {
			int _type = COLON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:323:9: ( ':' )
			// it.unibo.lpemc2014/FOOL.g:323:11: ':'
			{
			match(':'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COLON"

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			int _type = COMMA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:324:9: ( ',' )
			// it.unibo.lpemc2014/FOOL.g:324:11: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMA"

	// $ANTLR start "DOT"
	public final void mDOT() throws RecognitionException {
		try {
			int _type = DOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:325:9: ( '.' )
			// it.unibo.lpemc2014/FOOL.g:325:11: '.'
			{
			match('.'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DOT"

	// $ANTLR start "OR"
	public final void mOR() throws RecognitionException {
		try {
			int _type = OR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:326:9: ( '||' )
			// it.unibo.lpemc2014/FOOL.g:326:11: '||'
			{
			match("||"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OR"

	// $ANTLR start "AND"
	public final void mAND() throws RecognitionException {
		try {
			int _type = AND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:327:9: ( '&&' )
			// it.unibo.lpemc2014/FOOL.g:327:11: '&&'
			{
			match("&&"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AND"

	// $ANTLR start "NOT"
	public final void mNOT() throws RecognitionException {
		try {
			int _type = NOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:328:9: ( 'not' )
			// it.unibo.lpemc2014/FOOL.g:328:11: 'not'
			{
			match("not"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOT"

	// $ANTLR start "GR"
	public final void mGR() throws RecognitionException {
		try {
			int _type = GR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:329:9: ( '>=' )
			// it.unibo.lpemc2014/FOOL.g:329:11: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GR"

	// $ANTLR start "GREAT"
	public final void mGREAT() throws RecognitionException {
		try {
			int _type = GREAT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:330:9: ( '>' )
			// it.unibo.lpemc2014/FOOL.g:330:11: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GREAT"

	// $ANTLR start "LESS"
	public final void mLESS() throws RecognitionException {
		try {
			int _type = LESS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:331:9: ( '<' )
			// it.unibo.lpemc2014/FOOL.g:331:11: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LESS"

	// $ANTLR start "LE"
	public final void mLE() throws RecognitionException {
		try {
			int _type = LE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:332:9: ( '<=' )
			// it.unibo.lpemc2014/FOOL.g:332:11: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LE"

	// $ANTLR start "EQ"
	public final void mEQ() throws RecognitionException {
		try {
			int _type = EQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:333:9: ( '==' )
			// it.unibo.lpemc2014/FOOL.g:333:11: '=='
			{
			match("=="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EQ"

	// $ANTLR start "ASS"
	public final void mASS() throws RecognitionException {
		try {
			int _type = ASS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:334:9: ( '=' )
			// it.unibo.lpemc2014/FOOL.g:334:11: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASS"

	// $ANTLR start "TRUE"
	public final void mTRUE() throws RecognitionException {
		try {
			int _type = TRUE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:335:9: ( 'true' )
			// it.unibo.lpemc2014/FOOL.g:335:11: 'true'
			{
			match("true"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TRUE"

	// $ANTLR start "FALSE"
	public final void mFALSE() throws RecognitionException {
		try {
			int _type = FALSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:336:9: ( 'false' )
			// it.unibo.lpemc2014/FOOL.g:336:11: 'false'
			{
			match("false"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FALSE"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			int _type = IF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:337:9: ( 'if' )
			// it.unibo.lpemc2014/FOOL.g:337:11: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IF"

	// $ANTLR start "THEN"
	public final void mTHEN() throws RecognitionException {
		try {
			int _type = THEN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:338:9: ( 'then' )
			// it.unibo.lpemc2014/FOOL.g:338:11: 'then'
			{
			match("then"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "THEN"

	// $ANTLR start "ELSE"
	public final void mELSE() throws RecognitionException {
		try {
			int _type = ELSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:339:9: ( 'else' )
			// it.unibo.lpemc2014/FOOL.g:339:11: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ELSE"

	// $ANTLR start "PRINT"
	public final void mPRINT() throws RecognitionException {
		try {
			int _type = PRINT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:340:9: ( 'print' )
			// it.unibo.lpemc2014/FOOL.g:340:11: 'print'
			{
			match("print"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PRINT"

	// $ANTLR start "LET"
	public final void mLET() throws RecognitionException {
		try {
			int _type = LET;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:341:9: ( 'let' )
			// it.unibo.lpemc2014/FOOL.g:341:11: 'let'
			{
			match("let"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LET"

	// $ANTLR start "IN"
	public final void mIN() throws RecognitionException {
		try {
			int _type = IN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:342:9: ( 'in' )
			// it.unibo.lpemc2014/FOOL.g:342:11: 'in'
			{
			match("in"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IN"

	// $ANTLR start "VAR"
	public final void mVAR() throws RecognitionException {
		try {
			int _type = VAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:343:9: ( 'var' )
			// it.unibo.lpemc2014/FOOL.g:343:11: 'var'
			{
			match("var"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VAR"

	// $ANTLR start "FUN"
	public final void mFUN() throws RecognitionException {
		try {
			int _type = FUN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:344:9: ( 'fun' )
			// it.unibo.lpemc2014/FOOL.g:344:11: 'fun'
			{
			match("fun"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FUN"

	// $ANTLR start "CLASS"
	public final void mCLASS() throws RecognitionException {
		try {
			int _type = CLASS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:345:9: ( 'class' )
			// it.unibo.lpemc2014/FOOL.g:345:11: 'class'
			{
			match("class"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CLASS"

	// $ANTLR start "EXTENDS"
	public final void mEXTENDS() throws RecognitionException {
		try {
			int _type = EXTENDS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:346:9: ( 'extends' )
			// it.unibo.lpemc2014/FOOL.g:346:11: 'extends'
			{
			match("extends"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EXTENDS"

	// $ANTLR start "NEW"
	public final void mNEW() throws RecognitionException {
		try {
			int _type = NEW;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:347:9: ( 'new' )
			// it.unibo.lpemc2014/FOOL.g:347:11: 'new'
			{
			match("new"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEW"

	// $ANTLR start "NULL"
	public final void mNULL() throws RecognitionException {
		try {
			int _type = NULL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:348:9: ( 'null' )
			// it.unibo.lpemc2014/FOOL.g:348:11: 'null'
			{
			match("null"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NULL"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:349:9: ( 'int' )
			// it.unibo.lpemc2014/FOOL.g:349:11: 'int'
			{
			match("int"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "BOOL"
	public final void mBOOL() throws RecognitionException {
		try {
			int _type = BOOL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:350:9: ( 'bool' )
			// it.unibo.lpemc2014/FOOL.g:350:11: 'bool'
			{
			match("bool"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BOOL"

	// $ANTLR start "ARROW"
	public final void mARROW() throws RecognitionException {
		try {
			int _type = ARROW;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:351:9: ( '->' )
			// it.unibo.lpemc2014/FOOL.g:351:11: '->'
			{
			match("->"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARROW"

	// $ANTLR start "INTEGER"
	public final void mINTEGER() throws RecognitionException {
		try {
			int _type = INTEGER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:352:9: ( ( ( '1' .. '9' ) ( '0' .. '9' )* ) | '0' )
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( ((LA2_0 >= '1' && LA2_0 <= '9')) ) {
				alt2=1;
			}
			else if ( (LA2_0=='0') ) {
				alt2=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}

			switch (alt2) {
				case 1 :
					// it.unibo.lpemc2014/FOOL.g:352:11: ( ( '1' .. '9' ) ( '0' .. '9' )* )
					{
					// it.unibo.lpemc2014/FOOL.g:352:11: ( ( '1' .. '9' ) ( '0' .. '9' )* )
					// it.unibo.lpemc2014/FOOL.g:352:12: ( '1' .. '9' ) ( '0' .. '9' )*
					{
					if ( (input.LA(1) >= '1' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					// it.unibo.lpemc2014/FOOL.g:352:22: ( '0' .. '9' )*
					loop1:
					while (true) {
						int alt1=2;
						int LA1_0 = input.LA(1);
						if ( ((LA1_0 >= '0' && LA1_0 <= '9')) ) {
							alt1=1;
						}

						switch (alt1) {
						case 1 :
							// it.unibo.lpemc2014/FOOL.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop1;
						}
					}

					}

					}
					break;
				case 2 :
					// it.unibo.lpemc2014/FOOL.g:352:37: '0'
					{
					match('0'); 
					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INTEGER"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:353:9: ( ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )* )
			// it.unibo.lpemc2014/FOOL.g:353:11: ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// it.unibo.lpemc2014/FOOL.g:353:30: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( ((LA3_0 >= '0' && LA3_0 <= '9')||(LA3_0 >= 'A' && LA3_0 <= 'Z')||(LA3_0 >= 'a' && LA3_0 <= 'z')) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// it.unibo.lpemc2014/FOOL.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop3;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "WHITESP"
	public final void mWHITESP() throws RecognitionException {
		try {
			int _type = WHITESP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:354:9: ( ( '\\t' | ' ' | '\\r' | '\\n' )+ )
			// it.unibo.lpemc2014/FOOL.g:354:11: ( '\\t' | ' ' | '\\r' | '\\n' )+
			{
			// it.unibo.lpemc2014/FOOL.g:354:11: ( '\\t' | ' ' | '\\r' | '\\n' )+
			int cnt4=0;
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( ((LA4_0 >= '\t' && LA4_0 <= '\n')||LA4_0=='\r'||LA4_0==' ') ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// it.unibo.lpemc2014/FOOL.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt4 >= 1 ) break loop4;
					EarlyExitException eee = new EarlyExitException(4, input);
					throw eee;
				}
				cnt4++;
			}

			 skip(); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHITESP"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:356:9: ( '/*' ( options {greedy=false; } : . )* '*/' )
			// it.unibo.lpemc2014/FOOL.g:356:11: '/*' ( options {greedy=false; } : . )* '*/'
			{
			match("/*"); 

			// it.unibo.lpemc2014/FOOL.g:356:16: ( options {greedy=false; } : . )*
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( (LA5_0=='*') ) {
					int LA5_1 = input.LA(2);
					if ( (LA5_1=='/') ) {
						alt5=2;
					}
					else if ( ((LA5_1 >= '\u0000' && LA5_1 <= '.')||(LA5_1 >= '0' && LA5_1 <= '\uFFFF')) ) {
						alt5=1;
					}

				}
				else if ( ((LA5_0 >= '\u0000' && LA5_0 <= ')')||(LA5_0 >= '+' && LA5_0 <= '\uFFFF')) ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// it.unibo.lpemc2014/FOOL.g:356:43: .
					{
					matchAny(); 
					}
					break;

				default :
					break loop5;
				}
			}

			match("*/"); 

			 skip(); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "ERR"
	public final void mERR() throws RecognitionException {
		try {
			int _type = ERR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// it.unibo.lpemc2014/FOOL.g:357:8: ( . )
			// it.unibo.lpemc2014/FOOL.g:357:10: .
			{
			matchAny(); 
			 System.out.println("Invalid char: " + getText()); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ERR"

	@Override
	public void mTokens() throws RecognitionException {
		// it.unibo.lpemc2014/FOOL.g:1:8: ( PLUS | MINUS | MULT | DIV | LPAR | RPAR | CLPAR | CRPAR | SEMIC | COLON | COMMA | DOT | OR | AND | NOT | GR | GREAT | LESS | LE | EQ | ASS | TRUE | FALSE | IF | THEN | ELSE | PRINT | LET | IN | VAR | FUN | CLASS | EXTENDS | NEW | NULL | INT | BOOL | ARROW | INTEGER | ID | WHITESP | COMMENT | ERR )
		int alt6=43;
		alt6 = dfa6.predict(input);
		switch (alt6) {
			case 1 :
				// it.unibo.lpemc2014/FOOL.g:1:10: PLUS
				{
				mPLUS(); 

				}
				break;
			case 2 :
				// it.unibo.lpemc2014/FOOL.g:1:15: MINUS
				{
				mMINUS(); 

				}
				break;
			case 3 :
				// it.unibo.lpemc2014/FOOL.g:1:21: MULT
				{
				mMULT(); 

				}
				break;
			case 4 :
				// it.unibo.lpemc2014/FOOL.g:1:26: DIV
				{
				mDIV(); 

				}
				break;
			case 5 :
				// it.unibo.lpemc2014/FOOL.g:1:30: LPAR
				{
				mLPAR(); 

				}
				break;
			case 6 :
				// it.unibo.lpemc2014/FOOL.g:1:35: RPAR
				{
				mRPAR(); 

				}
				break;
			case 7 :
				// it.unibo.lpemc2014/FOOL.g:1:40: CLPAR
				{
				mCLPAR(); 

				}
				break;
			case 8 :
				// it.unibo.lpemc2014/FOOL.g:1:46: CRPAR
				{
				mCRPAR(); 

				}
				break;
			case 9 :
				// it.unibo.lpemc2014/FOOL.g:1:52: SEMIC
				{
				mSEMIC(); 

				}
				break;
			case 10 :
				// it.unibo.lpemc2014/FOOL.g:1:58: COLON
				{
				mCOLON(); 

				}
				break;
			case 11 :
				// it.unibo.lpemc2014/FOOL.g:1:64: COMMA
				{
				mCOMMA(); 

				}
				break;
			case 12 :
				// it.unibo.lpemc2014/FOOL.g:1:70: DOT
				{
				mDOT(); 

				}
				break;
			case 13 :
				// it.unibo.lpemc2014/FOOL.g:1:74: OR
				{
				mOR(); 

				}
				break;
			case 14 :
				// it.unibo.lpemc2014/FOOL.g:1:77: AND
				{
				mAND(); 

				}
				break;
			case 15 :
				// it.unibo.lpemc2014/FOOL.g:1:81: NOT
				{
				mNOT(); 

				}
				break;
			case 16 :
				// it.unibo.lpemc2014/FOOL.g:1:85: GR
				{
				mGR(); 

				}
				break;
			case 17 :
				// it.unibo.lpemc2014/FOOL.g:1:88: GREAT
				{
				mGREAT(); 

				}
				break;
			case 18 :
				// it.unibo.lpemc2014/FOOL.g:1:94: LESS
				{
				mLESS(); 

				}
				break;
			case 19 :
				// it.unibo.lpemc2014/FOOL.g:1:99: LE
				{
				mLE(); 

				}
				break;
			case 20 :
				// it.unibo.lpemc2014/FOOL.g:1:102: EQ
				{
				mEQ(); 

				}
				break;
			case 21 :
				// it.unibo.lpemc2014/FOOL.g:1:105: ASS
				{
				mASS(); 

				}
				break;
			case 22 :
				// it.unibo.lpemc2014/FOOL.g:1:109: TRUE
				{
				mTRUE(); 

				}
				break;
			case 23 :
				// it.unibo.lpemc2014/FOOL.g:1:114: FALSE
				{
				mFALSE(); 

				}
				break;
			case 24 :
				// it.unibo.lpemc2014/FOOL.g:1:120: IF
				{
				mIF(); 

				}
				break;
			case 25 :
				// it.unibo.lpemc2014/FOOL.g:1:123: THEN
				{
				mTHEN(); 

				}
				break;
			case 26 :
				// it.unibo.lpemc2014/FOOL.g:1:128: ELSE
				{
				mELSE(); 

				}
				break;
			case 27 :
				// it.unibo.lpemc2014/FOOL.g:1:133: PRINT
				{
				mPRINT(); 

				}
				break;
			case 28 :
				// it.unibo.lpemc2014/FOOL.g:1:139: LET
				{
				mLET(); 

				}
				break;
			case 29 :
				// it.unibo.lpemc2014/FOOL.g:1:143: IN
				{
				mIN(); 

				}
				break;
			case 30 :
				// it.unibo.lpemc2014/FOOL.g:1:146: VAR
				{
				mVAR(); 

				}
				break;
			case 31 :
				// it.unibo.lpemc2014/FOOL.g:1:150: FUN
				{
				mFUN(); 

				}
				break;
			case 32 :
				// it.unibo.lpemc2014/FOOL.g:1:154: CLASS
				{
				mCLASS(); 

				}
				break;
			case 33 :
				// it.unibo.lpemc2014/FOOL.g:1:160: EXTENDS
				{
				mEXTENDS(); 

				}
				break;
			case 34 :
				// it.unibo.lpemc2014/FOOL.g:1:168: NEW
				{
				mNEW(); 

				}
				break;
			case 35 :
				// it.unibo.lpemc2014/FOOL.g:1:172: NULL
				{
				mNULL(); 

				}
				break;
			case 36 :
				// it.unibo.lpemc2014/FOOL.g:1:177: INT
				{
				mINT(); 

				}
				break;
			case 37 :
				// it.unibo.lpemc2014/FOOL.g:1:181: BOOL
				{
				mBOOL(); 

				}
				break;
			case 38 :
				// it.unibo.lpemc2014/FOOL.g:1:186: ARROW
				{
				mARROW(); 

				}
				break;
			case 39 :
				// it.unibo.lpemc2014/FOOL.g:1:192: INTEGER
				{
				mINTEGER(); 

				}
				break;
			case 40 :
				// it.unibo.lpemc2014/FOOL.g:1:200: ID
				{
				mID(); 

				}
				break;
			case 41 :
				// it.unibo.lpemc2014/FOOL.g:1:203: WHITESP
				{
				mWHITESP(); 

				}
				break;
			case 42 :
				// it.unibo.lpemc2014/FOOL.g:1:211: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 43 :
				// it.unibo.lpemc2014/FOOL.g:1:219: ERR
				{
				mERR(); 

				}
				break;

		}
	}


	protected DFA6 dfa6 = new DFA6(this);
	static final String DFA6_eotS =
		"\2\uffff\1\43\1\uffff\1\46\10\uffff\2\40\1\64\1\66\1\70\1\72\11\64\25"+
		"\uffff\3\64\7\uffff\4\64\1\121\1\123\7\64\2\uffff\1\133\1\134\4\64\1\141"+
		"\1\uffff\1\142\1\uffff\3\64\1\146\1\147\2\64\2\uffff\1\152\1\153\1\154"+
		"\1\64\2\uffff\1\156\2\64\2\uffff\1\64\1\162\3\uffff\1\163\1\uffff\1\64"+
		"\1\165\1\166\2\uffff\1\64\2\uffff\1\170\1\uffff";
	static final String DFA6_eofS =
		"\171\uffff";
	static final String DFA6_minS =
		"\1\0\1\uffff\1\76\1\uffff\1\52\10\uffff\1\174\1\46\1\145\3\75\1\150\1"+
		"\141\1\146\1\154\1\162\1\145\1\141\1\154\1\157\25\uffff\1\164\1\167\1"+
		"\154\7\uffff\1\165\1\145\1\154\1\156\2\60\1\163\1\164\1\151\1\164\1\162"+
		"\1\141\1\157\2\uffff\2\60\1\154\1\145\1\156\1\163\1\60\1\uffff\1\60\1"+
		"\uffff\2\145\1\156\2\60\1\163\1\154\2\uffff\3\60\1\145\2\uffff\1\60\1"+
		"\156\1\164\2\uffff\1\163\1\60\3\uffff\1\60\1\uffff\1\144\2\60\2\uffff"+
		"\1\163\2\uffff\1\60\1\uffff";
	static final String DFA6_maxS =
		"\1\uffff\1\uffff\1\76\1\uffff\1\52\10\uffff\1\174\1\46\1\165\3\75\1\162"+
		"\1\165\1\156\1\170\1\162\1\145\1\141\1\154\1\157\25\uffff\1\164\1\167"+
		"\1\154\7\uffff\1\165\1\145\1\154\1\156\2\172\1\163\1\164\1\151\1\164\1"+
		"\162\1\141\1\157\2\uffff\2\172\1\154\1\145\1\156\1\163\1\172\1\uffff\1"+
		"\172\1\uffff\2\145\1\156\2\172\1\163\1\154\2\uffff\3\172\1\145\2\uffff"+
		"\1\172\1\156\1\164\2\uffff\1\163\1\172\3\uffff\1\172\1\uffff\1\144\2\172"+
		"\2\uffff\1\163\2\uffff\1\172\1\uffff";
	static final String DFA6_acceptS =
		"\1\uffff\1\1\1\uffff\1\3\1\uffff\1\5\1\6\1\7\1\10\1\11\1\12\1\13\1\14"+
		"\17\uffff\2\47\1\50\1\51\1\53\1\1\1\46\1\2\1\3\1\52\1\4\1\5\1\6\1\7\1"+
		"\10\1\11\1\12\1\13\1\14\1\15\1\16\3\uffff\1\50\1\20\1\21\1\23\1\22\1\24"+
		"\1\25\15\uffff\1\47\1\51\7\uffff\1\30\1\uffff\1\35\7\uffff\1\17\1\42\4"+
		"\uffff\1\37\1\44\3\uffff\1\34\1\36\2\uffff\1\43\1\26\1\31\1\uffff\1\32"+
		"\3\uffff\1\45\1\27\1\uffff\1\33\1\40\1\uffff\1\41";
	static final String DFA6_specialS =
		"\1\0\170\uffff}>";
	static final String[] DFA6_transitionS = {
			"\11\40\2\37\2\40\1\37\22\40\1\37\5\40\1\16\1\40\1\5\1\6\1\3\1\1\1\13"+
			"\1\2\1\14\1\4\1\35\11\34\1\12\1\11\1\21\1\22\1\20\2\40\32\36\6\40\1\36"+
			"\1\33\1\32\1\36\1\26\1\24\2\36\1\25\2\36\1\30\1\36\1\17\1\36\1\27\3\36"+
			"\1\23\1\36\1\31\4\36\1\7\1\15\1\10\uff82\40",
			"",
			"\1\42",
			"",
			"\1\45",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\57",
			"\1\60",
			"\1\62\11\uffff\1\61\5\uffff\1\63",
			"\1\65",
			"\1\67",
			"\1\71",
			"\1\74\11\uffff\1\73",
			"\1\75\23\uffff\1\76",
			"\1\77\7\uffff\1\100",
			"\1\101\13\uffff\1\102",
			"\1\103",
			"\1\104",
			"\1\105",
			"\1\106",
			"\1\107",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\112",
			"\1\113",
			"\1\114",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\115",
			"\1\116",
			"\1\117",
			"\1\120",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			"\12\64\7\uffff\32\64\6\uffff\23\64\1\122\6\64",
			"\1\124",
			"\1\125",
			"\1\126",
			"\1\127",
			"\1\130",
			"\1\131",
			"\1\132",
			"",
			"",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			"\1\135",
			"\1\136",
			"\1\137",
			"\1\140",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			"",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			"",
			"\1\143",
			"\1\144",
			"\1\145",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			"\1\150",
			"\1\151",
			"",
			"",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			"\1\155",
			"",
			"",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			"\1\157",
			"\1\160",
			"",
			"",
			"\1\161",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			"",
			"",
			"",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			"",
			"\1\164",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			"",
			"",
			"\1\167",
			"",
			"",
			"\12\64\7\uffff\32\64\6\uffff\32\64",
			""
	};

	static final short[] DFA6_eot = DFA.unpackEncodedString(DFA6_eotS);
	static final short[] DFA6_eof = DFA.unpackEncodedString(DFA6_eofS);
	static final char[] DFA6_min = DFA.unpackEncodedStringToUnsignedChars(DFA6_minS);
	static final char[] DFA6_max = DFA.unpackEncodedStringToUnsignedChars(DFA6_maxS);
	static final short[] DFA6_accept = DFA.unpackEncodedString(DFA6_acceptS);
	static final short[] DFA6_special = DFA.unpackEncodedString(DFA6_specialS);
	static final short[][] DFA6_transition;

	static {
		int numStates = DFA6_transitionS.length;
		DFA6_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA6_transition[i] = DFA.unpackEncodedString(DFA6_transitionS[i]);
		}
	}

	protected class DFA6 extends DFA {

		public DFA6(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 6;
			this.eot = DFA6_eot;
			this.eof = DFA6_eof;
			this.min = DFA6_min;
			this.max = DFA6_max;
			this.accept = DFA6_accept;
			this.special = DFA6_special;
			this.transition = DFA6_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( PLUS | MINUS | MULT | DIV | LPAR | RPAR | CLPAR | CRPAR | SEMIC | COLON | COMMA | DOT | OR | AND | NOT | GR | GREAT | LESS | LE | EQ | ASS | TRUE | FALSE | IF | THEN | ELSE | PRINT | LET | IN | VAR | FUN | CLASS | EXTENDS | NEW | NULL | INT | BOOL | ARROW | INTEGER | ID | WHITESP | COMMENT | ERR );";
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			IntStream input = _input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA6_0 = input.LA(1);
						s = -1;
						if ( (LA6_0=='+') ) {s = 1;}
						else if ( (LA6_0=='-') ) {s = 2;}
						else if ( (LA6_0=='*') ) {s = 3;}
						else if ( (LA6_0=='/') ) {s = 4;}
						else if ( (LA6_0=='(') ) {s = 5;}
						else if ( (LA6_0==')') ) {s = 6;}
						else if ( (LA6_0=='{') ) {s = 7;}
						else if ( (LA6_0=='}') ) {s = 8;}
						else if ( (LA6_0==';') ) {s = 9;}
						else if ( (LA6_0==':') ) {s = 10;}
						else if ( (LA6_0==',') ) {s = 11;}
						else if ( (LA6_0=='.') ) {s = 12;}
						else if ( (LA6_0=='|') ) {s = 13;}
						else if ( (LA6_0=='&') ) {s = 14;}
						else if ( (LA6_0=='n') ) {s = 15;}
						else if ( (LA6_0=='>') ) {s = 16;}
						else if ( (LA6_0=='<') ) {s = 17;}
						else if ( (LA6_0=='=') ) {s = 18;}
						else if ( (LA6_0=='t') ) {s = 19;}
						else if ( (LA6_0=='f') ) {s = 20;}
						else if ( (LA6_0=='i') ) {s = 21;}
						else if ( (LA6_0=='e') ) {s = 22;}
						else if ( (LA6_0=='p') ) {s = 23;}
						else if ( (LA6_0=='l') ) {s = 24;}
						else if ( (LA6_0=='v') ) {s = 25;}
						else if ( (LA6_0=='c') ) {s = 26;}
						else if ( (LA6_0=='b') ) {s = 27;}
						else if ( ((LA6_0 >= '1' && LA6_0 <= '9')) ) {s = 28;}
						else if ( (LA6_0=='0') ) {s = 29;}
						else if ( ((LA6_0 >= 'A' && LA6_0 <= 'Z')||LA6_0=='a'||LA6_0=='d'||(LA6_0 >= 'g' && LA6_0 <= 'h')||(LA6_0 >= 'j' && LA6_0 <= 'k')||LA6_0=='m'||LA6_0=='o'||(LA6_0 >= 'q' && LA6_0 <= 's')||LA6_0=='u'||(LA6_0 >= 'w' && LA6_0 <= 'z')) ) {s = 30;}
						else if ( ((LA6_0 >= '\t' && LA6_0 <= '\n')||LA6_0=='\r'||LA6_0==' ') ) {s = 31;}
						else if ( ((LA6_0 >= '\u0000' && LA6_0 <= '\b')||(LA6_0 >= '\u000B' && LA6_0 <= '\f')||(LA6_0 >= '\u000E' && LA6_0 <= '\u001F')||(LA6_0 >= '!' && LA6_0 <= '%')||LA6_0=='\''||(LA6_0 >= '?' && LA6_0 <= '@')||(LA6_0 >= '[' && LA6_0 <= '`')||(LA6_0 >= '~' && LA6_0 <= '\uFFFF')) ) {s = 32;}
						if ( s>=0 ) return s;
						break;
			}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 6, _s, input);
			error(nvae);
			throw nvae;
		}
	}

}
