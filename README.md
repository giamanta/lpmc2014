Progetto Linguaggi di Programmazione e Modelli Computazionali
-------------------------------------------------------------

- FOOL language
```
Il progetto consiste nella realizzazione di un compilatore per il linguaggio
la cui sintassi e` definita nel file FOOL.g (da aprire con ANTLRworks). Tale
linguaggio puo` essere considerato un linguaggio funzionale object-oriented.
E` possibile dichiarare classi e sottoclassi. Gli oggetti, che nascono come
instanziazione di classi, contengono dei campi (dichiarati all’inizio della
dichiarazione della classe o ereditati dalla superclasse) e dei metodi
(esplicitamente dichiarati o ereditati dalla superclasse). Se in una
sottoclasse viene dichiarato un campo o un metodo con il medesimo nome di un
campo della superclasse, tale campo o metodo sovrascrive quello della
superclasse. I campi non sono modificabili ed il loro valore viene definito in
fase di instanziazione. E` inoltre possibile dichiarare funzioni annidate. Le
funzioni possono essere passate come parametri.

Il compilatore deve comprendere un type-checker che controlli il corretto uso
dei tipi. In particolare si deve considerare una nozione di subtyping fra
classi e tipi di funzioni. Il tipo di una funzione f1 e` sottotipo del tipo di
una funzione f2 se il tipo ritornato da f1 e` sottotipo del tipo ritornato da
f2, se hanno il medesimo numero di parametri, e se ogni tipo di paramentro di
f1 e` sopratipo del corrisponde tipo di parametro di f2. Una classe C1 e`
sottotipo di una classe C2 se C1 estende C2 e se i campi e metodi che vengono
sovrascritti sono sottotipi rispetto ai campi e metodi corrispondenti di C2.
Inoltre, C1 e` sottotipo di C2 se esiste una classe C3 sottotipo di C2 di cui
C1 e` sottotipo.

Il compilatore deve generare codice per un esecutore virtuale chiamato SVM
(stack virtual machine) la cui sintassi e` definita nel file SVM.g (da aprire
con ANTLRworks). Tale esecutore ha una memoria in cui gli indirizzi alti sono
usati per uno stack. Uno stack pointer punta alla locazione successiva alla
prossima locazione libera per lo stack (se la memoria ha indirizzi da 0 a
MAXADDRESS, lo stack pointer inizialmente punta a MAXADDRESS+1). In questo
modo, quando lo stack non e` vuoto, lo stack pointer punta al top dello stack.
Il programma e` collocato in una memoria separata puntata dall’instruction
pointer (che punta alla prossima istruzione da eseguire). Gli altri registri
della macchina virtuale sono: HP (heap pointer), RA (return address), RV
(return value) e FP (frame pointer). In particolare, HP serve per puntare alla
prossima locazione disponobile dello heap; assumendo di usare gli indirizzi
bassi per lo heap, HP contiene inizialmente il valore 0.
```
 - Programma esempio
```
let

  class List (f:int, r:List) {
    fun first:int() f;
    fun rest:List() r; 
  }
  
  fun printList:List (l:List) 
    let  
      fun makeList:List (l:List, i:int) new List (i,l);
      fun printL:List (l:List)    
        if (l == null) then {null}
                       else {makeList(printL(l.rest()),print(l.first()))};
    in  printL(l);                 
  
  fun append:List (l1:List, l2:List)
      if (l1 == null)
        then {l2}
        else {new List(l1.first(), append(l1.rest(),l2))} ;  
    
  fun filter:List (l:List,check:(int)->bool) 
      if (l == null) 
        then {null}
        else {if (check(l.first()))
              then {new List(l.first(),filter(l.rest(),check))}
              else {filter(l.rest(),check)}};
       
  fun quicksort:List (l:List, rel:(int,int)->bool)    
      let
        var pivot:int = if (l==null) then {0} else {l.first()}; 
        fun prec:bool (x:int) rel(x,pivot);
        fun succ:bool (x:int) rel(pivot,x);
      in        
         if (l == null) 
           then {null}
           else {append( quicksort(filter(l,prec),rel),
                         new List(pivot, quicksort(filter(l,succ),rel)) )};
           
  fun increasing:List(l:List) 
    let fun inc:bool (x:int,y:int) not(y<=x);
    in quicksort(l,inc);
  
  fun decreasing:List(l:List) 
    let fun dec:bool (x:int,y:int) not(x<=y);
    in quicksort(l,dec);   
               
  var l:List = new List (3, 
                    new List(4,
                        new List(6,
                            new List (1, 
                                new List(2,
                                    new List(5,null))))));
  
in printList(increasing(l));
```