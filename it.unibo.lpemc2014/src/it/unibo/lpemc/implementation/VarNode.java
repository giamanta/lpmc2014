package it.unibo.lpemc.implementation;

import it.unibo.lpemc.interfaces.Node;
import it.unibo.lpemc.interfaces.NodePrintable;

/**
 * Nodo che realizza il concetto di variabile, una variabile ha un identificatore, tipo e valore. Anche gli oggetti sono trattati come variabili. Quindi per creare un oggetto devo fare var obj:ClassName = new ClassName(params); Il codice generato corrisponde a value.codeGeneration, in questo caso coincide con la codeGeneration di new ClassName(params); Di conseguenza per realizzare OOP non devo modificare questo nodo, bens� gestire in modo corretto l'operatore new responsabile alla creazione
 * dell'ogetto.
 */
public class VarNode extends NodePrintable {
	
	private final String	id;
	private final Node		type;
	private final Node		value;
	
	public VarNode(final String varName, final Node type, final Node value) {
		super(Syskb.VAR + Syskb.BLANK + varName, type, value);
		this.id = varName;
		this.type = type;
		this.value = value;
		FOOLlib.setFather(value, this);
	}
	
	@Override
	protected String codeGen() {
		return this.value.codeGeneration();
	}
	
	public Node getType() {
		return this.type;
	}
	
	@Override
	public Node getValue() {
		return this.value;
	}
	
	@Override
	public Node typeCheck() {
		final Node t = this.type.typeCheck();
		if (!(FOOLlib.isSubtype(this.value.typeCheck(), t))) {
			FOOLlib.exit("Incompatible value for variable " + this.id + ":" + this.value.typeCheck().toPrint("") + ", expected " + t.typeCheck().toPrint(""));
		}
		return t;
	}
}