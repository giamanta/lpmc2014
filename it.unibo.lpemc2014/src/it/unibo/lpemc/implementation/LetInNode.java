package it.unibo.lpemc.implementation;

import it.unibo.lpemc.interfaces.MergeNode;
import it.unibo.lpemc.interfaces.Node;

import java.util.ArrayList;

public class LetInNode extends Node {
	
	private final ArrayList<Node>	dec;
	private final MergeNode			declarations;
	private final Node				exp;
	private final MergeNode			expression;
	
	public LetInNode(final ArrayList<Node> declarations, final Node expression) {
		super("letin");
		this.dec = declarations;
		FOOLlib.setFather(this.dec, this);
		this.declarations = new MergeNode(Syskb.LET, declarations);
		this.exp = expression;
		FOOLlib.setFather(this.exp, this);
		this.expression = new MergeNode(Syskb.IN, expression);
	}
	
	@Override
	protected String codeGen() {
		// le variabili partono da offset -2, quindi utilizzo una push fittizia
		String s = ""; 
				
		for (final Node n : this.dec) {
			s += n.codeGeneration();
		}
		return "PROG:\n" + Syskb.PUSH + "-1" + Syskb.CR /*+ FOOLlib.getObjectCode()*/ + s + this.exp.codeGeneration() + Syskb.HALT + FOOLlib.getCode();
	}
	
	@Override
	public String toPrint(final String s) {
		return this.declarations.toPrint(s) + this.expression.toPrint(s);
	}
	
	@Override
	public Node typeCheck() {
		for (int i = 0; i < this.dec.size(); i++) {
			(this.dec.get(i)).typeCheck();
		}
		return this.exp.typeCheck();
	}
}