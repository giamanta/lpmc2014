package it.unibo.lpemc.implementation;

import it.unibo.lpemc.implementation.oop.ClassNode;
import it.unibo.lpemc.implementation.types.ArrowTypeNode;
import it.unibo.lpemc.interfaces.INode;
import it.unibo.lpemc.interfaces.Node;

import java.util.ArrayList;
import java.util.HashMap;

public class FOOLlib {
	
	interface ICrazyFactory {
		/**
		 * Aggiunge il parametro a un metodo
		 * 
		 * @param declarationLine
		 *            linea in cui � dichiarato il field nella grammatica
		 * @param methodNode
		 *            nodo del metodo
		 * @param id
		 *            id del metodo
		 * @param type
		 *            tipo del metodo
		 * @param nestingLevel
		 *            nesting level del metodo
		 * @param offset
		 *            offset del parametro all'interno del metodo (NB per costruzione gli offset dei parametri sono positivi e iniziano con offset a 1)
		 * @return
		 */
		ParNode addParamInFunction(int declarationLine, MethodNode methodNode, String id, Node type, int nestingLevel, Offset offset);
		
		/**
		 * Aggiunge una variabile alla funzione
		 * 
		 * @param declarationLine
		 *            linea in cui � dichiarato il field nella grammatica
		 * @param methodNode
		 *            nodo del metodo (funzione)
		 * @param id
		 *            id della variabile
		 * @param type
		 *            tipo della variabile
		 * @param value
		 *            valore della variabile
		 * @param nestingLevel
		 *            nesting level del metodo
		 * @param offset
		 *            offset della variabile all'interno del metodo (NB per costruzione gli offset delle dichiarazioni interne sono negativi e iniziano con offset a -2)
		 * @return
		 */
		VarNode addVarInFunction(int declarationLine, MethodNode methodNode, String id, Node type, Node value, int nestingLevel, Offset offset);
		
		/**
		 * Esegue la call del metodo
		 * 
		 * @param identifier
		 *            identificatore del metodo
		 * @param parameters
		 *            parametri
		 * @param entry
		 *            entry relativa al metodo
		 * @param nestingLevelDifference
		 *            differenza di nesting level
		 * @return
		 */
		CallNode checkCall(String identifier, ArrayList<Node> parameters, STentry entry, int nestingLevelDifference);
		
		/**
		 * Ottiene il riferimento a una variabile
		 * 
		 * @param declarationLine
		 *            linea in cui � dichiarato il field nella grammatica
		 * @param identifier
		 *            identificatore della variabile
		 * @param entry
		 *            entry relativa alla variabile
		 * @param nestingLevelDifference
		 *            differenza di nesting level
		 * @return
		 */
		IdNode checkId(int declarationLine, String identifier, STentry entry, int nestingLevelDifference);
		
		/**
		 * Restituisce il field dell'oggetto
		 * 
		 * @param declarationLine
		 *            linea in cui � dichiarato il field nella grammatica
		 * @param objectId
		 *            id dell'oggetto
		 * @param fieldId
		 *            id del field ricercato
		 * @param entry
		 *            entry relativa all'oggetto
		 * @param nestingLevelDifference
		 *            differenza di nesting level
		 * @return
		 */
		IdNode checkObjectField(int declarationLine, String objectId, String fieldId, STentry entry, int nestingLevelDifference);
		
		/**
		 * Create a field in a classNode
		 * 
		 * @param declarationLine
		 *            linea in cui � dichiarato il field nella grammatica
		 * @param classNode
		 *            nodo della classe
		 * @param fieldId
		 *            id del field
		 * @param fieldType
		 *            tipo del field
		 * @param map
		 *            hashmap relativa al nesting livel corrente
		 * @param nestingLevel
		 *            nesting level corrente
		 * @param offset
		 *            offset del field all'interno della classe
		 * @return field della classe
		 */
		ParNode createFieldInClass(int declarationLine, ClassNode classNode, String fieldId, Node fieldType, HashMap<String, STentry> map, int nestingLevel, Offset offset);
		
		/**
		 * Crea una funzione (FunNode) all'interno di un activation record
		 * 
		 * @param declarationLine
		 *            linea in cui � dichiarato il field nella grammatica
		 * @param id
		 *            id della funzione
		 * @param type
		 *            tipo della funzione
		 * @param symbolTable
		 *            riferimento alla symble table
		 * @param nestingLevel
		 *            nesting level relativo alla funzione
		 * @param offset
		 *            offset della funzione all'interno dell'activation record
		 * @return
		 */
		FunNode createFunction(int declarationLine, String id, Node type, ArrayList<HashMap<String, STentry>> symbolTable, int nestingLevel, Offset offset);
		
		/**
		 * Create a field in a classNode
		 * 
		 * @param declarationLine
		 *            linea in cui � dichiarato il metodo nella grammatica
		 * @param classNode
		 *            nodo della classe
		 * @param methodId
		 *            id del field
		 * @param methodType
		 *            tipo del field
		 * @param map
		 *            hashmap relativa al nesting livel corrente
		 * @param nestingLevel
		 *            nesting level corrente
		 * @param offset
		 *            offset del field all'interno della classe
		 * @return metodo della classe
		 */
		MethodNode createMethodInClass(int declarationLine, ClassNode classNode, String id, Node type, ArrayList<HashMap<String, STentry>> symbolTable, HashMap<String, STentry> map, int nestingLevel,
				Offset offset);
		
		/**
		 * Crea una variabile (VarNode) all'interno di un activation record
		 * 
		 * @param declarationLine
		 *            linea in cui � dichiarato l'id nella grammatica
		 * @param id
		 *            identificatore della variabile
		 * @param type
		 *            tipo della variabile
		 * @param value
		 *            valore della variabile
		 * @param symbolTable
		 *            riferimento alla symble table
		 * @param nestingLevel
		 *            nesting level dell'activation record
		 * @param offset
		 * @return
		 */
		VarNode createVariable(int declarationLine, String id, Node type, Node value, ArrayList<HashMap<String, STentry>> symbolTable, int nestingLevel, Offset offset);
		
	}
	
	public static ICrazyFactory	factory	= new ICrazyFactory() {
											
											@Override
											public ParNode addParamInFunction(int declarationLine, MethodNode methodNode, String id, Node type, int nestingLevel, Offset offset) {
												ParNode fpar = new ParNode(id, type);
												methodNode.addPar(fpar);
												checkNotNull(methodNode.getActivationRecordEntries().put(id, new STentry(fpar, nestingLevel, type, offset.getOffset())), "Parameter id " + id
														+ " @ line " + declarationLine + " already declared");
												offset.addOffset(type.getSize());
												return fpar;
											}
											
											@Override
											public VarNode addVarInFunction(int declarationLine, MethodNode methodNode, String id, Node type, Node value, int nestingLevel, Offset offset) {
												VarNode v = new VarNode(id, type, value);
												STentry entry = new STentry(v, nestingLevel, type, offset.getOffset());
												offset.addOffset(-type.getSize());
												checkNotNull(methodNode.getActivationRecordEntries().put(id, entry), "Var id " + id + " @ line " + declarationLine + " already declared");
												return v;
											}
											
											@Override
											public CallNode checkCall(String identifier, ArrayList<Node> parameters, STentry entry, int nestingLevelDifference) {
												CallNode callee;
												if (entry.isInClass()) {
													String className = entry.getClassName();
													if (Syskb.getClass(className).hasField(identifier))
														// sono in una classe, sto invocando il metodo di un field, quindi?
														// come primo parametro passo il riferimento al field
														parameters.add(0, Syskb.getClass(className).getFieldNode(identifier));
													callee = Syskb.getClass(className).callMethod(identifier, entry, parameters, nestingLevelDifference);
												} else
													callee = new CallNode(identifier, entry, parameters, nestingLevelDifference);
												if (entry.getNode() instanceof ParNode)
													callee.setFunctionalProgrammingFlag(true);
												callee.setFather(entry.getNode());
												return callee;
											}
											
											@Override
											public IdNode checkId(int declarationLine, String identifier, STentry entry, int nestingLevelDifference) {
												IdNode id = null;
												
												checkNull(entry, "Id " + identifier + " @ line " + declarationLine + " not declared");
												
												if (entry.isInClass())
													id = Syskb.getClass(entry.getClassName()).getFieldNode(identifier);
												else {
													id = new IdNode(identifier, entry, nestingLevelDifference);
													id.setFather(entry.getContest());
												}
												return id;
											}
											
											@Override
											public IdNode checkObjectField(int declarationLine, String objectId, String fieldId, STentry entry, int nestingLevelDifference) {
												IdNode id = null;
												
												if (entry.isInClass())
													// class Integer(i: int) {
													// fun getInt: int() i;
													// }
													// class A(i: Integer) {
													// fun getInt: int() i.i;
													// }
													// Devo recuperare il riferimento a i.i (field del field)
													id = ((ClassNode) entry.getType().typeCheck()).getFieldNode(Syskb.getClass(entry.getClassName()).getFieldNode(objectId), fieldId);
												else
													// class Integer(i: int) {
													// fun getInt: int() i;
													// }
													// print(i.i);
													// Devo recuperare il riferimento al field dell'oggetto i
													// (identificato da un IdNode)
													id = ((ClassNode) entry.getType().typeCheck()).getFieldNode(new IdNode(objectId, entry, nestingLevelDifference), fieldId);
												return id;
											}
											
											@Override
											public ParNode createFieldInClass(int declarationLine, ClassNode classNode, String fieldId, Node fieldType, HashMap<String, STentry> map, int nestingLevel,
													Offset offset) {
												ParNode fieldPar = new ParNode(fieldId, fieldType);
												classNode.addField(fieldPar);
												checkNotNull(map.put(fieldId, new STentry(fieldPar, nestingLevel, fieldType, offset.getOffset(), classNode.getId())), "Field id " + fieldId
														+ " @ line " + declarationLine + " already declared");
												offset.addOffset(fieldPar.getSize());
												return fieldPar;
											}
											
											@Override
											public FunNode createFunction(int declarationLine, String id, Node type, ArrayList<HashMap<String, STentry>> symbolTable, int nestingLevel, Offset offset) {
												FunNode f = new FunNode(id, type);
												HashMap<String, STentry> hm = symbolTable.get(nestingLevel);
												// come � memoriazzata la funzione nello stack?
												// lfp
												// push function
												// quindi l'offset della funzione sar� offset.getOffset() - 1
												STentry entry = new STentry(f, nestingLevel, offset.getOffset() - 1);
												offset.addOffset(-f.getSize());
												checkNotNull(hm.put(id, entry), "Fun id " + id + " at line " + declarationLine + " already declared");
												// creare una nuova hashmap per la symTable
												nestingLevel++;
												symbolTable.add(f.getActivationRecordEntries());
												return f;
											}
											
											@Override
											public MethodNode createMethodInClass(int declarationLine, ClassNode classNode, String id, Node type, ArrayList<HashMap<String, STentry>> symbolTable,
													HashMap<String, STentry> map, int nestingLevel, Offset offset) {
												MethodNode method = new MethodNode(id, type);
												STentry functionEntry = new STentry(method, nestingLevel, offset.getOffset(), classNode.getId());
												checkNotNull(map.put(id, functionEntry), "Method id " + id + " @ line " + declarationLine + " already declared");
												offset.addOffset(method.getSize());
												symbolTable.add(method.getActivationRecordEntries());
												return method;
											}
											
											@Override
											public VarNode createVariable(int declarationLine, String id, Node type, Node value, ArrayList<HashMap<String, STentry>> symbolTable, int nestingLevel,
													Offset offset) {
												VarNode v = new VarNode(id, type, value);
												HashMap<String, STentry> hm = symbolTable.get(nestingLevel);
												int off = offset.getOffset();
												// una variabile pu� essere anche una funzione, prima della label
												// della funzione nello stack trovo sempre il suo fp, devo tenerne conto
												// tramite off --
												if (type instanceof ArrowTypeNode)
													off--;
												STentry entry = new STentry(v, nestingLevel, type, off);
												offset.addOffset(-v.getSize());
												checkNotNull(hm.put(id, entry), "Var id " + id + " at line " + declarationLine + " already declared");
												entry.setType(type);
												return v;
											}
										};
	
	private static String		funCode	= Syskb.CR;
	
	private static int			label	= -1;
	
	private static void checkNotNull(Object object, String errorMessage) {
		if (!isNull(object)) {
			exit(errorMessage);
		}
	}
	
	private static void checkNull(Object object, String errorMessage) {
		if (isNull(object)) {
			exit(errorMessage);
		}
	}
	
	public static void exit(String message) {
		// System.err.println(message);
		throw new RuntimeException(message);
	}
	
	public static String getCode() {
		return FOOLlib.funCode;
	}
	
	public static String getLabelId() {
		return FOOLlib.getLabelId("label");
	}
	
	public static String getLabelId(final String s) {
		return s + (++FOOLlib.label);
	}
	
	private static boolean isNull(Object object) {
		return object == null;
	}
	
	public static boolean isSubtype(final Node subType, final Node superType) {
		boolean ret = false;
		if ((subType.toPrint("")).equals(superType.toPrint(""))) {
			ret = true;
		} else if ((subType.toPrint("")).equals(Syskb.BOOLTYPE) && (superType.toPrint("")).equals(Syskb.INTTYPE)) {
			ret = true;
		} else if ((subType.toPrint("")).startsWith(superType.toPrint(""))) {
			// Controllo che serve per il type check delle classi
			// Esempio sottoclasse Object -> Integer -> Double
			// Esempio superclasse Object -> Integer
			// Se la sottoclasse inizia con la super classe allora � sottotipo
			ret = true;
		} else if (subType.toPrint("").equals(Syskb.NULLTYPE) && superType.toPrint("").startsWith(Syskb.OBJECT)) {
			// Se il valore � NULL e il supertipo � una classe...
			ret = true;
		} else if (subType instanceof ArrowTypeNode && superType instanceof ArrowTypeNode) {
			// Vedi le specifiche del progetto su covarianza e controvarianza
			ArrayList<Node> parAttuali = ((ArrowTypeNode) subType).getPar();
			ArrayList<Node> parFormali = ((ArrowTypeNode) superType).getPar();
			for (int i = 0; i < ((ArrowTypeNode) superType).getPar().size(); i++) {
				if (!isSupertype(parAttuali.get(i), parFormali.get(i)))
					return false;
			}
			ret = isSubtype(((ArrowTypeNode) subType).getRet().typeCheck(), ((ArrowTypeNode) superType).getRet().typeCheck());
		}
		return ret;
	}
	
	public static boolean isSupertype(final Node superType, final Node subType) {
		return isSubtype(subType, superType);
	}
	
	public static void putCode(final String c) {
		FOOLlib.funCode += c + Syskb.CR;
	}
	
	public static void resetLabel() {
		FOOLlib.label = -1;
		funCode = Syskb.CR;
		Syskb.reset();
	}
	
	public static void setFather(ArrayList<Node> nodes, INode father) {
		for (INode n : nodes)
			setFather(n, father);
	}
	
	public static void setFather(INode node, INode father) {
		node.setFather(father);
		if (node.getSTEntry() != null)
			node.getSTEntry().setContest(father);
	}
}
