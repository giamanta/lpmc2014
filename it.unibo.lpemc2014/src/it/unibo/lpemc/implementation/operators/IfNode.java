package it.unibo.lpemc.implementation.operators;

import it.unibo.lpemc.implementation.FOOLlib;
import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.implementation.types.BoolTypeNode;
import it.unibo.lpemc.interfaces.MergeNode;
import it.unibo.lpemc.interfaces.Node;

public class IfNode extends Node {

	private final Node		cond;
	private final MergeNode	condition;
	private final Node		el;
	private final MergeNode	els;
	private final Node		th;
	private final MergeNode	then;

	public IfNode(final Node c, final Node t, final Node e) {
		super("if");
		this.cond = c;
		this.condition = new MergeNode(Syskb.IF, this.cond);
		this.th = t;
		this.then = new MergeNode(Syskb.THEN, this.th);
		this.el = e;
		this.els = new MergeNode(Syskb.ELSE, this.el);
	}

	@Override
	protected String codeGen() {
		final String labelThen = FOOLlib.getLabelId("then");
		final String labelDone = FOOLlib.getLabelId("done");
		return this.cond.codeGeneration() + Syskb.PUSH + "1" + Syskb.CR + Syskb.BRANCHEQ + labelThen + Syskb.CR + this.el.codeGeneration() + Syskb.BRANCH + labelDone + Syskb.CR + labelThen + ":"
		+ Syskb.CR + this.th.codeGeneration() + labelDone + ":" + Syskb.CR;
	}

	@Override
	public String toPrint(final String s) {
		return this.condition.toPrint(s + Syskb.INDENT) + this.then.toPrint(s + Syskb.INDENT) + this.els.toPrint(s + Syskb.INDENT);
	}

	@Override
	public Node typeCheck() {
		if (!(FOOLlib.isSubtype(this.cond.typeCheck(), new BoolTypeNode()))) {
			FOOLlib.exit("Non boolean condition in if");
		}
		final Node t = this.th.typeCheck();
		final Node e = this.el.typeCheck();
		if (FOOLlib.isSubtype(t, e)) {
			return e;
		}
		if (FOOLlib.isSubtype(e, t)) {
			return t;
		}
		FOOLlib.exit("Incompatible types in then else branches");
		return null;
	}
}
