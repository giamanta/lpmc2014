package it.unibo.lpemc.implementation.operators;

import it.unibo.lpemc.implementation.FOOLlib;
import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.implementation.types.BoolTypeNode;
import it.unibo.lpemc.interfaces.Node;

public class GreaterEqualNode extends Node {

	private final Node	left;
	private final Node	right;

	public GreaterEqualNode(final Node l, final Node r) {
		super("greaterEqual");
		this.left = l;
		this.right = r;
	}

	@Override
	protected String codeGen() {
		final String less = FOOLlib.getLabelId("less");
		final String notEq = FOOLlib.getLabelId("lessEq");
		final String equal = FOOLlib.getLabelId("greatEqual");
		return this.left.codeGeneration() + this.right.codeGeneration() + Syskb.BRANCHEQ + equal + Syskb.CR + // if ==
        this.left.codeGeneration() + this.right.codeGeneration() + Syskb.BRANCHLESS + less + Syskb.CR +
        equal + ":" + Syskb.CR +
        Syskb.PUSH + "1" + Syskb.CR + // if >
        Syskb.BRANCH + notEq + Syskb.CR +
        less + ":" + Syskb.CR +
        Syskb.PUSH + "0" + Syskb.CR +
        notEq + ":" + Syskb.CR;
	}

	@Override
	public String toPrint(final String s) {
		return s + "GreaterEqual\n" + this.left.toPrint(s + "  ") + this.right.toPrint(s + "  ");
	}

	@Override
	public Node typeCheck() {
		final Node l = this.left.typeCheck();
		final Node r = this.right.typeCheck();
		if (!(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l))) {
			FOOLlib.exit("Incompatible types in greaterEqual");
		}
		return new BoolTypeNode();
	}
}
