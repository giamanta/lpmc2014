package it.unibo.lpemc.implementation.operators;

import it.unibo.lpemc.implementation.FOOLlib;
import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.implementation.types.BoolTypeNode;
import it.unibo.lpemc.interfaces.Node;

public class NotNode extends Node {

	private final Node	n;

	public NotNode(final Node expressionAstInput) {
		super("not");
		this.n = expressionAstInput;
	}

	@Override
	protected String codeGen() {
		final String equalTrue = FOOLlib.getLabelId("isTrue");
		final String equalFalse = FOOLlib.getLabelId("isFalse");
		return this.n.codeGeneration() + Syskb.PUSH + "1" + Syskb.CR +
      Syskb.BRANCHEQ + equalTrue + Syskb.CR +
      Syskb.PUSH + "1" + Syskb.CR +
      Syskb.BRANCH + equalFalse + Syskb.CR +
      equalTrue + ":" + Syskb.CR +
      Syskb.PUSH + "0" + Syskb.CR +
      equalFalse + ":" + Syskb.CR;
	}

	@Override
	public String toPrint(final String s) {
		return s + Syskb.NOT + "(" + this.n.toPrint(s + "  ") + ")";
	}

	@Override
	public Node typeCheck() {
		if (!(FOOLlib.isSubtype(this.n.typeCheck(), new BoolTypeNode()))) {
			FOOLlib.exit("Non boolean in stack");
		}
		return new BoolTypeNode();
	}
}
