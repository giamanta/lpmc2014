package it.unibo.lpemc.implementation.operators;

import it.unibo.lpemc.implementation.FOOLlib;
import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.implementation.types.BoolTypeNode;
import it.unibo.lpemc.interfaces.Node;

public class AndNode extends Node {

	private final Node	left;
	private final Node	right;

	public AndNode(final Node l, final Node r) {
		super("&&");
		this.left = l;
		this.right = r;
	}

	@Override
	protected String codeGen() {
		final String equalTrue1 = FOOLlib.getLabelId("leftEqTrue");
		final String equalTrue2 = FOOLlib.getLabelId("bothEqTrue");
		final String equalFalse = FOOLlib.getLabelId("oneEqFalse");
		return this.left.codeGeneration() + Syskb.PUSH + "1" + Syskb.CR +
        Syskb.BRANCHEQ + equalTrue1 + Syskb.CR +
        Syskb.PUSH + "0" + Syskb.CR +
        Syskb.BRANCH + equalFalse + Syskb.CR +
        equalTrue1 + ":" + Syskb.CR +
        this.right.codeGeneration() + Syskb.PUSH + "1" + Syskb.CR +
        Syskb.BRANCHEQ + equalTrue2 + Syskb.CR +
        Syskb.PUSH + "0" + Syskb.CR +
        Syskb.BRANCH + equalFalse + Syskb.CR +
        equalTrue2 + ":" + Syskb.CR +
        Syskb.PUSH + "1" + Syskb.CR +
        equalFalse + ":" + Syskb.CR;
	}

	@Override
	public String toPrint(final String s) {
		return s + this.left.toPrint(s + "  ") + Syskb.AND + this.right.toPrint(s + "  ");
	}

	@Override
	public Node typeCheck() {
    if (!(FOOLlib.isSubtype(this.left.typeCheck(), new BoolTypeNode()) &&
          FOOLlib.isSubtype(this.right.typeCheck(), new BoolTypeNode()))) {
    		FOOLlib.exit("No boolean in &&");
		}
		return new BoolTypeNode();
	}
}
