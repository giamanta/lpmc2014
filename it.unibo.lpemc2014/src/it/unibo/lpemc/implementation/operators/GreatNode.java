package it.unibo.lpemc.implementation.operators;

import it.unibo.lpemc.implementation.FOOLlib;
import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.implementation.types.BoolTypeNode;
import it.unibo.lpemc.interfaces.Node;

public class GreatNode extends Node {

	private final Node	left;
	private final Node	right;

	public GreatNode(final Node l, final Node r) {
		super("Great");
		this.left = l;
		this.right = r;
	}

	@Override
	protected String codeGen() {
		final String less = FOOLlib.getLabelId("lessEqual");
		final String great = FOOLlib.getLabelId("great");
		return this.left.codeGeneration() + this.right.codeGeneration() + Syskb.BRANCHLESS + less + Syskb.CR +
        Syskb.PUSH + "1" + Syskb.CR +
        Syskb.BRANCH + great + Syskb.CR +
        less + ":" + Syskb.CR +
        Syskb.PUSH + "0" + Syskb.CR +
        great + ":" + Syskb.CR;
	}

	@Override
	public String toPrint(final String s) {
		return s + "Great (>)\n" + this.left.toPrint(s + "  ") + this.right.toPrint(s + "  ");
	}

	@Override
	public Node typeCheck() {
		final Node l = this.left.typeCheck();
		final Node r = this.right.typeCheck();
		if (!(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l))) {
			FOOLlib.exit("Incompatible types in Great");
		}
		return new BoolTypeNode();
	}
}
