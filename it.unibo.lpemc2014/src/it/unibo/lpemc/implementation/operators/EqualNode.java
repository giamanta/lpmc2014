package it.unibo.lpemc.implementation.operators;

import it.unibo.lpemc.implementation.FOOLlib;
import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.implementation.types.BoolTypeNode;
import it.unibo.lpemc.interfaces.Node;

public class EqualNode extends Node {

	private final Node	left;
	private final Node	right;

	public EqualNode(final Node l, final Node r) {
		super("equal");
		this.left = l;
		this.right = r;
	}

	@Override
	protected String codeGen() {
//		final String equal = FOOLlib.getLabelId("==_n");
//		final String notEqual = FOOLlib.getLabelId("!=_n");
		final String equal = FOOLlib.getLabelId("equal");
		final String notEqual = FOOLlib.getLabelId("notEqual");
		return this.left.codeGeneration() + this.right.codeGeneration() + Syskb.BRANCHEQ + equal + Syskb.CR + Syskb.PUSH + "0" + Syskb.CR + Syskb.BRANCH + notEqual + Syskb.CR + equal + ":" + Syskb.CR
				+ Syskb.PUSH + "1" + Syskb.CR + notEqual + ":" + Syskb.CR;
	}

	@Override
	public String toPrint(final String s) {
		return s + "Equal\n" + this.left.toPrint(s + "  ") + this.right.toPrint(s + "  ");
	}

	@Override
	public Node typeCheck() {
		final Node l = this.left.typeCheck();
		final Node r = this.right.typeCheck();
		if (!(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l))) {
			FOOLlib.exit("Incompatible types in equal");
		}
		return new BoolTypeNode();
	}
}
