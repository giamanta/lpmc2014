package it.unibo.lpemc.implementation.operators;

import it.unibo.lpemc.implementation.FOOLlib;
import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.implementation.types.BoolTypeNode;
import it.unibo.lpemc.interfaces.Node;

public class OrNode extends Node {

	private final Node	left;
	private final Node	right;

	public OrNode(final Node l, final Node r) {
		super("or");
		this.left = l;
		this.right = r;
	}

	@Override
	protected String codeGen() {
		final String equalTrue = FOOLlib.getLabelId("oneTrue");
		final String equalFalse = FOOLlib.getLabelId("bothFalse");
		return this.left.codeGeneration() + Syskb.PUSH + "1" + Syskb.CR +
        Syskb.BRANCHEQ + equalTrue + Syskb.CR +
        this.right.codeGeneration() + Syskb.PUSH + "1" + Syskb.CR +
        Syskb.BRANCHEQ + equalTrue + Syskb.CR +
        Syskb.PUSH + "0" + Syskb.CR +
        Syskb.BRANCH + equalFalse + Syskb.CR +
        equalTrue + ":" + Syskb.CR +
        Syskb.PUSH + "1" + Syskb.CR +
        equalFalse + ":" + Syskb.CR;
	}

	@Override
	public String toPrint(final String s) {
		return s + this.left.toPrint(s + "  ") + Syskb.OR + this.right.toPrint(s + "  ");
	}

	@Override
	public Node typeCheck() {
    if (!(FOOLlib.isSubtype(this.left.typeCheck(), new BoolTypeNode()) &&
          FOOLlib.isSubtype(this.right.typeCheck(), new BoolTypeNode()))) {
    		FOOLlib.exit("Non booleans in ||");
		}
		return new BoolTypeNode();
	}
}
