package it.unibo.lpemc.implementation.operators;

import it.unibo.lpemc.implementation.FOOLlib;
import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.implementation.types.IntTypeNode;
import it.unibo.lpemc.interfaces.Node;

public class SubNode extends Node {
	
	private final Node	left;
	private final Node	right;
	
	public SubNode(final Node l, final Node r) {
		super("sub");
		this.left = l;
		this.right = r;
	}
	
	@Override
	protected String codeGen() {
		return this.left.codeGeneration() + this.right.codeGeneration() + Syskb.SUB;
	}
	
	@Override
	public String toPrint(final String s) {
		return s + Syskb.SUB + this.left.toPrint(s + "  ") + this.right.toPrint(s + "  ");
	}
	
	@Override
	public Node typeCheck() {
		if (!(FOOLlib.isSubtype(this.left.typeCheck(), new IntTypeNode()) && FOOLlib.isSubtype(this.right.typeCheck(), new IntTypeNode()))) {
			FOOLlib.exit("Non integers in sub");
		}
		return new IntTypeNode();
	}
}
