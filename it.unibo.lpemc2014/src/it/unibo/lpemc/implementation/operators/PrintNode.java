package it.unibo.lpemc.implementation.operators;

import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.interfaces.Node;

public class PrintNode extends Node {
	
	private final Node	val;
	
	public PrintNode(final Node v) {
		super(Syskb.PRINT);
		this.val = v;
	}
	
	@Override
	protected String codeGen() {
		return this.val.codeGeneration() + Syskb.PRINT + "\n";
	}
	
	@Override
	public String toPrint(final String s) {
		return s + Syskb.PRINT + "\n" + this.val.toPrint(s + Syskb.INDENT);
	}
	
	@Override
	public Node typeCheck() {
		return this.val.typeCheck();
	}
}