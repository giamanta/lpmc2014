package it.unibo.lpemc.implementation.operators;

import it.unibo.lpemc.implementation.FOOLlib;
import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.implementation.types.IntTypeNode;
import it.unibo.lpemc.interfaces.Node;

public class AddNode extends Node {
	
	private final Node	left;
	private final Node	right;
	
	public AddNode(final Node l, final Node r) {
		super("add");
		this.left = l;
		this.right = r;
	}
	
	@Override
	protected String codeGen() {
		return this.left.codeGeneration() + this.right.codeGeneration() + Syskb.ADD;
	}
	
	@Override
	public String toPrint(final String s) {
		return s + Syskb.ADD + this.left.toPrint(s + Syskb.INDENT) + this.right.toPrint(s + Syskb.INDENT);
	}
	
	@Override
	public Node typeCheck() {
		if (!(FOOLlib.isSubtype(this.left.typeCheck(), new IntTypeNode()) && FOOLlib.isSubtype(this.right.typeCheck(), new IntTypeNode()))) {
			FOOLlib.exit("Non integers in add");
		}
		return new IntTypeNode();
	}
	
}
