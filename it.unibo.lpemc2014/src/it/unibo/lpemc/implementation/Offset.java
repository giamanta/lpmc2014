package it.unibo.lpemc.implementation;

public class Offset {
	private int	offset;
	
	public Offset(int offset) {
		this.offset = offset;
	}
	
	public void addOffset(int offset) {
		this.offset += offset;
	}
	
	public int getOffset() {
		return offset;
	}
}
