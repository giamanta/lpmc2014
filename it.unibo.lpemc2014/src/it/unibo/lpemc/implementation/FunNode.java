package it.unibo.lpemc.implementation;

import java.util.ArrayList;

import it.unibo.lpemc.implementation.types.ArrowTypeNode;
import it.unibo.lpemc.interfaces.Node;

public class FunNode extends MethodNode {
	
	public FunNode(String i, Node t) {
		super(i, t);
	}
	
	@Override
	protected String codeGen() {
		return Syskb.LOADFP + 
				super.codeGen();
	}
	
	@Override
	public int getSize() {
		return 2; // etichetta + possibile al
	}
	
	@Override
	public Node typeCheck() {
		final ArrayList<Node> pt = new ArrayList<Node>();
		for (int i = 0; i < this.parameters.size(); i++) {
			pt.add((this.parameters.get(i)).typeCheck());
		}
		for (int j = 0; j < this.localDeclarations.size(); j++) {
			(this.localDeclarations.get(j)).typeCheck();
		}
		if (!(FOOLlib.isSubtype(this.body.typeCheck(), this.type))) {
			FOOLlib.exit("Wrong return type for function " + this.id);
		}
		return new ArrowTypeNode(pt, this.type);
	}
}
