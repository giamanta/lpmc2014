package it.unibo.lpemc.implementation;

import it.unibo.lpemc.implementation.types.ArrowTypeNode;
import it.unibo.lpemc.interfaces.Node;

public class IdNode extends Node {
	protected final int	nestingLevelDifference;
	
	public IdNode(final String i, final STentry st, final int nestingLevelDifference) {
		super(i, st);
		this.nestingLevelDifference = nestingLevelDifference;
	}
	
	@Override
	protected String codeGen() {
		String getAR = Syskb.LOADFP, ret = "";
		for (int i = 0; i < this.nestingLevelDifference; i++) {
			getAR += Syskb.LOADW;
		}
		// l'id � una variabile
		ret = Syskb.PUSH + getSTEntry().getOffset() + Syskb.CR + // offset variabile in AR
				getAR + // carica il record di attivazione
				Syskb.ADD + // li sommo
				Syskb.LOADW; // vado a prendere il contenuto della variabile
		
		if (getSTEntry().getType() instanceof ArrowTypeNode)
			//id � una funzione, devo caricare anche l'AL
			ret = getAR + // riferimento a AR
			Syskb.PUSH + (getSTEntry().getOffset() + 1) + Syskb.CR + // offset di lfp 
			Syskb.ADD + // sommo i valori
			Syskb.LOADW + // carico l'AL della funzione
			ret;

		return ret;
	}
	
	@Override
	public String toPrint(final String s) {
		return s + "id:" + this.id + " type:" + getSTEntry().getType().toPrint("") + " nl_diff:" + this.nestingLevelDifference;
	}
	
	@Override
	public Node typeCheck() {
		return getSTEntry().getType();
	}
}