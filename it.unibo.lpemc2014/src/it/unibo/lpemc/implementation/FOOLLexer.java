// $ANTLR 3.5.1 C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g 2015-03-13 12:03:37
package it.unibo.lpemc.implementation;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLLexer extends Lexer {
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ARROW=5;
	public static final int ASS=6;
	public static final int BOOL=7;
	public static final int CLASS=8;
	public static final int CLPAR=9;
	public static final int COLON=10;
	public static final int COMMA=11;
	public static final int COMMENT=12;
	public static final int CRPAR=13;
	public static final int DIV=14;
	public static final int DOT=15;
	public static final int ELSE=16;
	public static final int EQ=17;
	public static final int ERR=18;
	public static final int EXTENDS=19;
	public static final int FALSE=20;
	public static final int FUN=21;
	public static final int GR=22;
	public static final int GREAT=23;
	public static final int ICOMMENT=24;
	public static final int ID=25;
	public static final int IF=26;
	public static final int IN=27;
	public static final int INT=28;
	public static final int INTEGER=29;
	public static final int LE=30;
	public static final int LESS=31;
	public static final int LET=32;
	public static final int LPAR=33;
	public static final int MINUS=34;
	public static final int MULT=35;
	public static final int NEW=36;
	public static final int NOT=37;
	public static final int NULL=38;
	public static final int OR=39;
	public static final int PLUS=40;
	public static final int PRINT=41;
	public static final int RPAR=42;
	public static final int SEMIC=43;
	public static final int THEN=44;
	public static final int TRUE=45;
	public static final int VAR=46;
	public static final int WHITESP=47;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public FOOLLexer() {} 
	public FOOLLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g"; }

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:270:9: ( '+' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:270:11: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:271:9: ( '-' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:271:11: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "MULT"
	public final void mMULT() throws RecognitionException {
		try {
			int _type = MULT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:272:9: ( '*' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:272:11: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MULT"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:273:9: ( '/' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:273:11: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	// $ANTLR start "LPAR"
	public final void mLPAR() throws RecognitionException {
		try {
			int _type = LPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:274:9: ( '(' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:274:11: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LPAR"

	// $ANTLR start "RPAR"
	public final void mRPAR() throws RecognitionException {
		try {
			int _type = RPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:275:9: ( ')' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:275:11: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RPAR"

	// $ANTLR start "CLPAR"
	public final void mCLPAR() throws RecognitionException {
		try {
			int _type = CLPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:276:9: ( '{' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:276:11: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CLPAR"

	// $ANTLR start "CRPAR"
	public final void mCRPAR() throws RecognitionException {
		try {
			int _type = CRPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:277:9: ( '}' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:277:11: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CRPAR"

	// $ANTLR start "SEMIC"
	public final void mSEMIC() throws RecognitionException {
		try {
			int _type = SEMIC;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:278:9: ( ';' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:278:11: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SEMIC"

	// $ANTLR start "COLON"
	public final void mCOLON() throws RecognitionException {
		try {
			int _type = COLON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:279:9: ( ':' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:279:11: ':'
			{
			match(':'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COLON"

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			int _type = COMMA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:280:9: ( ',' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:280:11: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMA"

	// $ANTLR start "DOT"
	public final void mDOT() throws RecognitionException {
		try {
			int _type = DOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:281:9: ( '.' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:281:11: '.'
			{
			match('.'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DOT"

	// $ANTLR start "OR"
	public final void mOR() throws RecognitionException {
		try {
			int _type = OR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:282:9: ( '||' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:282:11: '||'
			{
			match("||"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OR"

	// $ANTLR start "AND"
	public final void mAND() throws RecognitionException {
		try {
			int _type = AND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:283:9: ( '&&' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:283:11: '&&'
			{
			match("&&"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AND"

	// $ANTLR start "NOT"
	public final void mNOT() throws RecognitionException {
		try {
			int _type = NOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:284:9: ( 'not' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:284:11: 'not'
			{
			match("not"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOT"

	// $ANTLR start "GR"
	public final void mGR() throws RecognitionException {
		try {
			int _type = GR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:285:9: ( '>=' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:285:11: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GR"

	// $ANTLR start "GREAT"
	public final void mGREAT() throws RecognitionException {
		try {
			int _type = GREAT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:286:9: ( '>' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:286:11: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GREAT"

	// $ANTLR start "LE"
	public final void mLE() throws RecognitionException {
		try {
			int _type = LE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:287:9: ( '<=' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:287:11: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LE"

	// $ANTLR start "LESS"
	public final void mLESS() throws RecognitionException {
		try {
			int _type = LESS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:288:9: ( '<' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:288:11: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LESS"

	// $ANTLR start "EQ"
	public final void mEQ() throws RecognitionException {
		try {
			int _type = EQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:289:9: ( '==' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:289:11: '=='
			{
			match("=="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EQ"

	// $ANTLR start "ASS"
	public final void mASS() throws RecognitionException {
		try {
			int _type = ASS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:290:9: ( '=' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:290:11: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASS"

	// $ANTLR start "TRUE"
	public final void mTRUE() throws RecognitionException {
		try {
			int _type = TRUE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:291:9: ( 'true' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:291:11: 'true'
			{
			match("true"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TRUE"

	// $ANTLR start "FALSE"
	public final void mFALSE() throws RecognitionException {
		try {
			int _type = FALSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:292:9: ( 'false' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:292:11: 'false'
			{
			match("false"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FALSE"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			int _type = IF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:293:9: ( 'if' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:293:11: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IF"

	// $ANTLR start "THEN"
	public final void mTHEN() throws RecognitionException {
		try {
			int _type = THEN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:294:9: ( 'then' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:294:11: 'then'
			{
			match("then"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "THEN"

	// $ANTLR start "ELSE"
	public final void mELSE() throws RecognitionException {
		try {
			int _type = ELSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:295:9: ( 'else' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:295:11: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ELSE"

	// $ANTLR start "PRINT"
	public final void mPRINT() throws RecognitionException {
		try {
			int _type = PRINT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:296:9: ( 'print' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:296:11: 'print'
			{
			match("print"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PRINT"

	// $ANTLR start "LET"
	public final void mLET() throws RecognitionException {
		try {
			int _type = LET;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:297:9: ( 'let' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:297:11: 'let'
			{
			match("let"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LET"

	// $ANTLR start "IN"
	public final void mIN() throws RecognitionException {
		try {
			int _type = IN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:298:9: ( 'in' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:298:11: 'in'
			{
			match("in"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IN"

	// $ANTLR start "VAR"
	public final void mVAR() throws RecognitionException {
		try {
			int _type = VAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:299:9: ( 'var' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:299:11: 'var'
			{
			match("var"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VAR"

	// $ANTLR start "FUN"
	public final void mFUN() throws RecognitionException {
		try {
			int _type = FUN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:300:9: ( 'fun' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:300:11: 'fun'
			{
			match("fun"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FUN"

	// $ANTLR start "CLASS"
	public final void mCLASS() throws RecognitionException {
		try {
			int _type = CLASS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:301:9: ( 'class' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:301:11: 'class'
			{
			match("class"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CLASS"

	// $ANTLR start "EXTENDS"
	public final void mEXTENDS() throws RecognitionException {
		try {
			int _type = EXTENDS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:302:9: ( 'extends' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:302:11: 'extends'
			{
			match("extends"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EXTENDS"

	// $ANTLR start "NEW"
	public final void mNEW() throws RecognitionException {
		try {
			int _type = NEW;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:303:9: ( 'new' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:303:11: 'new'
			{
			match("new"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEW"

	// $ANTLR start "NULL"
	public final void mNULL() throws RecognitionException {
		try {
			int _type = NULL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:304:9: ( 'null' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:304:11: 'null'
			{
			match("null"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NULL"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:305:9: ( 'int' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:305:11: 'int'
			{
			match("int"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "BOOL"
	public final void mBOOL() throws RecognitionException {
		try {
			int _type = BOOL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:306:9: ( 'bool' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:306:11: 'bool'
			{
			match("bool"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BOOL"

	// $ANTLR start "ARROW"
	public final void mARROW() throws RecognitionException {
		try {
			int _type = ARROW;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:307:9: ( '->' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:307:11: '->'
			{
			match("->"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARROW"

	// $ANTLR start "INTEGER"
	public final void mINTEGER() throws RecognitionException {
		try {
			int _type = INTEGER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:308:9: ( ( ( '1' .. '9' ) ( '0' .. '9' )* ) | '0' )
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( ((LA2_0 >= '1' && LA2_0 <= '9')) ) {
				alt2=1;
			}
			else if ( (LA2_0=='0') ) {
				alt2=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}

			switch (alt2) {
				case 1 :
					// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:308:11: ( ( '1' .. '9' ) ( '0' .. '9' )* )
					{
					// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:308:11: ( ( '1' .. '9' ) ( '0' .. '9' )* )
					// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:308:12: ( '1' .. '9' ) ( '0' .. '9' )*
					{
					if ( (input.LA(1) >= '1' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:308:22: ( '0' .. '9' )*
					loop1:
					while (true) {
						int alt1=2;
						int LA1_0 = input.LA(1);
						if ( ((LA1_0 >= '0' && LA1_0 <= '9')) ) {
							alt1=1;
						}

						switch (alt1) {
						case 1 :
							// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop1;
						}
					}

					}

					}
					break;
				case 2 :
					// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:308:37: '0'
					{
					match('0'); 
					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INTEGER"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:309:9: ( ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )* )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:309:11: ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:309:30: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( ((LA3_0 >= '0' && LA3_0 <= '9')||(LA3_0 >= 'A' && LA3_0 <= 'Z')||(LA3_0 >= 'a' && LA3_0 <= 'z')) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop3;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "WHITESP"
	public final void mWHITESP() throws RecognitionException {
		try {
			int _type = WHITESP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:310:9: ( ( '\\t' | ' ' | '\\r' | '\\n' )+ )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:310:11: ( '\\t' | ' ' | '\\r' | '\\n' )+
			{
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:310:11: ( '\\t' | ' ' | '\\r' | '\\n' )+
			int cnt4=0;
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( ((LA4_0 >= '\t' && LA4_0 <= '\n')||LA4_0=='\r'||LA4_0==' ') ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt4 >= 1 ) break loop4;
					EarlyExitException eee = new EarlyExitException(4, input);
					throw eee;
				}
				cnt4++;
			}

			 skip(); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHITESP"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:312:9: ( '/*' ( options {greedy=false; } : . )* '*/' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:312:11: '/*' ( options {greedy=false; } : . )* '*/'
			{
			match("/*"); 

			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:312:16: ( options {greedy=false; } : . )*
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( (LA5_0=='*') ) {
					int LA5_1 = input.LA(2);
					if ( (LA5_1=='/') ) {
						alt5=2;
					}
					else if ( ((LA5_1 >= '\u0000' && LA5_1 <= '.')||(LA5_1 >= '0' && LA5_1 <= '\uFFFF')) ) {
						alt5=1;
					}

				}
				else if ( ((LA5_0 >= '\u0000' && LA5_0 <= ')')||(LA5_0 >= '+' && LA5_0 <= '\uFFFF')) ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:312:43: .
					{
					matchAny(); 
					}
					break;

				default :
					break loop5;
				}
			}

			match("*/"); 

			 skip(); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "ICOMMENT"
	public final void mICOMMENT() throws RecognitionException {
		try {
			int _type = ICOMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:313:9: ( '//' ( options {greedy=false; } : . )* ( '\\r' )? '\\n' )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:313:11: '//' ( options {greedy=false; } : . )* ( '\\r' )? '\\n'
			{
			match("//"); 

			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:313:16: ( options {greedy=false; } : . )*
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( (LA6_0=='\r') ) {
					alt6=2;
				}
				else if ( (LA6_0=='\n') ) {
					alt6=2;
				}
				else if ( ((LA6_0 >= '\u0000' && LA6_0 <= '\t')||(LA6_0 >= '\u000B' && LA6_0 <= '\f')||(LA6_0 >= '\u000E' && LA6_0 <= '\uFFFF')) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:313:43: .
					{
					matchAny(); 
					}
					break;

				default :
					break loop6;
				}
			}

			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:313:47: ( '\\r' )?
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( (LA7_0=='\r') ) {
				alt7=1;
			}
			switch (alt7) {
				case 1 :
					// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:313:47: '\\r'
					{
					match('\r'); 
					}
					break;

			}

			match('\n'); 
			 skip(); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ICOMMENT"

	// $ANTLR start "ERR"
	public final void mERR() throws RecognitionException {
		try {
			int _type = ERR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:314:8: ( . )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:314:10: .
			{
			matchAny(); 
			System.err.println("Invalid char: " + getText());
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ERR"

	@Override
	public void mTokens() throws RecognitionException {
		// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:8: ( PLUS | MINUS | MULT | DIV | LPAR | RPAR | CLPAR | CRPAR | SEMIC | COLON | COMMA | DOT | OR | AND | NOT | GR | GREAT | LE | LESS | EQ | ASS | TRUE | FALSE | IF | THEN | ELSE | PRINT | LET | IN | VAR | FUN | CLASS | EXTENDS | NEW | NULL | INT | BOOL | ARROW | INTEGER | ID | WHITESP | COMMENT | ICOMMENT | ERR )
		int alt8=44;
		alt8 = dfa8.predict(input);
		switch (alt8) {
			case 1 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:10: PLUS
				{
				mPLUS(); 

				}
				break;
			case 2 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:15: MINUS
				{
				mMINUS(); 

				}
				break;
			case 3 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:21: MULT
				{
				mMULT(); 

				}
				break;
			case 4 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:26: DIV
				{
				mDIV(); 

				}
				break;
			case 5 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:30: LPAR
				{
				mLPAR(); 

				}
				break;
			case 6 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:35: RPAR
				{
				mRPAR(); 

				}
				break;
			case 7 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:40: CLPAR
				{
				mCLPAR(); 

				}
				break;
			case 8 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:46: CRPAR
				{
				mCRPAR(); 

				}
				break;
			case 9 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:52: SEMIC
				{
				mSEMIC(); 

				}
				break;
			case 10 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:58: COLON
				{
				mCOLON(); 

				}
				break;
			case 11 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:64: COMMA
				{
				mCOMMA(); 

				}
				break;
			case 12 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:70: DOT
				{
				mDOT(); 

				}
				break;
			case 13 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:74: OR
				{
				mOR(); 

				}
				break;
			case 14 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:77: AND
				{
				mAND(); 

				}
				break;
			case 15 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:81: NOT
				{
				mNOT(); 

				}
				break;
			case 16 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:85: GR
				{
				mGR(); 

				}
				break;
			case 17 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:88: GREAT
				{
				mGREAT(); 

				}
				break;
			case 18 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:94: LE
				{
				mLE(); 

				}
				break;
			case 19 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:97: LESS
				{
				mLESS(); 

				}
				break;
			case 20 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:102: EQ
				{
				mEQ(); 

				}
				break;
			case 21 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:105: ASS
				{
				mASS(); 

				}
				break;
			case 22 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:109: TRUE
				{
				mTRUE(); 

				}
				break;
			case 23 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:114: FALSE
				{
				mFALSE(); 

				}
				break;
			case 24 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:120: IF
				{
				mIF(); 

				}
				break;
			case 25 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:123: THEN
				{
				mTHEN(); 

				}
				break;
			case 26 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:128: ELSE
				{
				mELSE(); 

				}
				break;
			case 27 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:133: PRINT
				{
				mPRINT(); 

				}
				break;
			case 28 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:139: LET
				{
				mLET(); 

				}
				break;
			case 29 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:143: IN
				{
				mIN(); 

				}
				break;
			case 30 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:146: VAR
				{
				mVAR(); 

				}
				break;
			case 31 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:150: FUN
				{
				mFUN(); 

				}
				break;
			case 32 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:154: CLASS
				{
				mCLASS(); 

				}
				break;
			case 33 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:160: EXTENDS
				{
				mEXTENDS(); 

				}
				break;
			case 34 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:168: NEW
				{
				mNEW(); 

				}
				break;
			case 35 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:172: NULL
				{
				mNULL(); 

				}
				break;
			case 36 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:177: INT
				{
				mINT(); 

				}
				break;
			case 37 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:181: BOOL
				{
				mBOOL(); 

				}
				break;
			case 38 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:186: ARROW
				{
				mARROW(); 

				}
				break;
			case 39 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:192: INTEGER
				{
				mINTEGER(); 

				}
				break;
			case 40 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:200: ID
				{
				mID(); 

				}
				break;
			case 41 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:203: WHITESP
				{
				mWHITESP(); 

				}
				break;
			case 42 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:211: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 43 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:219: ICOMMENT
				{
				mICOMMENT(); 

				}
				break;
			case 44 :
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:1:228: ERR
				{
				mERR(); 

				}
				break;

		}
	}


	protected DFA8 dfa8 = new DFA8(this);
	static final String DFA8_eotS =
		"\2\uffff\1\43\1\uffff\1\47\10\uffff\2\40\1\65\1\67\1\71\1\73\11\65\26"+
		"\uffff\3\65\7\uffff\4\65\1\122\1\124\7\65\2\uffff\1\134\1\135\4\65\1\142"+
		"\1\uffff\1\143\1\uffff\3\65\1\147\1\150\2\65\2\uffff\1\153\1\154\1\155"+
		"\1\65\2\uffff\1\157\2\65\2\uffff\1\65\1\163\3\uffff\1\164\1\uffff\1\65"+
		"\1\166\1\167\2\uffff\1\65\2\uffff\1\171\1\uffff";
	static final String DFA8_eofS =
		"\172\uffff";
	static final String DFA8_minS =
		"\1\0\1\uffff\1\76\1\uffff\1\52\10\uffff\1\174\1\46\1\145\3\75\1\150\1"+
		"\141\1\146\1\154\1\162\1\145\1\141\1\154\1\157\26\uffff\1\164\1\167\1"+
		"\154\7\uffff\1\165\1\145\1\154\1\156\2\60\1\163\1\164\1\151\1\164\1\162"+
		"\1\141\1\157\2\uffff\2\60\1\154\1\145\1\156\1\163\1\60\1\uffff\1\60\1"+
		"\uffff\2\145\1\156\2\60\1\163\1\154\2\uffff\3\60\1\145\2\uffff\1\60\1"+
		"\156\1\164\2\uffff\1\163\1\60\3\uffff\1\60\1\uffff\1\144\2\60\2\uffff"+
		"\1\163\2\uffff\1\60\1\uffff";
	static final String DFA8_maxS =
		"\1\uffff\1\uffff\1\76\1\uffff\1\57\10\uffff\1\174\1\46\1\165\3\75\1\162"+
		"\1\165\1\156\1\170\1\162\1\145\1\141\1\154\1\157\26\uffff\1\164\1\167"+
		"\1\154\7\uffff\1\165\1\145\1\154\1\156\2\172\1\163\1\164\1\151\1\164\1"+
		"\162\1\141\1\157\2\uffff\2\172\1\154\1\145\1\156\1\163\1\172\1\uffff\1"+
		"\172\1\uffff\2\145\1\156\2\172\1\163\1\154\2\uffff\3\172\1\145\2\uffff"+
		"\1\172\1\156\1\164\2\uffff\1\163\1\172\3\uffff\1\172\1\uffff\1\144\2\172"+
		"\2\uffff\1\163\2\uffff\1\172\1\uffff";
	static final String DFA8_acceptS =
		"\1\uffff\1\1\1\uffff\1\3\1\uffff\1\5\1\6\1\7\1\10\1\11\1\12\1\13\1\14"+
		"\17\uffff\2\47\1\50\1\51\1\54\1\1\1\46\1\2\1\3\1\52\1\53\1\4\1\5\1\6\1"+
		"\7\1\10\1\11\1\12\1\13\1\14\1\15\1\16\3\uffff\1\50\1\20\1\21\1\22\1\23"+
		"\1\24\1\25\15\uffff\1\47\1\51\7\uffff\1\30\1\uffff\1\35\7\uffff\1\17\1"+
		"\42\4\uffff\1\37\1\44\3\uffff\1\34\1\36\2\uffff\1\43\1\26\1\31\1\uffff"+
		"\1\32\3\uffff\1\45\1\27\1\uffff\1\33\1\40\1\uffff\1\41";
	static final String DFA8_specialS =
		"\1\0\171\uffff}>";
	static final String[] DFA8_transitionS = {
			"\11\40\2\37\2\40\1\37\22\40\1\37\5\40\1\16\1\40\1\5\1\6\1\3\1\1\1\13"+
			"\1\2\1\14\1\4\1\35\11\34\1\12\1\11\1\21\1\22\1\20\2\40\32\36\6\40\1\36"+
			"\1\33\1\32\1\36\1\26\1\24\2\36\1\25\2\36\1\30\1\36\1\17\1\36\1\27\3\36"+
			"\1\23\1\36\1\31\4\36\1\7\1\15\1\10\uff82\40",
			"",
			"\1\42",
			"",
			"\1\45\4\uffff\1\46",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\60",
			"\1\61",
			"\1\63\11\uffff\1\62\5\uffff\1\64",
			"\1\66",
			"\1\70",
			"\1\72",
			"\1\75\11\uffff\1\74",
			"\1\76\23\uffff\1\77",
			"\1\100\7\uffff\1\101",
			"\1\102\13\uffff\1\103",
			"\1\104",
			"\1\105",
			"\1\106",
			"\1\107",
			"\1\110",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\113",
			"\1\114",
			"\1\115",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\116",
			"\1\117",
			"\1\120",
			"\1\121",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			"\12\65\7\uffff\32\65\6\uffff\23\65\1\123\6\65",
			"\1\125",
			"\1\126",
			"\1\127",
			"\1\130",
			"\1\131",
			"\1\132",
			"\1\133",
			"",
			"",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			"\1\136",
			"\1\137",
			"\1\140",
			"\1\141",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			"",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			"",
			"\1\144",
			"\1\145",
			"\1\146",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			"\1\151",
			"\1\152",
			"",
			"",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			"\1\156",
			"",
			"",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			"\1\160",
			"\1\161",
			"",
			"",
			"\1\162",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			"",
			"",
			"",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			"",
			"\1\165",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			"",
			"",
			"\1\170",
			"",
			"",
			"\12\65\7\uffff\32\65\6\uffff\32\65",
			""
	};

	static final short[] DFA8_eot = DFA.unpackEncodedString(DFA8_eotS);
	static final short[] DFA8_eof = DFA.unpackEncodedString(DFA8_eofS);
	static final char[] DFA8_min = DFA.unpackEncodedStringToUnsignedChars(DFA8_minS);
	static final char[] DFA8_max = DFA.unpackEncodedStringToUnsignedChars(DFA8_maxS);
	static final short[] DFA8_accept = DFA.unpackEncodedString(DFA8_acceptS);
	static final short[] DFA8_special = DFA.unpackEncodedString(DFA8_specialS);
	static final short[][] DFA8_transition;

	static {
		int numStates = DFA8_transitionS.length;
		DFA8_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA8_transition[i] = DFA.unpackEncodedString(DFA8_transitionS[i]);
		}
	}

	protected class DFA8 extends DFA {

		public DFA8(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 8;
			this.eot = DFA8_eot;
			this.eof = DFA8_eof;
			this.min = DFA8_min;
			this.max = DFA8_max;
			this.accept = DFA8_accept;
			this.special = DFA8_special;
			this.transition = DFA8_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( PLUS | MINUS | MULT | DIV | LPAR | RPAR | CLPAR | CRPAR | SEMIC | COLON | COMMA | DOT | OR | AND | NOT | GR | GREAT | LE | LESS | EQ | ASS | TRUE | FALSE | IF | THEN | ELSE | PRINT | LET | IN | VAR | FUN | CLASS | EXTENDS | NEW | NULL | INT | BOOL | ARROW | INTEGER | ID | WHITESP | COMMENT | ICOMMENT | ERR );";
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			IntStream input = _input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA8_0 = input.LA(1);
						s = -1;
						if ( (LA8_0=='+') ) {s = 1;}
						else if ( (LA8_0=='-') ) {s = 2;}
						else if ( (LA8_0=='*') ) {s = 3;}
						else if ( (LA8_0=='/') ) {s = 4;}
						else if ( (LA8_0=='(') ) {s = 5;}
						else if ( (LA8_0==')') ) {s = 6;}
						else if ( (LA8_0=='{') ) {s = 7;}
						else if ( (LA8_0=='}') ) {s = 8;}
						else if ( (LA8_0==';') ) {s = 9;}
						else if ( (LA8_0==':') ) {s = 10;}
						else if ( (LA8_0==',') ) {s = 11;}
						else if ( (LA8_0=='.') ) {s = 12;}
						else if ( (LA8_0=='|') ) {s = 13;}
						else if ( (LA8_0=='&') ) {s = 14;}
						else if ( (LA8_0=='n') ) {s = 15;}
						else if ( (LA8_0=='>') ) {s = 16;}
						else if ( (LA8_0=='<') ) {s = 17;}
						else if ( (LA8_0=='=') ) {s = 18;}
						else if ( (LA8_0=='t') ) {s = 19;}
						else if ( (LA8_0=='f') ) {s = 20;}
						else if ( (LA8_0=='i') ) {s = 21;}
						else if ( (LA8_0=='e') ) {s = 22;}
						else if ( (LA8_0=='p') ) {s = 23;}
						else if ( (LA8_0=='l') ) {s = 24;}
						else if ( (LA8_0=='v') ) {s = 25;}
						else if ( (LA8_0=='c') ) {s = 26;}
						else if ( (LA8_0=='b') ) {s = 27;}
						else if ( ((LA8_0 >= '1' && LA8_0 <= '9')) ) {s = 28;}
						else if ( (LA8_0=='0') ) {s = 29;}
						else if ( ((LA8_0 >= 'A' && LA8_0 <= 'Z')||LA8_0=='a'||LA8_0=='d'||(LA8_0 >= 'g' && LA8_0 <= 'h')||(LA8_0 >= 'j' && LA8_0 <= 'k')||LA8_0=='m'||LA8_0=='o'||(LA8_0 >= 'q' && LA8_0 <= 's')||LA8_0=='u'||(LA8_0 >= 'w' && LA8_0 <= 'z')) ) {s = 30;}
						else if ( ((LA8_0 >= '\t' && LA8_0 <= '\n')||LA8_0=='\r'||LA8_0==' ') ) {s = 31;}
						else if ( ((LA8_0 >= '\u0000' && LA8_0 <= '\b')||(LA8_0 >= '\u000B' && LA8_0 <= '\f')||(LA8_0 >= '\u000E' && LA8_0 <= '\u001F')||(LA8_0 >= '!' && LA8_0 <= '%')||LA8_0=='\''||(LA8_0 >= '?' && LA8_0 <= '@')||(LA8_0 >= '[' && LA8_0 <= '`')||(LA8_0 >= '~' && LA8_0 <= '\uFFFF')) ) {s = 32;}
						if ( s>=0 ) return s;
						break;
			}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 8, _s, input);
			error(nvae);
			throw nvae;
		}
	}

}
