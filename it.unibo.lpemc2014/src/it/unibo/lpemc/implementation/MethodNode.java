package it.unibo.lpemc.implementation;

import it.unibo.lpemc.implementation.oop.FieldNode;
import it.unibo.lpemc.implementation.oop.Reference;
import it.unibo.lpemc.implementation.types.ArrowTypeNode;
import it.unibo.lpemc.interfaces.INode;
import it.unibo.lpemc.interfaces.Node;

import java.util.ArrayList;
import java.util.HashMap;

public class MethodNode extends Node {
	
	private HashMap<String, STentry>	activationRecordEntries;
	protected Node						body;
	protected ArrayList<Node>			localDeclarations;
	protected final ArrayList<Node>		parameters;
	protected final Node					type;
	
	public MethodNode(final String i, final Node t) {
		super(i);
		this.type = t;
		this.parameters = new ArrayList<Node>();
		FOOLlib.setFather(parameters, this);
		activationRecordEntries = new HashMap<String, STentry>();
	}
	
	/**
	 * Aggiunge il corpo al metodo
	 *
	 * @param declarations
	 *            Dichiarazioni interne al meotodo
	 * @param body
	 *            Corpo del metodo
	 */
	public void addDecBody(final ArrayList<Node> declarations, final Node body) {
		this.localDeclarations = declarations;
		FOOLlib.setFather(this.localDeclarations, this);
		this.body = body;
		FOOLlib.setFather(this.body, this);
	}
	
	public void addPar(final Node p) {
		FOOLlib.setFather(p, this);
		this.parameters.add(p);
	}
	
	@Override
	protected String codeGen() {
		/*
		 * aggiungo in FOOLlib il codice associato alla funzione, lascio nella cella di memoria relativa alla dichiarazione la push della label. Il valore della funzione è l'indirizzo a cui fare la jump
		 */
		String localDeclarations = "";
		String popSledgeDef = "";
		for (final INode n : this.localDeclarations) {
			localDeclarations += n.codeGeneration();
			for (int j = 0; j < n.getSize(); j++)
				popSledgeDef += Syskb.POP;
		}
		
		String popSledgeParams = "";
		for (int i = this.parameters.size() - 1; i >= 0; i--) {
			for (int j = 0; j < this.parameters.get(i).getSize(); j++)
				popSledgeParams += Syskb.POP;
			
		}
		
		if (Syskb.isClass(this.parameters.get(0).typeCheck().typeCheck().getId())) {
			final ArrayList<Node> struct = Syskb.getClass(this.parameters.get(0).typeCheck().typeCheck().getId()).getStruct();
			for (final Object n : struct) {
				if (n instanceof FieldNode) {
					((FieldNode) n).setObject(new Reference());
				}
			}
		}
		
		String label = FOOLlib.getLabelId(getId());
		FOOLlib.putCode(label + ":" + Syskb.CR + // inizia con label
				Syskb.COPYFP + // copy in the FP register the currest stack pointer
				Syskb.LOADRA + // push in the stack the content of the RA register
				localDeclarations + // inserimento in memoria delle dichiarazioni locali
				this.body.codeGeneration() + // genero il codice del body della funzione
				Syskb.STORERV + // pop the top of the stack and copy it in the RV register
				popSledgeDef + // pop per dichiarazioni locali, le tolgo dallo stack
				Syskb.STORERA + // pop the top of the stack and copy it in the RA register
				Syskb.POP + // pop
				popSledgeParams + // pop per i parametri
				Syskb.STOREFP + // pop the top of the stack and copy it in the FP register
				Syskb.LOADRV + // push in the stack the content of the RV register
				Syskb.LOADRA + // push in the stack the content of the RA register
				Syskb.JS // jump
		);
		return Syskb.PUSH + label + Syskb.CR;
	}
	
	public HashMap<String, STentry> getActivationRecordEntries() {
		return activationRecordEntries;
	}
	
	public int getParameterOffset(String paramId) {
		for (int i = 0; i < parameters.size(); i++)
			if (parameters.get(i).getId().equals(paramId))
				return i;
		return -1;
	}
	
	@Override
	public int getSize() {
		return 1; // etichetta + possibile al
	}
	
	@Override
	public String toPrint(final String s) {
		return s + Syskb.FUN + Syskb.BLANK + this.id;
	}
	
	@Override
	public Node typeCheck() {
		final ArrayList<Node> pt = new ArrayList<Node>();
		for (int i = 1; i < this.parameters.size(); i++) {
			pt.add((this.parameters.get(i)).typeCheck());
		}
		for (int j = 0; j < this.localDeclarations.size(); j++) {
			(this.localDeclarations.get(j)).typeCheck();
		}
		if (!(FOOLlib.isSubtype(this.body.typeCheck(), this.type))) {
			FOOLlib.exit("Wrong return type for function " + this.id);
		}
		return new ArrowTypeNode(pt, this.type);
	}
}
