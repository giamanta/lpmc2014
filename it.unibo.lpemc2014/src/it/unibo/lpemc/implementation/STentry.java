package it.unibo.lpemc.implementation;

import it.unibo.lpemc.interfaces.INode;
import it.unibo.lpemc.interfaces.Node;

/**
 * Entry della symbol table, contiene informazioni sul nodo della variabile quali tipo e offset
 */
public class STentry {
	
	private String		className;
	private final int	nestingLevel;
	private int			offset;
	private Node		type;
	private final Node	variableNode;
	
	public STentry(final Node variableNode, final int nestingLevel, final int varOffset) {
		this(variableNode, nestingLevel, null, varOffset, null);
	}
	
	public STentry(final Node variableNode, final int nestingLevel, final int varOffset, String className) {
		this(variableNode, nestingLevel, null, varOffset, className);
	}
	
	public STentry(final Node variableNode, final int nestingLevel, final Node type) {
		this(variableNode, nestingLevel, type, Integer.MAX_VALUE, null);
	}
	
	public STentry(Node field, int nestingLevel, Node fieldType, int fieldOffset) {
		this(field, nestingLevel, fieldType, fieldOffset, null);
	}
	
	/**
	 * Esempio di utilizzo del costruttore: new STentry(v,nestingLevel,$t.ast,varOffset--)
	 * 
	 * @param variableNode
	 *            nodo della variabile
	 * @param nestingLevel
	 *            nesting level
	 * @param type
	 *            tipo della variabile
	 * @param offset
	 *            offset della variabile rispetto a fp, parte da -2
	 */
	public STentry(final Node variableNode, final int nestingLevel, final Node type, final int offset, String className) {
		this.type = type;
		this.offset = offset;
		this.variableNode = variableNode;
		this.nestingLevel = nestingLevel;
		variableNode.setSTEntry(this);
		this.className = className;
	}
	
	public String getClassName() {
		return className;
	}
	
	public INode getContest() {
		return variableNode.getFather();
	}
	
	public int getNestingLevel() {
		return this.nestingLevel;
	}
	
	public Node getNode() {
		return this.variableNode;
	}
	
	public int getOffset() {
		return this.offset;
	}
	
	public Node getType() {
		return this.type;
	}
	
	public boolean isInClass() {
		return className != null;
	}
	
	public void setClassFlag(String className) {
		this.className = className;
	}
	
	public void setContest(final INode father) {
		variableNode.setFather(father);
	}
	
	public void setOffset(final int offset) {
		this.offset = offset;
	}
	
	public void setType(final Node type) {
		this.type = type;
	}
}
