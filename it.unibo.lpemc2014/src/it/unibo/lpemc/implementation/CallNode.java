package it.unibo.lpemc.implementation;

import it.unibo.lpemc.implementation.types.ArrowTypeNode;
import it.unibo.lpemc.interfaces.INode;
import it.unibo.lpemc.interfaces.MergeNode;
import it.unibo.lpemc.interfaces.Node;

import java.util.ArrayList;

/**
 * Nodo che gestisce la chiamata della funzione nel codice assembly, non corrisponde al codice della funzione bens� indica il modo in cui la stessa � invocata dal programma. Mentre il codice della funzione si trova dopo halt, l'invocazione � precedente alla terminazione del programma.
 */
public class CallNode extends Node {
	
	private boolean					fpflag;
	protected final int				nestingLevelDifference;
	protected final ArrayList<Node>	par;
	private final MergeNode			parameters;
	protected final STentry			st;
	
	/**
	 * @param id
	 *            function id
	 * @param stEntry
	 *            symbol table entry
	 * @param parameters
	 *            parameters of the function
	 * @param nestingLevelDifference
	 */
	public CallNode(final String id, final STentry stEntry, final ArrayList<Node> parameters, final int nestingLevelDifference) {
		super(id);
		this.st = stEntry;
		this.par = parameters;
		for (INode n : parameters) {
			if (n != null)
				n.setFather(this);
		}
		this.parameters = new MergeNode("invoking " + id + " with params", parameters);
		this.nestingLevelDifference = nestingLevelDifference;
	}
	
	@Override
	protected String codeGen() {
		String parCode = "", ret = "", getAR = Syskb.LOADFP;

		for (int i = this.par.size() - 1; i >= 0; i--) {
			parCode += this.par.get(i).codeGeneration();
		}

		for (int i = 0; i < this.nestingLevelDifference; i++) {
			getAR += Syskb.LOADW;
		}
		
		if (getFunctionalProgrammingFlag()) {
			ret = Syskb.LOADFP + // inserisco nello stack il codice dei parametri
					parCode + // params
					// devo ottenere riferimento a AR record
					getAR + // accedo a access link del chiamante
					Syskb.PUSH + (this.st.getOffset() + 1) + Syskb.CR + // considero l'offset rispetto a questo chiamante
					Syskb.ADD + // sommo i valori
					Syskb.LOADW + // ottengo l'access link della funzione chiamante
					// Syskb.PRINT +
					Syskb.PUSH + (this.st.getOffset()) + Syskb.CR + // offset della funzione da chiamare
					getAR + // considero attivation record del chiamante
					Syskb.ADD + // sommo i valori
					Syskb.LOADW + // leggo l'indirizzo
					// Syskb.PRINT +
					Syskb.JS; // eseguo la jump
			
		} else {
			ret = // inserisco nello stack il frame pointer
			Syskb.LOADFP + // inserisco nello stack il codice dei parametri
					parCode + // params
					// inserisco nello stack il frame pointer
					
					// navigo in modo ricorsivo memoria[fp] per risalire
					// all'activation record padre, ogni volta che eseguo una loadw
					// memorizzo nello stack l'indirizzo letto
					getAR +
					
					Syskb.PUSH + this.st.getOffset() + Syskb.CR +
					// inserisco nello stack il frame pointer, lo faccio due volte, perch�?
					
					getAR +
					// cerco indirizzo per il jump, ora come primo valore nello stack
					// mi trovo l'indirizzo dell'AR, a questo aggiungo l'offset dell'StEntry
					Syskb.ADD +
					
					// leggo l'indirizzo
					Syskb.LOADW +
					// eseguo la jump
					Syskb.JS;
		}
		return ret;
	}
	
	public boolean getFunctionalProgrammingFlag() {
		return fpflag;
	}
	
	public ArrayList<Node> getParameters() {
		return par;
	}
	
	public void setFunctionalProgrammingFlag(boolean flag) {
		fpflag = flag;
	}
	
	@Override
	public String toPrint(final String s) {
		return this.parameters.toPrint(s);
	}
	
	@Override
	public Node typeCheck() {
		final Node t = this.st.getType();
		if (!(t instanceof ArrowTypeNode)) {
			FOOLlib.exit("Invocation of a non-function " + this.id);
		}
		final ArrayList<Node> p = ((ArrowTypeNode) t).getPar();
		if (!(p.size() == this.par.size())) {
			FOOLlib.exit("Wrong number of parameters in the invocation of " + this.id);
		}
		for (int i = 0; i < this.par.size(); i++) {
			if (!(FOOLlib.isSubtype(this.par.get(i).typeCheck(), p.get(i)))) {
				FOOLlib.exit("Wrong type " + p.get(i).typeCheck().toPrint("") + " for " + (i + 1) + "-th parameter in the invocation of " + this.id + ", expected a subtype of " + this.par.get(i).typeCheck().toPrint(""));
			}
		}
		return ((ArrowTypeNode) t).getRet();
	}
	
}