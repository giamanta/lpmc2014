package it.unibo.lpemc.implementation;

import it.unibo.lpemc.interfaces.Node;

public class ProgNode extends Node {
	
	private final Node	exp;
	
	public ProgNode(final Node e) {
		super("prog");
		this.exp = e;
		FOOLlib.setFather(this.exp, this);
	}
	
	@Override
	protected String codeGen() {
		return this.exp.codeGeneration() + Syskb.HALT;
	}
	
	@Override
	public String toPrint(final String s) {
		
		return "Prog\n" + this.exp.toPrint(Syskb.INDENT);
	}
	
	@Override
	public Node typeCheck() {
		return this.exp.typeCheck();
	}
}