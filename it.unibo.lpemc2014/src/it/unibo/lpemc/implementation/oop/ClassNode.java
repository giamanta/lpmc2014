package it.unibo.lpemc.implementation.oop;

import it.unibo.lpemc.implementation.FOOLlib;
import it.unibo.lpemc.implementation.IdNode;
import it.unibo.lpemc.implementation.MethodNode;
import it.unibo.lpemc.implementation.STentry;
import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.interfaces.IClass;
import it.unibo.lpemc.interfaces.Node;

import java.util.ArrayList;
import java.util.HashMap;

public class ClassNode extends Node implements IClass {

	public static final int indexStart = 1;
	private ArrayList<Node> struct;
	private ClassNode superClaz;
	private int nfields = 0;
	public HashMap<String, String> labels;

	/**
	 * Tabella che mantiene la relazione tra un metodo/campo e la classe di
	 * appartenenza. ... Requirements ... Gli oggetti, che nascono come
	 * instanziazione di classi , contengono dei campi (dichiarati all'inizio
	 * della dichiarazione della classe o ereditati dalla superclasse) e dei
	 * metodi (esplicitamente dichiarati o ereditati dalla superclasse). Se in
	 * una sottoclasse viene dichiarato un campo o un metodo con il medesimo
	 * nome di un campo della superclasse, tale campo o metodo sovrascrive
	 * quello della superclasse. I campi non sono modificabili ed il loro valore
	 * viene definito in fase di instanziazione. E` inoltre possibile dichiarare
	 * funzioni annidate. Le funzioni possono essere passate come parametri. ...
	 */

	public ClassNode(final String className) {
		super(className);
		this.struct = new ArrayList<>();
		// rifaso la struct
		for (int i = 0; i < ClassNode.indexStart; i++) {
			this.struct.add(null);
		}
		Syskb.addClass(className, this);
		labels = new HashMap<String, String>();
	}

	/**
	 * Ogni classe ha dei campi, questo metodo richiamato quando alla classe �
	 * aggiunto un nuovo field. Cosa succede in caso di ereditarieta' Come
	 * descritto nelle specifiche del progetto, se due campi hanno lo stesso
	 * nome, allora il campo della super classe sovrascritto da quello della
	 * sotto classe
	 **/
	@Override
	public void addField(final Node field) {
		final int s = this.struct.size();
		int index = ClassNode.indexStart;
		for (; index < s; index++) {
			if (this.struct.get(index).getId().equals(field.getId())) {
				if (!FOOLlib.isSubtype(field.typeCheck(), this.struct
						.get(index).typeCheck()))
					FOOLlib.exit("Error in class " + getId() + ", field "
							+ field.getId() + ": type "
							+ field.typeCheck().toPrint("")
							+ " expected a subtype of "
							+ this.struct.get(index).typeCheck().toPrint(""));
				final FieldNode f = (FieldNode) this.struct.get(index);
				f.setType(field.typeCheck());
				return;
			}
		}
		final FieldNode tmp = new FieldNode(field.getId(), field.typeCheck(),
				index);
		tmp.setFather(this);
		this.struct.add(tmp);
		this.nfields++;
	}

	@Override
	public void addMethod(final MethodNode method) {
		labels.put(method.getId(), method.codeGeneration());
		final int s = this.struct.size();
		int index = ClassNode.indexStart;
		for (; index < s; index++) {
			if (this.struct.get(index).getId().equals(method.getId())) {
				if (!FOOLlib.isSubtype(method.typeCheck(),
						this.struct.get(index).typeCheck()))
					FOOLlib.exit("Error in class " + getId() + ", method "
							+ method.getId() + ": type \n"
							+ method.typeCheck().toPrint("")
							+ " expected a subtype of \n"
							+ this.struct.get(index).typeCheck().toPrint(""));
				this.struct.add(index, method);
				this.struct.remove(index + 1);

				return;
			}
		}
		method.setFather(this);
		this.struct.add(method);
	}

	public CallMethodNode callMethod(final String methodId,
			final STentry entry, final ArrayList<Node> argList,
			final int nestingLevelDifference) {
		for (int index = ClassNode.indexStart; index < this.struct.size(); index++) {
			final Node n = this.struct.get(index);
			if (n.getId().equals(methodId)) {
				return new CallMethodNode(this.getId() + "." + n.toString(),
						n.getSTEntry(), argList, nestingLevelDifference, index);
			}
		}
		return null;
	}

	public FieldNode getFieldNode(final IdNode obj, final String fieldId) {
		/*
		 * Perch� utilizzo i campi della classe e non quelli dell'oggetto?
		 * Perch� i campi della classe rappresentano i parametri formali e
		 * contengono anche l'id del parametro. I campi dell'oggetto contengono
		 * invece solo il valore e se ne fregano dell'id
		 */
		for (int index = ClassNode.indexStart; index < this.struct.size(); index++) {
			if (this.struct.get(index) instanceof Node
					&& ((Node) this.struct.get(index)).getId().equals(fieldId)) {
				return new FieldNode(obj,
						((Node) this.struct.get(index)).typeCheck(), index);
			}
		}
		return null;
	}

	@Override
	public FieldNode getFieldNode(final String fieldId) {
		for (int index = ClassNode.indexStart; index < this.struct.size(); index++) {
			if (this.struct.get(index) instanceof Node
					&& (this.struct.get(index) instanceof FieldNode)
					&& ((Node) this.struct.get(index)).getId().equals(fieldId)) {
				return (FieldNode) this.struct.get(index);
			}
		}
		return null;
	}

	@Override
	public int getFieldOffset(final String fieldId) {
		for (int index = ClassNode.indexStart; index < this.struct.size(); index++) {
			if (this.struct.get(index) instanceof Node
					&& ((Node) this.struct.get(index)).getId().equals(fieldId)) {
				return index;
			}
		}
		return -1;
	}

	@Override
	public int getSize() {
		return 1;
	}

	@Override
	public ArrayList<Node> getStruct() {
		return this.struct;
	}

	@Override
	public int getStuctSize() {
		return struct.size();
	}

	@Override
	public boolean hasField(final String fieldId) {
		return this.getFieldOffset(fieldId) == -1 ? false : true;
	}

	/**
	 * Ogni classe ha dei metodi, questo metodo richiamato quando alla classe �
	 * aggiunto un nuovo metodo. Cosa succede in caso di ereditariet� Come
	 * descritto nelle specifiche del progetto, se due metodi hanno lo stesso
	 * nome, allora il metodo della super classe � sovrascritto da quello della
	 * sotto classe
	 **/
	@SuppressWarnings("unchecked")
	@Override
	public void setSuperClass(final String superClass) {
		this.superClaz = Syskb.getClass(superClass);
		// eredito i campi e metodi della super classe (se esiste)
		this.struct = (superClass == null) ? new ArrayList<Node>()
				: (ArrayList<Node>) this.superClaz.getStruct().clone();
		this.labels = (HashMap<String, String>) superClaz.labels.clone();
	}

	@Override
	public String toPrint(final String indent) {
		final String ret = (this.superClaz == null) ? Syskb.OBJECT + "->"
				: this.superClaz.typeCheck().toPrint("") + "->";
		return ret + this.getId();
	}

	@Override
	public Node typeCheck() {
		return this;
	}

	public int getFieldSize() {
		return this.nfields;
	}
}
