package it.unibo.lpemc.implementation.oop;

import it.unibo.lpemc.implementation.IdNode;
import it.unibo.lpemc.implementation.STentry;
import it.unibo.lpemc.implementation.Syskb;

public class Reference extends IdNode {
	
	public Reference() {
		super(null, null, 0);
	}
	
	public Reference(final String i, final STentry st, final int nestingLevelDifference) {
		super(null, null, 0);
	}
	
	@Override
	protected String codeGen() {
		return Syskb.LOADFP + 
				Syskb.PUSH + 1 + Syskb.CR + 
				Syskb.ADD + 
				Syskb.LOADW;
	}
	
}
