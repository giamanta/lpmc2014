package it.unibo.lpemc.implementation.oop;

import it.unibo.lpemc.implementation.CallNode;
import it.unibo.lpemc.implementation.FOOLlib;
import it.unibo.lpemc.implementation.MethodNode;
import it.unibo.lpemc.implementation.STentry;
import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.implementation.types.ArrowTypeNode;
import it.unibo.lpemc.interfaces.Node;

import java.util.ArrayList;

public class CallMethodNode extends CallNode {
	private final int methodOffset;
	private final ArrayList<Node> parameters;
	private final Node thiz;

	public CallMethodNode(final String id, final STentry stEntry,
			final ArrayList<Node> parameters, final int nestingLevelDifference,
			final Integer methodOffset) {
		super(id, stEntry, parameters, nestingLevelDifference);
		this.parameters = parameters;
		if (parameters.size() == 0) 
			this.thiz = null; 
		else 
			this.thiz = parameters.get(0);
		this.methodOffset = methodOffset;
	}

	@Override
	protected String codeGen() {
		/*
		 * Come funziona questa code generation? Ho due casi
		 * 1) Il padre del metodo � la classe, non devo fare nulla di particolare.
		 * Come primo parametro assumo di avere l'idNode dell'oggetto chiamante,
		 * al suo offset (ottenuto tramite la code generation) sommo l'offset del metodo 
		 * e lo chiamo.
		 * 2) Il padre del metodo � un altro metodo della stessa classe. Quindi? Devo ereditare il parametro
		 * idNode del padre. Come faccio? Lo prendo dalla sequenze lfp push 1 add lw. 
		 * Ovviamente devo generare i parametri che vanno da n a 1, in quanto appunto lo zero
		 * lo eredito. Perch� controllo this.parameters.get(0) instanceof NullNode? perch�
		 * ci sono casi speciali: fun get:int () n.getInt();
		 * get � il padre di getInt, quindi per evitare di copiare il padre sbagliato 
		 * copio il padre solo nel caso in cui il mio stesso padre non sia settato
		 */
		int toIndex = 0; boolean flag = false;
		if (getFather() instanceof MethodNode && 
				// il padre non � stato settato
				this.thiz == null) {
			toIndex = ClassNode.indexStart;
			flag = true;
		}
		
		String parCode = "";
		for (int i = this.parameters.size() - 1; i >= toIndex; i--)
			parCode += this.parameters.get(i).codeGeneration();

		if (!flag) {
			return // inserisco nello stack il frame pointer
					Syskb.LOADFP + // inserisco nello stack il codice dei parametri
					parCode + // params
					Syskb.LOADFP + // inserisco il frame pointer nello stack
					thiz.codeGeneration() + // offset id node
					Syskb.PUSH + this.methodOffset + Syskb.CR + // ottengo
																// offset del
																// metodo
					Syskb.ADD + // li sommo...
					Syskb.LOADW + // carico l'etichetta
					Syskb.JS // eseguo la jump
			;
		} else {
			return Syskb.LOADFP + // inserisco nello stack il frame pointer
					parCode + // inserisco nello stack il codice dei parametri
					Syskb.LOADFP + 
					Syskb.PUSH + 1 + Syskb.CR + 
					Syskb.ADD + 
					Syskb.LOADW + // riferimento all'idNode del padre
					Syskb.LOADFP + // inserisco il frame pointer nello stack
					Syskb.LOADFP + 
					Syskb.PUSH + 1 + Syskb.CR + 
					Syskb.ADD + Syskb.LOADW + // riferimento all'idNode del padre a cui sommo l'offset
					Syskb.PUSH + this.methodOffset + Syskb.CR + // ottengo
																// offset del metodo
					Syskb.ADD + // li sommo...
					Syskb.LOADW + // carico l'etichetta
					Syskb.JS // eseguo la jump
			;
		}
	}
	
	@Override
	public Node typeCheck() {
		final Node t = this.st.getType();
		if (!(t instanceof ArrowTypeNode)) {
			System.out.println("Invocation of a non-function " + this.id);
			System.exit(0);
		}
		final ArrayList<Node> p = ((ArrowTypeNode) t).getPar();
		if (!(p.size() == this.par.size())) {
			System.out.println("Wrong number of parameters in the invocation of " + this.id);
			System.exit(0);
		}
		for (int i = ClassNode.indexStart; i < this.par.size(); i++) {
			if (!(FOOLlib.isSubtype(this.par.get(i).typeCheck(), p.get(i)))) {
				System.out.println("Wrong type " + p.get(i).typeCheck() + " for " + (i + 1) + "-th parameter in the invocation of " + this.id + ", expected " + this.par.get(i).typeCheck());
				System.exit(0);
			}
		}
		return ((ArrowTypeNode) t).getRet();
	}
}
