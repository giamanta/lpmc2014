package it.unibo.lpemc.implementation.oop;

import it.unibo.lpemc.implementation.FOOLlib;
import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.interfaces.Node;

import java.util.ArrayList;

/**
 * var obj:ClassName = new ClassName(params); | NEW ID LPAR (exp (COMMA exp)* )?
 * RPAR obj.a(params); I parametri sono a loro volta valori derivanti da
 * espressioni | DOT ID LPAR (exp (COMMA exp)* )? RPAR )? Se ho solo DOT ID sto
 * accedendo a un parametro, se ho anche la parentesi invece sto invocando un
 * metodo, proprio come succede nel caso di IdNode e CallNode (problema simile),
 * ora per� Id e Metodo devo cercarli all'interno dell'object record
 * dell'oggetto
 */
public class ObjectNode extends Node {

	private static int objCount = 0;
	private static final String storeValue = Syskb.LOADHP + // carico hp e lo
															// inserisco nello
															// stack
			Syskb.STOREW + // salvo il valore in memoria, e rimuovo i valori
							// precedentemente inseriti nello STACK
			Syskb.LOADHP + // devo incrementare HP, come? Lo carico in memoria
			Syskb.PUSH + "1" + Syskb.CR + // faccio la push di 1
			Syskb.ADD + // sommo i valori (ottenendo hp++)
			Syskb.STOREHP; // salvo il valore incrementato
	private final ClassNode claz;
	private final ArrayList<Node> fieldValues;

	public ObjectNode(final String string, final ArrayList<Node> fieldValues) {
		super(string + ObjectNode.objCount++);
		this.claz = Syskb.getClass(string);

		int nfields = 0;
		for (int i = 0; i < this.claz.getStuctSize(); i++)
			if (this.claz.getStruct().get(i) instanceof FieldNode)
				nfields++;

		if (nfields != fieldValues.size())
			FOOLlib.exit("Fields number mismatch");

		for (int index = ClassNode.indexStart; index <= nfields; index++)
			if (this.claz.getStruct().get(index) instanceof FieldNode) {
				if (!FOOLlib.isSubtype(fieldValues.get(index - 1).typeCheck(),
						this.claz.getStruct().get(index).typeCheck()))
					FOOLlib.exit("Fields types mismatch");
			} else
				continue;

		this.fieldValues = fieldValues;
	}

	@Override
	protected String codeGen() {
		String values = "", object = "", popSledgeParams = "";
		for (int i = this.fieldValues.size() - 1; i >= 0; i--) {
			values += this.fieldValues.get(i).codeGeneration();
			for (int j = 0; j < fieldValues.get(i).getSize(); j++)
				popSledgeParams += Syskb.POP;
		}

		int fieldIndex = 0;
		for (int index = ClassNode.indexStart; index < this.claz.getStruct()
				.size(); index++) {
			object += // push value
			(this.claz.getStruct().get(index) instanceof FieldNode ? // field
					Syskb.LOADFP + // carico fp
					Syskb.PUSH + (fieldIndex++ + 1) + Syskb.CR + // indice del
																	// parametro
					Syskb.ADD + // sommo i valori
					Syskb.LOADW // carico la word
					: claz.labels.get(this.claz.getStruct().get(index).getId()))
					+ // metodo
					storeValue; // il valore � un metodo
		}

		FOOLlib.putCode(getId() + ":" + Syskb.CR + // inizia con label
				Syskb.COPYFP + // copy in the FP register the currest stack
								// pointer
				Syskb.LOADRA + // push in the stack the content of the RA
								// register
				// genero il codice del body della funzione
				Syskb.LOADHP + // puntatore all'oggetto
				Syskb.LOADHP + // carico hp sullo stack e poi lo memorizzo in
								// heap, questo � il puntatore dell'oggetto a se
								// stesso nella memoria heap
				storeValue + 
				object + 
				Syskb.STORERV + // pop the top of the
														// stack and copy it in
														// the RV register
				Syskb.STORERA + // pop the top of the stack and copy it in the
								// RA register
				Syskb.POP + // pop
				popSledgeParams + // pop per i parametri
				Syskb.STOREFP + // pop the top of the stack and copy it in the
								// FP register
				Syskb.LOADRV + // push in the stack the content of the RV
								// register
				Syskb.LOADRA + // push in the stack the content of the RA
								// register
				Syskb.JS // jump
		);
		return Syskb.LOADFP + // preparo AR
				values + // inserisco la code generation dei parametri
				Syskb.LOADFP + Syskb.PUSH + getId() + Syskb.CR + // push della
																	// label
				Syskb.JS; // jump
	}

	@Override
	public Node typeCheck() {
		return this.claz.typeCheck();
	}
}
