package it.unibo.lpemc.implementation.oop;

import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.implementation.types.NullTypeNode;
import it.unibo.lpemc.interfaces.Node;

/**
 * Questa classe realizza il nodo nullo
 */
public class NullNode extends Node {
	
	public NullNode() {
		super(Syskb.NULL);
	}
	@Override
	protected String codeGen() {
		return Syskb.PUSH + Integer.MIN_VALUE + Syskb.CR // questa cella rimane sullo stack, e costituisce il riferimento della variabile alla memoria in hp
		;
	}
	
	@Override
	public Node typeCheck() {
		return new NullTypeNode();
	}
	
}
