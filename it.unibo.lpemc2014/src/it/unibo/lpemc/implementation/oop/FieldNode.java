package it.unibo.lpemc.implementation.oop;

import it.unibo.lpemc.implementation.IdNode;
import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.interfaces.Node;

public class FieldNode extends IdNode {
	private Node	object;
	private int		offset;
	private Node	type;
	
	public FieldNode(final IdNode object, final Node type, final int offset) {
		this("undefinedId", object, type, offset);
	}
	
	public FieldNode(final Node type, final int offset) {
		this("undefinedId", null, type, offset);
	}
	
	public FieldNode(final String id, final IdNode object, final Node typeCheck, final int idOffset) {
		super(id, null, Integer.MAX_VALUE);
		this.object = object;
		this.offset = idOffset;
		this.type = typeCheck;
	}
	
	public FieldNode(final String id, final Node type) {
		this(id, null, type, Integer.MAX_VALUE);
	}
	
	public FieldNode(final String id, final Node type, final int offset) {
		this(id, null, type, offset);
	}
	
	@Override
	protected String codeGen() {
		return this.object.codeGeneration() + // riferimento all'oggetto
				Syskb.PUSH + this.offset + Syskb.CR + // offset del campo
				Syskb.ADD + // sommo i due valori
				Syskb.LOADW;
	}
	
	public void setObject(final IdNode objectReference) {
		this.object = objectReference;
	}
	
	public void setOffset(final int offset) {
		this.offset = offset;
	}
	
	public void setType(final Node type) {
		this.type = type;
	}
	
	@Override
	public String toPrint(final String s) {
		return this.toString();
	}
	
	@Override
	public Node typeCheck() {
		return this.type;
	}
	
}