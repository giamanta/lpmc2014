package it.unibo.lpemc.implementation.types;

import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.interfaces.IClass;
import it.unibo.lpemc.interfaces.Node;

public class IdType extends Node {
	private final IClass	claz;
	private final String	id;
	
	public IdType(final String id) {
		this.id = id;
		this.claz = Syskb.getClass(this.id);
	}
	
	@Override
	public int getSize() {
		return 1;
	}
	
	@Override
	public String toPrint(final String indent) {
		return this.claz.toPrint(indent);
	}
	
	@Override
	public Node typeCheck() {
		// this.claz = Syskb.getClass(this.id);
		return this.claz.typeCheck();
	}
}
