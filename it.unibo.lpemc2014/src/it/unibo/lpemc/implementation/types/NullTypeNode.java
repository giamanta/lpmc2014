package it.unibo.lpemc.implementation.types;

import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.interfaces.Node;

public class NullTypeNode extends Node {
	public NullTypeNode() {
		super(Syskb.NULLTYPE);
	}
	
	@Override
	public int getSize() {
		return 1;
	}
	
	@Override
	public String toPrint(final String indent) {
		return indent + Syskb.NULLTYPE;
	}
	
	@Override
	public Node typeCheck() {
		return NullTypeNode.this;
	}
}
