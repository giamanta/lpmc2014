package it.unibo.lpemc.implementation.types;

import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.interfaces.Node;
import it.unibo.lpemc.interfaces.NodePrintable;

public class BoolTypeNode extends NodePrintable {
	
	public BoolTypeNode() {
		super(Syskb.BOOLTYPE);
	}
	
	@Override
	public int getSize() {
		return 1;
	}
	@Override
	public Node typeCheck() {
		return BoolTypeNode.this;
	}
}