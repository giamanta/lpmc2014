package it.unibo.lpemc.implementation.types;

import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.interfaces.Node;
import it.unibo.lpemc.interfaces.NodePrintable;

public class IntTypeNode extends NodePrintable {
	
	public IntTypeNode() {
		super(Syskb.INTTYPE);
	}
	
	@Override
	public int getSize() {
		return 1;
	}
	
	@Override
	public Node typeCheck() {
		return IntTypeNode.this;
	}
	
}