package it.unibo.lpemc.implementation.types;

import it.unibo.lpemc.interfaces.Node;

import java.util.ArrayList;

/**
 * Classe che contiene i tipi dei parametri e il tipo di ritorno di una funzione
 */
public class ArrowTypeNode extends Node {
	
	private final ArrayList<Node>	par;
	private final Node				ret;
	
	/**
	 * Costruttore di arrow type node
	 *
	 * @param parameterTypes
	 * @param returnType
	 *            il tipo di ritorno della funzione corrisponde al tipo della funzione e al tipo dell'arrow type
	 */
	public ArrowTypeNode(final ArrayList<Node> parameterTypes, final Node returnType) {
		super("arrow");
		this.par = parameterTypes;
		this.ret = returnType;
	}
	
	public ArrayList<Node> getPar() {
		return this.par;
	}
	
	public Node getRet() {
		return this.ret;
	}
	
	@Override
	public int getSize() {
		return 2;
	}
	
	@Override
	public String toPrint(final String s) {
		String r = s;
		for (final Node type : this.par) {
			r += "[" + type.typeCheck().toPrint("") + "]" + "->";
		}
		r += "[" + this.ret.typeCheck().toPrint("") + "]";
		return r;
	}
	
	@Override
	public Node typeCheck() {
		return ArrowTypeNode.this;
	}
}
