package it.unibo.lpemc.implementation.values;

import it.unibo.lpemc.implementation.types.IntTypeNode;
import it.unibo.lpemc.interfaces.ValueNode;

public class NatNode extends ValueNode {
	public NatNode(final Integer n) {
		super(new IntTypeNode(), n);
	}
}