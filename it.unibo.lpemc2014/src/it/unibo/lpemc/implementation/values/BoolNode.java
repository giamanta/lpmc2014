package it.unibo.lpemc.implementation.values;

import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.implementation.types.BoolTypeNode;
import it.unibo.lpemc.interfaces.ValueNode;

public class BoolNode extends ValueNode {
	
	public BoolNode(final Boolean n) {
		super(new BoolTypeNode(), n);
	}
	
	@Override
	protected String codeGen() {
		return Syskb.PUSH + (((Boolean) this.getValue()).booleanValue() == true ? 1 : 0) + Syskb.CR;
	}
}