package it.unibo.lpemc.implementation;

import it.unibo.lpemc.implementation.oop.ClassNode;
import it.unibo.lpemc.interfaces.IOutput;

import java.util.HashMap;

public class Syskb {
	public static final String					ADD				= "add" + Syskb.CR;			// add two values from the stack
																								
	public static final String					AND				= "&&";
	
	public static final String					BLANK			= " ";
	
	public static final String					BOOL			= "bool";
	
	public static final String					BOOLTYPE		= "boolType";
	
	public static final String					BRANCH			= "b" + Syskb.CR ;			// jump to label
																								
	public static final String					BRANCHEQ		= "beq" + Syskb.CR;			// jump to label if top == next
	public static final String					BRANCHLESS		= "bless" + Syskb.CR;		// jump to label if top < next
	private static HashMap<String, ClassNode>	classMap		= new HashMap<>();
	
	static {
		reset();
	}
	public static final String					COPYFP			= "cfp" + Syskb.CR;			// copy stack pointer into frame pointer
	public static final String					CR				= "\r";
	public static final String					DECLARATION		= "declaration";
	public static final String					DIV				= "div" + Syskb.CR;
	public static final String					ELSE			= "else";
	public static final String					EQ				= " == ";
	public static final String					EXP				= "exp";
	public static final String					FUN				= "fun";
	public static final String					GADD			= "add";
	public static final String					GDIV			= "div";
	public static final String					GMULT			= "mult";
	public static final String					GR				= " >= ";
	public static final String					GSUB			= "sub";
	public static final String					HALT			= "halt" + Syskb.CR;			// stop executions
	public static final String					IDTYPE			= "idType";
	public static final String					IF				= "if ... then ... else ...";
	public static final String					IN				= "in";
	public static final String					INDENT			= "\t";
	public static final String					INTTYPE			= "intType";
	public static final String					JS				= "js" + Syskb.CR;				// jump to instruction pointed by top of stack and store next instruction in ra
	public static final String					LE				= " <= ";
	public static final String					LET				= "let";
	public static final String					LOADFP			= "lfp" + Syskb.CR;			// load frame pointer in the stack
	public static final String					LOADHP			= "lhp" + Syskb.CR;			// load heap pointer in the stack
	public static final String					LOADRA			= "lra" + Syskb.CR;			// load from ra
	public static final String					LOADRV			= "lrv" + Syskb.CR;			// load from rv
	public static final String					LOADW			= "lw" + Syskb.CR;				// load a value from the memory cell pointed by top
	public static IOutput<String>				log;
	public static final String					LPAR			= "LPAR";
	public static final String					MINUS			= "-";
	public static final String					MULT			= "mult" + Syskb.CR;
	public static final String					NAT				= "N";
	public static final String					NOT				= "not";
	public static final String					NULL			= "Null";
	public static final String					NULLTYPE		= "null";
	public static final String					OR				= "||";
	public static final String					PADD			= "+";
	// perch� parameterOffset = 2? perch� in ogni funzione al suo inizio inserisco l'id dell'oggetto chiamante
	public static final int						parameterOffset	= 2;
	public static final String					PDIV			= "/";
	public static final String					PMULT			= "*";
	public static final String					POP				= "pop" + Syskb.CR;			// decreases stack pointer
	public static final String					PRINT			= "print" + Syskb.CR;
	public static final String					PROG			= "prog";
	public static final String					PSUB			= "-";
	public static final String					PUSH			= "push ";
	public static final String					RPAR			= "RPAR";
	public static final String					STOREFP			= "sfp" + Syskb.CR;			// store top into frame pointer
	public static final String					STOREHP			= "shp" + Syskb.CR;			// store top into heap pointer
	public static final String					STORERA			= "sra" + Syskb.CR;			// store top into ra
	public static final String					STORERV			= "srv" + Syskb.CR;			// store top into rv
	public static final String					STOREW			= "sw" + Syskb.CR;				// store in the memory cell pointed by top the value next
	public static final String					SUB				= "sub" + Syskb.CR;
	public static final String					THEN			= "then";
	
	public static final String					VAR				= "var";

	public static final String	OBJECT	= "Object";
	public static boolean						VERBOSE			= false;
	
	public static void addClass(final String classId, final ClassNode claz) {
		if (Syskb.classMap.put(classId, claz) != null) {
			System.out.println("Class id " + classId + " already declared");
			System.exit(0);
		}
	}
	
	public static ClassNode getClass(final String classId) {
		final ClassNode c = Syskb.classMap.get(classId);
		if (c == null) {
			System.out.println("Class " + classId + " not found");
			System.exit(0);
		}
		return c;
	}
	
	public static IOutput<String> getLog() {
		return Syskb.log;
	}
	
	public static boolean isClass(final String classId) {
		return Syskb.classMap.get(classId) == null ? false : true;
	}
	
	public static void log(final String logg) {
		if (Syskb.VERBOSE) {
			Syskb.log.addOutput(logg);
		}
	}
	
	public static void reset() {
		classMap = new HashMap<>();
	}
	
	public static void setLog(final IOutput<String> logg) {
		Syskb.log = logg;
		if (Syskb.log != null) {
			Syskb.VERBOSE = true;
		}
	}
}
