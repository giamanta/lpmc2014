// $ANTLR 3.5.1 C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g 2015-03-13 12:03:36
// {{{
package it.unibo.lpemc.implementation;

import it.unibo.lpemc.implementation.oop.ClassNode;
import it.unibo.lpemc.implementation.oop.NullNode;
import it.unibo.lpemc.implementation.oop.ObjectNode;
import it.unibo.lpemc.implementation.operators.AddNode;
import it.unibo.lpemc.implementation.operators.AndNode;
import it.unibo.lpemc.implementation.operators.DivNode;
import it.unibo.lpemc.implementation.operators.EqualNode;
import it.unibo.lpemc.implementation.operators.GreatNode;
import it.unibo.lpemc.implementation.operators.GreaterEqualNode;
import it.unibo.lpemc.implementation.operators.IfNode;
import it.unibo.lpemc.implementation.operators.LessEqualNode;
import it.unibo.lpemc.implementation.operators.LessNode;
import it.unibo.lpemc.implementation.operators.MultNode;
import it.unibo.lpemc.implementation.operators.NotNode;
import it.unibo.lpemc.implementation.operators.OrNode;
import it.unibo.lpemc.implementation.operators.PrintNode;
import it.unibo.lpemc.implementation.operators.SubNode;
import it.unibo.lpemc.implementation.types.ArrowTypeNode;
import it.unibo.lpemc.implementation.types.BoolTypeNode;
import it.unibo.lpemc.implementation.types.IdType;
import it.unibo.lpemc.implementation.types.IntTypeNode;
import it.unibo.lpemc.implementation.values.BoolNode;
import it.unibo.lpemc.implementation.values.NatNode;
import it.unibo.lpemc.interfaces.Node;

import java.util.ArrayList;
import java.util.HashMap;

import org.antlr.runtime.BitSet;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.Parser;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenStream;

@SuppressWarnings("all")
public class FOOLParser extends Parser {
	public static final String[]	tokenNames	= new String[] { "<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "ARROW", "ASS", "BOOL", "CLASS", "CLPAR", "COLON", "COMMA", "COMMENT", "CRPAR", "DIV",
			"DOT", "ELSE", "EQ", "ERR", "EXTENDS", "FALSE", "FUN", "GR", "GREAT", "ICOMMENT", "ID", "IF", "IN", "INT", "INTEGER", "LE", "LESS", "LET", "LPAR", "MINUS", "MULT", "NEW", "NOT", "NULL",
			"OR", "PLUS", "PRINT", "RPAR", "SEMIC", "THEN", "TRUE", "VAR", "WHITESP" };
	public static final int			EOF			= -1;
	public static final int			AND			= 4;
	public static final int			ARROW		= 5;
	public static final int			ASS			= 6;
	public static final int			BOOL		= 7;
	public static final int			CLASS		= 8;
	public static final int			CLPAR		= 9;
	public static final int			COLON		= 10;
	public static final int			COMMA		= 11;
	public static final int			COMMENT		= 12;
	public static final int			CRPAR		= 13;
	public static final int			DIV			= 14;
	public static final int			DOT			= 15;
	public static final int			ELSE		= 16;
	public static final int			EQ			= 17;
	public static final int			ERR			= 18;
	public static final int			EXTENDS		= 19;
	public static final int			FALSE		= 20;
	public static final int			FUN			= 21;
	public static final int			GR			= 22;
	public static final int			GREAT		= 23;
	public static final int			ICOMMENT	= 24;
	public static final int			ID			= 25;
	public static final int			IF			= 26;
	public static final int			IN			= 27;
	public static final int			INT			= 28;
	public static final int			INTEGER		= 29;
	public static final int			LE			= 30;
	public static final int			LESS		= 31;
	public static final int			LET			= 32;
	public static final int			LPAR		= 33;
	public static final int			MINUS		= 34;
	public static final int			MULT		= 35;
	public static final int			NEW			= 36;
	public static final int			NOT			= 37;
	public static final int			NULL		= 38;
	public static final int			OR			= 39;
	public static final int			PLUS		= 40;
	public static final int			PRINT		= 41;
	public static final int			RPAR		= 42;
	public static final int			SEMIC		= 43;
	public static final int			THEN		= 44;
	public static final int			TRUE		= 45;
	public static final int			VAR			= 46;
	public static final int			WHITESP		= 47;
	
	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}
	
	// delegators
	
	public FOOLParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	
	public FOOLParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}
	
	@Override
	public String[] getTokenNames() {
		return FOOLParser.tokenNames;
	}
	
	@Override
	public String getGrammarFileName() {
		return "C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g";
	}
	
	private ArrayList<HashMap<String, STentry>>	symTable		= new ArrayList<HashMap<String, STentry>>();
	private int									nestingLevel	= -1;
	
	// $ANTLR start "prog"
	// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:17:1: prog returns [Node ast] : (e= exp SEMIC | LET c= cllist d= declist IN e= exp SEMIC );
	public final Node prog() throws RecognitionException {
		Node ast = null;
		
		Node e = null;
		ArrayList<Node> c = null;
		ArrayList<Node> d = null;
		
		try {
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:18:2: (e= exp SEMIC | LET c= cllist d= declist IN e= exp SEMIC )
			int alt1 = 2;
			int LA1_0 = input.LA(1);
			if ((LA1_0 == FALSE || (LA1_0 >= ID && LA1_0 <= IF) || LA1_0 == INTEGER || LA1_0 == LPAR || (LA1_0 >= NEW && LA1_0 <= NULL) || LA1_0 == PRINT || LA1_0 == TRUE)) {
				alt1 = 1;
			} else if ((LA1_0 == LET)) {
				alt1 = 2;
			}
			
			else {
				NoViableAltException nvae = new NoViableAltException("", 1, 0, input);
				throw nvae;
			}
			
			switch (alt1) {
				case 1:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:18:4: e= exp SEMIC
				{
					pushFollow(FOLLOW_exp_in_prog38);
					e = exp();
					state._fsp--;
					
					match(input, SEMIC, FOLLOW_SEMIC_in_prog40);
					ast = new ProgNode(e);
				}
					break;
				case 2:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:19:5: LET c= cllist d= declist IN e= exp SEMIC
				{
					match(input, LET, FOLLOW_LET_in_prog48);
					nestingLevel++;
					HashMap<String, STentry> hm = new HashMap<String, STentry>();
					symTable.add(hm);
					
					pushFollow(FOLLOW_cllist_in_prog62);
					c = cllist();
					state._fsp--;
					
					pushFollow(FOLLOW_declist_in_prog70);
					d = declist();
					state._fsp--;
					
					match(input, IN, FOLLOW_IN_in_prog72);
					pushFollow(FOLLOW_exp_in_prog76);
					e = exp();
					state._fsp--;
					
					match(input, SEMIC, FOLLOW_SEMIC_in_prog78);
					symTable.remove(nestingLevel--);
					ast = new LetInNode(d, e);
					
				}
					break;
			
			}
		} catch (RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return ast;
	}
	
	// $ANTLR end "prog"
	
	// $ANTLR start "cllist"
	// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:32:1: cllist returns [ArrayList<Node> classList] : ( CLASS className= ID ( EXTENDS extClassName= ID )? LPAR (field= ID COLON fieldType= basic ( COMMA otherField= ID COLON otherFieldType= basic )* )? RPAR CLPAR ( FUN function= ID COLON functionReturnType= basic LPAR (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )? RPAR ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )?
	// expression= exp SEMIC )* CRPAR )* ;
	public final ArrayList<Node> cllist() throws RecognitionException {
		ArrayList<Node> classList = null;
		
		Token className = null;
		Token extClassName = null;
		Token field = null;
		Token otherField = null;
		Token function = null;
		Token parameter = null;
		Token otherParameter = null;
		Token varId = null;
		Node fieldType = null;
		Node otherFieldType = null;
		Node functionReturnType = null;
		Node parameterType = null;
		Node otherParameterType = null;
		Node varType = null;
		Node varValue = null;
		Node expression = null;
		
		try {
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:33:3: ( ( CLASS className= ID ( EXTENDS extClassName= ID )? LPAR (field= ID COLON fieldType= basic ( COMMA otherField= ID COLON otherFieldType= basic )* )? RPAR CLPAR ( FUN function= ID COLON functionReturnType= basic LPAR (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )? RPAR ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )? expression= exp SEMIC )* CRPAR )*
			// )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:35:3: ( CLASS className= ID ( EXTENDS extClassName= ID )? LPAR (field= ID COLON fieldType= basic ( COMMA otherField= ID COLON otherFieldType= basic )* )? RPAR CLPAR ( FUN function= ID COLON functionReturnType= basic LPAR (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )? RPAR ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )? expression= exp SEMIC )* CRPAR )*
			{
				classList = new ArrayList<Node>();
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:37:3: ( CLASS className= ID ( EXTENDS extClassName= ID )? LPAR (field= ID COLON fieldType= basic ( COMMA otherField= ID COLON otherFieldType= basic )* )? RPAR CLPAR ( FUN function= ID COLON functionReturnType= basic LPAR (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )? RPAR ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )? expression= exp SEMIC )* CRPAR
				// )*
				loop10: while (true) {
					int alt10 = 2;
					int LA10_0 = input.LA(1);
					if ((LA10_0 == CLASS)) {
						alt10 = 1;
					}
					
					switch (alt10) {
						case 1:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:37:4: CLASS className= ID ( EXTENDS extClassName= ID )? LPAR (field= ID COLON fieldType= basic ( COMMA otherField= ID COLON otherFieldType= basic )* )? RPAR CLPAR ( FUN function= ID COLON functionReturnType= basic LPAR (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )? RPAR ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )? expression= exp SEMIC )*
						// CRPAR
						{
							match(input, CLASS, FOLLOW_CLASS_in_cllist116);
							className = (Token) match(input, ID, FOLLOW_ID_in_cllist120);
							
							ClassNode classNode = new ClassNode((className != null ? className.getText() : null));
							classList.add(classNode);
							// ora inserisco un nuovo nesting level per i campi e i metodi interni alla classe
							nestingLevel++;
							// creo una nuova hashmap e la inserisco nella lista
							HashMap<String, STentry> hmn = new HashMap<String, STentry>();
							symTable.add(hmn);
							
							// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:48:3: ( EXTENDS extClassName= ID )?
							int alt2 = 2;
							int LA2_0 = input.LA(1);
							if ((LA2_0 == EXTENDS)) {
								alt2 = 1;
							}
							switch (alt2) {
								case 1:
								// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:48:4: EXTENDS extClassName= ID
								{
									match(input, EXTENDS, FOLLOW_EXTENDS_in_cllist133);
									extClassName = (Token) match(input, ID, FOLLOW_ID_in_cllist137);
									classNode.setSuperClass((extClassName != null ? extClassName.getText() : null));
								}
									break;
							
							}
							
							ArrayList<Node> fieldTypes = new ArrayList<Node>();
							Offset fieldOffset = new Offset(1);
							match(input, LPAR, FOLLOW_LPAR_in_cllist152);
							// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:51:8: (field= ID COLON fieldType= basic ( COMMA otherField= ID COLON otherFieldType= basic )* )?
							int alt4 = 2;
							int LA4_0 = input.LA(1);
							if ((LA4_0 == ID)) {
								alt4 = 1;
							}
							switch (alt4) {
								case 1:
								// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:51:9: field= ID COLON fieldType= basic ( COMMA otherField= ID COLON otherFieldType= basic )*
								{
									field = (Token) match(input, ID, FOLLOW_ID_in_cllist157);
									match(input, COLON, FOLLOW_COLON_in_cllist159);
									pushFollow(FOLLOW_basic_in_cllist163);
									fieldType = basic();
									state._fsp--;
									
									fieldTypes.add(fieldType);
									FOOLlib.factory.createFieldInClass((field != null ? field.getLine() : 0), classNode, (field != null ? field.getText() : null), fieldType, hmn, nestingLevel,
											fieldOffset);
									
									// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:55:3: ( COMMA otherField= ID COLON otherFieldType= basic )*
									loop3: while (true) {
										int alt3 = 2;
										int LA3_0 = input.LA(1);
										if ((LA3_0 == COMMA)) {
											alt3 = 1;
										}
										
										switch (alt3) {
											case 1:
											// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:55:4: COMMA otherField= ID COLON otherFieldType= basic
											{
												match(input, COMMA, FOLLOW_COMMA_in_cllist172);
												otherField = (Token) match(input, ID, FOLLOW_ID_in_cllist176);
												match(input, COLON, FOLLOW_COLON_in_cllist178);
												pushFollow(FOLLOW_basic_in_cllist182);
												otherFieldType = basic();
												state._fsp--;
												
												fieldTypes.add(otherFieldType);
												FOOLlib.factory.createFieldInClass((otherField != null ? otherField.getLine() : 0), classNode, (otherField != null ? otherField.getText() : null),
														otherFieldType, hmn, nestingLevel, fieldOffset);
												
											}
												break;
											
											default:
												break loop3;
										}
									}
									
								}
									break;
							
							}
							
							match(input, RPAR, FOLLOW_RPAR_in_cllist197);
							match(input, CLPAR, FOLLOW_CLPAR_in_cllist202);
							Offset classMemberOffset = new Offset(ClassNode.indexStart);
							// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:62:4: ( FUN function= ID COLON functionReturnType= basic LPAR (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )? RPAR ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )? expression= exp SEMIC )*
							loop9: while (true) {
								int alt9 = 2;
								int LA9_0 = input.LA(1);
								if ((LA9_0 == FUN)) {
									alt9 = 1;
								}
								
								switch (alt9) {
									case 1:
									// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:64:4: FUN function= ID COLON functionReturnType= basic LPAR (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )? RPAR ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )? expression= exp SEMIC
									{
										match(input, FUN, FOLLOW_FUN_in_cllist219);
										function = (Token) match(input, ID, FOLLOW_ID_in_cllist223);
										match(input, COLON, FOLLOW_COLON_in_cllist225);
										pushFollow(FOLLOW_basic_in_cllist229);
										functionReturnType = basic();
										state._fsp--;
										
										MethodNode method = FOOLlib.factory.createMethodInClass((function != null ? function.getLine() : 0), classNode, (function != null ? function.getText() : null),
												functionReturnType, symTable, hmn, nestingLevel, fieldOffset);
										// creo un nuovo nesting level per la funzione
										nestingLevel++;
										
										match(input, LPAR, FOLLOW_LPAR_in_cllist243);
										
										Offset parOffset = new Offset(1);
										ArrayList<Node> parTypes = new ArrayList<Node>();
										
										// il primo argomento di una funzione � sempre un id type
										IdType idType = new IdType((className != null ? className.getText() : null));
										parTypes.add(idType);
										
										FOOLlib.factory.addParamInFunction(-1, method, "__this___", idType, nestingLevel, parOffset);
										
										// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:81:4: (parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )* )?
										int alt6 = 2;
										int LA6_0 = input.LA(1);
										if ((LA6_0 == ID)) {
											alt6 = 1;
										}
										switch (alt6) {
											case 1:
											// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:81:5: parameter= ID COLON parameterType= type ( COMMA otherParameter= ID COLON otherParameterType= type )*
											{
												parameter = (Token) match(input, ID, FOLLOW_ID_in_cllist253);
												match(input, COLON, FOLLOW_COLON_in_cllist255);
												pushFollow(FOLLOW_type_in_cllist259);
												parameterType = type();
												state._fsp--;
												
												parTypes.add(parameterType);
												FOOLlib.factory.addParamInFunction((parameter != null ? parameter.getLine() : 0), method, (parameter != null ? parameter.getText() : null),
														parameterType, nestingLevel, parOffset);
												
												// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:85:7: ( COMMA otherParameter= ID COLON otherParameterType= type )*
												loop5: while (true) {
													int alt5 = 2;
													int LA5_0 = input.LA(1);
													if ((LA5_0 == COMMA)) {
														alt5 = 1;
													}
													
													switch (alt5) {
														case 1:
														// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:85:8: COMMA otherParameter= ID COLON otherParameterType= type
														{
															match(input, COMMA, FOLLOW_COMMA_in_cllist275);
															otherParameter = (Token) match(input, ID, FOLLOW_ID_in_cllist279);
															match(input, COLON, FOLLOW_COLON_in_cllist281);
															pushFollow(FOLLOW_type_in_cllist285);
															otherParameterType = type();
															state._fsp--;
															
															parTypes.add(otherParameterType);
															FOOLlib.factory.addParamInFunction((otherParameter != null ? otherParameter.getLine() : 0), method,
																	(otherParameter != null ? otherParameter.getText() : null), otherParameterType, nestingLevel, parOffset);
															
														}
															break;
														
														default:
															break loop5;
													}
												}
												
											}
												break;
										
										}
										
										match(input, RPAR, FOLLOW_RPAR_in_cllist303);
										
										method.getSTEntry().setType(new ArrowTypeNode(parTypes, functionReturnType));
										ArrayList<Node> decList = new ArrayList<Node>();
										Offset varOffset = new Offset(-2);
										
										// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:94:4: ( LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN )?
										int alt8 = 2;
										int LA8_0 = input.LA(1);
										if ((LA8_0 == LET)) {
											alt8 = 1;
										}
										switch (alt8) {
											case 1:
											// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:94:5: LET ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )* IN
											{
												match(input, LET, FOLLOW_LET_in_cllist311);
												// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:95:5: ( VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC )*
												loop7: while (true) {
													int alt7 = 2;
													int LA7_0 = input.LA(1);
													if ((LA7_0 == VAR)) {
														alt7 = 1;
													}
													
													switch (alt7) {
														case 1:
														// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:96:9: VAR varId= ID COLON varType= basic ASS varValue= exp SEMIC
														{
															match(input, VAR, FOLLOW_VAR_in_cllist327);
															varId = (Token) match(input, ID, FOLLOW_ID_in_cllist331);
															match(input, COLON, FOLLOW_COLON_in_cllist333);
															pushFollow(FOLLOW_basic_in_cllist337);
															varType = basic();
															state._fsp--;
															
															match(input, ASS, FOLLOW_ASS_in_cllist339);
															pushFollow(FOLLOW_exp_in_cllist343);
															varValue = exp();
															state._fsp--;
															
															match(input, SEMIC, FOLLOW_SEMIC_in_cllist345);
															decList.add(FOOLlib.factory.addVarInFunction((varId != null ? varId.getLine() : 0), method, (varId != null ? varId.getText() : null),
																	varType, varValue, nestingLevel, varOffset));
														}
															break;
														
														default:
															break loop7;
													}
												}
												
												match(input, IN, FOLLOW_IN_in_cllist369);
											}
												break;
										
										}
										
										pushFollow(FOLLOW_exp_in_cllist376);
										expression = exp();
										state._fsp--;
										
										match(input, SEMIC, FOLLOW_SEMIC_in_cllist378);
										// chiudere scope
										symTable.remove(nestingLevel--); // rimuovo il nl della funzione
										method.addDecBody(decList, expression);
										classNode.addMethod(method);
										
									}
										break;
									
									default:
										break loop9;
								}
							}
							
							match(input, CRPAR, FOLLOW_CRPAR_in_cllist397);
							symTable.remove(nestingLevel--); /* rimuovo nl della classe */
						}
							break;
						
						default:
							break loop10;
					}
				}
				
			}
			
		} catch (RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return classList;
	}
	
	// $ANTLR end "cllist"
	
	// $ANTLR start "declist"
	// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:112:1: declist returns [ArrayList<Node> astlist] : ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp SEMIC )* ;
	public final ArrayList<Node> declist() throws RecognitionException {
		ArrayList<Node> astlist = null;
		
		Token i = null;
		Token fid = null;
		Token id = null;
		Node t = null;
		Node e = null;
		Node fty = null;
		Node ty = null;
		ArrayList<Node> d = null;
		
		try {
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:113:2: ( ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp SEMIC )* )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:113:4: ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp SEMIC )*
			{
				astlist = new ArrayList<Node>();
				Offset varOffset = new Offset(-2);
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:114:4: ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp SEMIC )*
				loop14: while (true) {
					int alt14 = 3;
					int LA14_0 = input.LA(1);
					if ((LA14_0 == VAR)) {
						alt14 = 1;
					} else if ((LA14_0 == FUN)) {
						alt14 = 2;
					}
					
					switch (alt14) {
						case 1:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:114:6: VAR i= ID COLON t= type ASS e= exp SEMIC
						{
							match(input, VAR, FOLLOW_VAR_in_declist430);
							i = (Token) match(input, ID, FOLLOW_ID_in_declist434);
							match(input, COLON, FOLLOW_COLON_in_declist436);
							pushFollow(FOLLOW_type_in_declist440);
							t = type();
							state._fsp--;
							
							match(input, ASS, FOLLOW_ASS_in_declist442);
							pushFollow(FOLLOW_exp_in_declist446);
							e = exp();
							state._fsp--;
							
							match(input, SEMIC, FOLLOW_SEMIC_in_declist448);
							astlist.add(FOOLlib.factory.createVariable((i != null ? i.getLine() : 0), (i != null ? i.getText() : null), t, e, symTable, nestingLevel, varOffset));
						}
							break;
						case 2:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:116:9: FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp SEMIC
						{
							match(input, FUN, FOLLOW_FUN_in_declist466);
							i = (Token) match(input, ID, FOLLOW_ID_in_declist470);
							match(input, COLON, FOLLOW_COLON_in_declist472);
							pushFollow(FOLLOW_type_in_declist476);
							t = type();
							state._fsp--;
							
							FunNode f = FOOLlib.factory.createFunction((i != null ? i.getLine() : 0), (i != null ? i.getText() : null), t, symTable, nestingLevel, varOffset);
							astlist.add(f);
							nestingLevel++;
							
							match(input, LPAR, FOLLOW_LPAR_in_declist496);
							ArrayList<Node> parTypes = new ArrayList<Node>();
							Offset parOffset = new Offset(1);
							
							// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:125:9: (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )?
							int alt12 = 2;
							int LA12_0 = input.LA(1);
							if ((LA12_0 == ID)) {
								alt12 = 1;
							}
							switch (alt12) {
								case 1:
								// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:125:11: fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )*
								{
									fid = (Token) match(input, ID, FOLLOW_ID_in_declist520);
									match(input, COLON, FOLLOW_COLON_in_declist522);
									pushFollow(FOLLOW_type_in_declist526);
									fty = type();
									state._fsp--;
									
									parTypes.add(fty);
									FOOLlib.factory.addParamInFunction((fid != null ? fid.getLine() : 0), f, (fid != null ? fid.getText() : null), fty, nestingLevel, parOffset);
									
									// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:129:11: ( COMMA id= ID COLON ty= type )*
									loop11: while (true) {
										int alt11 = 2;
										int LA11_0 = input.LA(1);
										if ((LA11_0 == COMMA)) {
											alt11 = 1;
										}
										
										switch (alt11) {
											case 1:
											// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:129:13: COMMA id= ID COLON ty= type
											{
												match(input, COMMA, FOLLOW_COMMA_in_declist552);
												id = (Token) match(input, ID, FOLLOW_ID_in_declist556);
												match(input, COLON, FOLLOW_COLON_in_declist558);
												pushFollow(FOLLOW_type_in_declist562);
												ty = type();
												state._fsp--;
												
												parTypes.add(ty);
												FOOLlib.factory.addParamInFunction((id != null ? id.getLine() : 0), f, (id != null ? id.getText() : null), ty, nestingLevel, parOffset);
												
											}
												break;
											
											default:
												break loop11;
										}
									}
									
								}
									break;
							
							}
							
							match(input, RPAR, FOLLOW_RPAR_in_declist611);
							f.getSTEntry().setType(new ArrowTypeNode(parTypes, t));
							ArrayList<Node> decList = new ArrayList<Node>();
							// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:137:9: ( LET d= declist IN )?
							int alt13 = 2;
							int LA13_0 = input.LA(1);
							if ((LA13_0 == LET)) {
								alt13 = 1;
							}
							switch (alt13) {
								case 1:
								// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:137:10: LET d= declist IN
								{
									match(input, LET, FOLLOW_LET_in_declist625);
									pushFollow(FOLLOW_declist_in_declist629);
									d = declist();
									state._fsp--;
									
									match(input, IN, FOLLOW_IN_in_declist631);
									decList = d;
								}
									break;
							
							}
							
							pushFollow(FOLLOW_exp_in_declist639);
							e = exp();
							state._fsp--;
							
							match(input, SEMIC, FOLLOW_SEMIC_in_declist641);
							// chiudere scope
							symTable.remove(nestingLevel--);
							f.addDecBody(decList, e);
							
						}
							break;
						
						default:
							break loop14;
					}
				}
				
			}
			
		} catch (RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return astlist;
	}
	
	// $ANTLR end "declist"
	
	// $ANTLR start "exp"
	// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:145:1: exp returns [Node ast] : f= term ( PLUS l= term | MINUS l= term | OR l= term )* ;
	public final Node exp() throws RecognitionException {
		Node ast = null;
		
		Node f = null;
		Node l = null;
		
		try {
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:146:3: (f= term ( PLUS l= term | MINUS l= term | OR l= term )* )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:146:5: f= term ( PLUS l= term | MINUS l= term | OR l= term )*
			{
				pushFollow(FOLLOW_term_in_exp677);
				f = term();
				state._fsp--;
				
				ast = f;
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:147:5: ( PLUS l= term | MINUS l= term | OR l= term )*
				loop15: while (true) {
					int alt15 = 4;
					switch (input.LA(1)) {
						case PLUS: {
							alt15 = 1;
						}
							break;
						case MINUS: {
							alt15 = 2;
						}
							break;
						case OR: {
							alt15 = 3;
						}
							break;
					}
					switch (alt15) {
						case 1:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:147:9: PLUS l= term
						{
							match(input, PLUS, FOLLOW_PLUS_in_exp689);
							pushFollow(FOLLOW_term_in_exp694);
							l = term();
							state._fsp--;
							
							ast = new AddNode(ast, l);
						}
							break;
						case 2:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:148:9: MINUS l= term
						{
							match(input, MINUS, FOLLOW_MINUS_in_exp706);
							pushFollow(FOLLOW_term_in_exp710);
							l = term();
							state._fsp--;
							
							ast = new SubNode(ast, l);
						}
							break;
						case 3:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:149:9: OR l= term
						{
							match(input, OR, FOLLOW_OR_in_exp722);
							pushFollow(FOLLOW_term_in_exp729);
							l = term();
							state._fsp--;
							
							ast = new OrNode(ast, l);
						}
							break;
						
						default:
							break loop15;
					}
				}
				
			}
			
		} catch (RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return ast;
	}
	
	// $ANTLR end "exp"
	
	// $ANTLR start "term"
	// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:154:1: term returns [Node ast] : f= factor ( MULT l= factor | DIV l= factor | AND l= factor )* ;
	public final Node term() throws RecognitionException {
		Node ast = null;
		
		Node f = null;
		Node l = null;
		
		try {
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:155:2: (f= factor ( MULT l= factor | DIV l= factor | AND l= factor )* )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:155:4: f= factor ( MULT l= factor | DIV l= factor | AND l= factor )*
			{
				pushFollow(FOLLOW_factor_in_term758);
				f = factor();
				state._fsp--;
				
				ast = f;
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:156:4: ( MULT l= factor | DIV l= factor | AND l= factor )*
				loop16: while (true) {
					int alt16 = 4;
					switch (input.LA(1)) {
						case MULT: {
							alt16 = 1;
						}
							break;
						case DIV: {
							alt16 = 2;
						}
							break;
						case AND: {
							alt16 = 3;
						}
							break;
					}
					switch (alt16) {
						case 1:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:156:8: MULT l= factor
						{
							match(input, MULT, FOLLOW_MULT_in_term769);
							pushFollow(FOLLOW_factor_in_term773);
							l = factor();
							state._fsp--;
							
							ast = new MultNode(ast, l);
						}
							break;
						case 2:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:157:9: DIV l= factor
						{
							match(input, DIV, FOLLOW_DIV_in_term785);
							pushFollow(FOLLOW_factor_in_term790);
							l = factor();
							state._fsp--;
							
							ast = new DivNode(ast, l);
						}
							break;
						case 3:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:158:8: AND l= factor
						{
							match(input, AND, FOLLOW_AND_in_term801);
							pushFollow(FOLLOW_factor_in_term806);
							l = factor();
							state._fsp--;
							
							ast = new AndNode(ast, l);
						}
							break;
						
						default:
							break loop16;
					}
				}
				
			}
			
		} catch (RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return ast;
	}
	
	// $ANTLR end "term"
	
	// $ANTLR start "factor"
	// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:163:1: factor returns [Node ast] : f= value ( EQ l= value | GR l= value | LE l= value | GREAT l= value | LESS l= value )* ;
	public final Node factor() throws RecognitionException {
		Node ast = null;
		
		Node f = null;
		Node l = null;
		
		try {
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:164:2: (f= value ( EQ l= value | GR l= value | LE l= value | GREAT l= value | LESS l= value )* )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:164:4: f= value ( EQ l= value | GR l= value | LE l= value | GREAT l= value | LESS l= value )*
			{
				pushFollow(FOLLOW_value_in_factor833);
				f = value();
				state._fsp--;
				
				ast = f;
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:165:4: ( EQ l= value | GR l= value | LE l= value | GREAT l= value | LESS l= value )*
				loop17: while (true) {
					int alt17 = 6;
					switch (input.LA(1)) {
						case EQ: {
							alt17 = 1;
						}
							break;
						case GR: {
							alt17 = 2;
						}
							break;
						case LE: {
							alt17 = 3;
						}
							break;
						case GREAT: {
							alt17 = 4;
						}
							break;
						case LESS: {
							alt17 = 5;
						}
							break;
					}
					switch (alt17) {
						case 1:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:165:8: EQ l= value
						{
							match(input, EQ, FOLLOW_EQ_in_factor844);
							pushFollow(FOLLOW_value_in_factor848);
							l = value();
							state._fsp--;
							
							ast = new EqualNode(ast, l);
						}
							break;
						case 2:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:166:8: GR l= value
						{
							match(input, GR, FOLLOW_GR_in_factor859);
							pushFollow(FOLLOW_value_in_factor863);
							l = value();
							state._fsp--;
							
							ast = new GreaterEqualNode(ast, l);
						}
							break;
						case 3:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:167:8: LE l= value
						{
							match(input, LE, FOLLOW_LE_in_factor874);
							pushFollow(FOLLOW_value_in_factor878);
							l = value();
							state._fsp--;
							
							ast = new LessEqualNode(ast, l);
						}
							break;
						case 4:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:168:8: GREAT l= value
						{
							match(input, GREAT, FOLLOW_GREAT_in_factor889);
							pushFollow(FOLLOW_value_in_factor893);
							l = value();
							state._fsp--;
							
							ast = new GreatNode(ast, l);
						}
							break;
						case 5:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:169:8: LESS l= value
						{
							match(input, LESS, FOLLOW_LESS_in_factor904);
							pushFollow(FOLLOW_value_in_factor908);
							l = value();
							state._fsp--;
							
							ast = new LessNode(ast, l);
						}
							break;
						
						default:
							break loop17;
					}
				}
				
			}
			
		} catch (RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return ast;
	}
	
	// $ANTLR end "factor"
	
	// $ANTLR start "value"
	// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:174:1: value returns [Node ast] : (n= INTEGER | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ( ELSE CLPAR z= exp CRPAR )? | PRINT LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT identifier= ID ( LPAR (firstArgument= exp ( COMMA otherArgument= exp )* )? RPAR )? )? | NULL | NEW className= ID LPAR (paramater= exp ( COMMA otherParameter= exp )* )? RPAR | NOT LPAR e= exp RPAR );
	public final Node value() throws RecognitionException {
		Node ast = null;
		
		Token n = null;
		Token i = null;
		Token identifier = null;
		Token className = null;
		Node e = null;
		Node x = null;
		Node y = null;
		Node z = null;
		Node fa = null;
		Node a = null;
		Node firstArgument = null;
		Node otherArgument = null;
		Node paramater = null;
		Node otherParameter = null;
		
		try {
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:175:2: (n= INTEGER | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ( ELSE CLPAR z= exp CRPAR )? | PRINT LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT identifier= ID ( LPAR (firstArgument= exp ( COMMA otherArgument= exp )* )? RPAR )? )? | NULL | NEW className= ID LPAR (paramater= exp ( COMMA otherParameter= exp )* )? RPAR | NOT LPAR e= exp RPAR )
			int alt27 = 10;
			switch (input.LA(1)) {
				case INTEGER: {
					alt27 = 1;
				}
					break;
				case TRUE: {
					alt27 = 2;
				}
					break;
				case FALSE: {
					alt27 = 3;
				}
					break;
				case LPAR: {
					alt27 = 4;
				}
					break;
				case IF: {
					alt27 = 5;
				}
					break;
				case PRINT: {
					alt27 = 6;
				}
					break;
				case ID: {
					alt27 = 7;
				}
					break;
				case NULL: {
					alt27 = 8;
				}
					break;
				case NEW: {
					alt27 = 9;
				}
					break;
				case NOT: {
					alt27 = 10;
				}
					break;
				default:
					NoViableAltException nvae = new NoViableAltException("", 27, 0, input);
					throw nvae;
			}
			switch (alt27) {
				case 1:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:175:4: n= INTEGER
				{
					n = (Token) match(input, INTEGER, FOLLOW_INTEGER_in_value937);
					Integer integer = Integer.parseInt((n != null ? n.getText() : null));
					if (integer == 0)
						ast = new BoolNode(false);
					else if (integer == 1)
						ast = new BoolNode(true);
					else
						ast = new NatNode(integer);
				}
					break;
				case 2:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:180:4: TRUE
				{
					match(input, TRUE, FOLLOW_TRUE_in_value947);
					ast = new BoolNode(true);
				}
					break;
				case 3:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:182:4: FALSE
				{
					match(input, FALSE, FOLLOW_FALSE_in_value957);
					ast = new BoolNode(false);
				}
					break;
				case 4:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:184:4: LPAR e= exp RPAR
				{
					match(input, LPAR, FOLLOW_LPAR_in_value967);
					pushFollow(FOLLOW_exp_in_value973);
					e = exp();
					state._fsp--;
					
					match(input, RPAR, FOLLOW_RPAR_in_value975);
					ast = e;
				}
					break;
				case 5:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:186:4: IF x= exp THEN CLPAR y= exp CRPAR ( ELSE CLPAR z= exp CRPAR )?
				{
					match(input, IF, FOLLOW_IF_in_value985);
					pushFollow(FOLLOW_exp_in_value989);
					x = exp();
					state._fsp--;
					
					match(input, THEN, FOLLOW_THEN_in_value991);
					match(input, CLPAR, FOLLOW_CLPAR_in_value993);
					pushFollow(FOLLOW_exp_in_value997);
					y = exp();
					state._fsp--;
					
					match(input, CRPAR, FOLLOW_CRPAR_in_value999);
					// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:187:3: ( ELSE CLPAR z= exp CRPAR )?
					int alt18 = 2;
					int LA18_0 = input.LA(1);
					if ((LA18_0 == ELSE)) {
						alt18 = 1;
					}
					switch (alt18) {
						case 1:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:187:4: ELSE CLPAR z= exp CRPAR
						{
							match(input, ELSE, FOLLOW_ELSE_in_value1004);
							match(input, CLPAR, FOLLOW_CLPAR_in_value1006);
							pushFollow(FOLLOW_exp_in_value1010);
							z = exp();
							state._fsp--;
							
							match(input, CRPAR, FOLLOW_CRPAR_in_value1012);
						}
							break;
					
					}
					
					ast = new IfNode(x, y, z);
				}
					break;
				case 6:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:189:4: PRINT LPAR e= exp RPAR
				{
					match(input, PRINT, FOLLOW_PRINT_in_value1024);
					match(input, LPAR, FOLLOW_LPAR_in_value1026);
					pushFollow(FOLLOW_exp_in_value1030);
					e = exp();
					state._fsp--;
					
					match(input, RPAR, FOLLOW_RPAR_in_value1032);
					ast = new PrintNode(e);
				}
					break;
				case 7:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:191:4: i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT identifier= ID ( LPAR (firstArgument= exp ( COMMA otherArgument= exp )* )? RPAR )? )?
				{
					i = (Token) match(input, ID, FOLLOW_ID_in_value1044);
					// cercare la dichiarazione
					STentry entry = null;
					int j = nestingLevel;
					
					while (j >= 0 && entry == null)
						entry = symTable.get(j--).get((i != null ? i.getText() : null));
					
					ast = FOOLlib.factory.checkId((i != null ? i.getLine() : 0), (i != null ? i.getText() : null), entry, nestingLevel - j - 1);
					
					// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:201:5: ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT identifier= ID ( LPAR (firstArgument= exp ( COMMA otherArgument= exp )* )? RPAR )? )?
					int alt24 = 3;
					int LA24_0 = input.LA(1);
					if ((LA24_0 == LPAR)) {
						alt24 = 1;
					} else if ((LA24_0 == DOT)) {
						alt24 = 2;
					}
					switch (alt24) {
						case 1:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:201:7: LPAR (fa= exp ( COMMA a= exp )* )? RPAR
						{
							match(input, LPAR, FOLLOW_LPAR_in_value1056);
							ArrayList<Node> argList = new ArrayList<Node>();
							// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:202:5: (fa= exp ( COMMA a= exp )* )?
							int alt20 = 2;
							int LA20_0 = input.LA(1);
							if ((LA20_0 == FALSE || (LA20_0 >= ID && LA20_0 <= IF) || LA20_0 == INTEGER || LA20_0 == LPAR || (LA20_0 >= NEW && LA20_0 <= NULL) || LA20_0 == PRINT || LA20_0 == TRUE)) {
								alt20 = 1;
							}
							switch (alt20) {
								case 1:
								// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:202:6: fa= exp ( COMMA a= exp )*
								{
									pushFollow(FOLLOW_exp_in_value1067);
									fa = exp();
									state._fsp--;
									
									argList.add(fa);
									// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:202:37: ( COMMA a= exp )*
									loop19: while (true) {
										int alt19 = 2;
										int LA19_0 = input.LA(1);
										if ((LA19_0 == COMMA)) {
											alt19 = 1;
										}
										
										switch (alt19) {
											case 1:
											// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:202:38: COMMA a= exp
											{
												match(input, COMMA, FOLLOW_COMMA_in_value1072);
												pushFollow(FOLLOW_exp_in_value1076);
												a = exp();
												state._fsp--;
												
												argList.add(a);
											}
												break;
											
											default:
												break loop19;
										}
									}
									
								}
									break;
							
							}
							
							ast = FOOLlib.factory.checkCall((i != null ? i.getText() : null), argList, entry, nestingLevel - j - 1);
							match(input, RPAR, FOLLOW_RPAR_in_value1096);
						}
							break;
						case 2:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:205:7: DOT identifier= ID ( LPAR (firstArgument= exp ( COMMA otherArgument= exp )* )? RPAR )?
						{
							match(input, DOT, FOLLOW_DOT_in_value1104);
							identifier = (Token) match(input, ID, FOLLOW_ID_in_value1108);
							ast = FOOLlib.factory.checkObjectField((i != null ? i.getLine() : 0), (i != null ? i.getText() : null), (identifier != null ? identifier.getText() : null), entry,
									nestingLevel - j - 1);
							// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:206:4: ( LPAR (firstArgument= exp ( COMMA otherArgument= exp )* )? RPAR )?
							int alt23 = 2;
							int LA23_0 = input.LA(1);
							if ((LA23_0 == LPAR)) {
								alt23 = 1;
							}
							switch (alt23) {
								case 1:
								// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:206:5: LPAR (firstArgument= exp ( COMMA otherArgument= exp )* )? RPAR
								{
									match(input, LPAR, FOLLOW_LPAR_in_value1116);
									ArrayList<Node> argList = new ArrayList<Node>();
									// stiamo chiamando il metodo di un field
									// class Integer(i: int) {
									// fun getInt: int() i;
									// }
									// class A(i: Integer) {
									// fun getInt: int() i.getInt();
									// }
									// Devo recuperare il riferimento a i (field)
									if (entry.isInClass())
										argList.add(Syskb.getClass(entry.getClassName()).getFieldNode((i != null ? i.getText() : null)));
									else
										argList.add(new IdNode((i != null ? i.getText() : null), entry, nestingLevel - j - 1));
									// Non sono dentro alla classe
									// class Integer(i: int) {
									// fun getInt: int() i;
									// }
									// print(i.getInt());
									// devo recuperare il riferimento a i
									
									// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:227:4: (firstArgument= exp ( COMMA otherArgument= exp )* )?
									int alt22 = 2;
									int LA22_0 = input.LA(1);
									if ((LA22_0 == FALSE || (LA22_0 >= ID && LA22_0 <= IF) || LA22_0 == INTEGER || LA22_0 == LPAR || (LA22_0 >= NEW && LA22_0 <= NULL) || LA22_0 == PRINT || LA22_0 == TRUE)) {
										alt22 = 1;
									}
									switch (alt22) {
										case 1:
										// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:227:5: firstArgument= exp ( COMMA otherArgument= exp )*
										{
											pushFollow(FOLLOW_exp_in_value1133);
											firstArgument = exp();
											state._fsp--;
											
											argList.add(firstArgument);
											// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:227:58: ( COMMA otherArgument= exp )*
											loop21: while (true) {
												int alt21 = 2;
												int LA21_0 = input.LA(1);
												if ((LA21_0 == COMMA)) {
													alt21 = 1;
												}
												
												switch (alt21) {
													case 1:
													// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:227:59: COMMA otherArgument= exp
													{
														match(input, COMMA, FOLLOW_COMMA_in_value1138);
														pushFollow(FOLLOW_exp_in_value1142);
														otherArgument = exp();
														state._fsp--;
														
														argList.add(otherArgument);
													}
														break;
													
													default:
														break loop21;
												}
											}
											
										}
											break;
									
									}
									
									// invoca il metodo della classe (classMethodNode)
									ast = ((ClassNode) entry.getType().typeCheck()).callMethod((identifier != null ? identifier.getText() : null), entry, argList, nestingLevel - j - 1);
									
									match(input, RPAR, FOLLOW_RPAR_in_value1159);
								}
									break;
							
							}
							
							if (ast == null) {
								FOOLlib.exit((i != null ? i.getText() : null) + "." + (identifier != null ? identifier.getText() : null) + " @ line " + (identifier != null ? identifier.getLine() : 0)
										+ ", " + (identifier != null ? identifier.getText() : null) + " not declared");
							}
							
						}
							break;
					
					}
					
				}
					break;
				case 8:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:237:5: NULL
				{
					match(input, NULL, FOLLOW_NULL_in_value1179);
					ast = new NullNode();
				}
					break;
				case 9:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:238:4: NEW className= ID LPAR (paramater= exp ( COMMA otherParameter= exp )* )? RPAR
				{
					ArrayList<Node> argList = new ArrayList<Node>();
					match(input, NEW, FOLLOW_NEW_in_value1190);
					className = (Token) match(input, ID, FOLLOW_ID_in_value1194);
					match(input, LPAR, FOLLOW_LPAR_in_value1200);
					// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:240:10: (paramater= exp ( COMMA otherParameter= exp )* )?
					int alt26 = 2;
					int LA26_0 = input.LA(1);
					if ((LA26_0 == FALSE || (LA26_0 >= ID && LA26_0 <= IF) || LA26_0 == INTEGER || LA26_0 == LPAR || (LA26_0 >= NEW && LA26_0 <= NULL) || LA26_0 == PRINT || LA26_0 == TRUE)) {
						alt26 = 1;
					}
					switch (alt26) {
						case 1:
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:240:11: paramater= exp ( COMMA otherParameter= exp )*
						{
							pushFollow(FOLLOW_exp_in_value1205);
							paramater = exp();
							state._fsp--;
							
							argList.add(paramater);
							// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:240:56: ( COMMA otherParameter= exp )*
							loop25: while (true) {
								int alt25 = 2;
								int LA25_0 = input.LA(1);
								if ((LA25_0 == COMMA)) {
									alt25 = 1;
								}
								
								switch (alt25) {
									case 1:
									// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:240:57: COMMA otherParameter= exp
									{
										match(input, COMMA, FOLLOW_COMMA_in_value1210);
										pushFollow(FOLLOW_exp_in_value1214);
										otherParameter = exp();
										state._fsp--;
										
										argList.add(otherParameter);
									}
										break;
									
									default:
										break loop25;
								}
							}
							
						}
							break;
					
					}
					
					ast = new ObjectNode((className != null ? className.getText() : null), argList);
					match(input, RPAR, FOLLOW_RPAR_in_value1233);
				}
					break;
				case 10:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:243:4: NOT LPAR e= exp RPAR
				{
					match(input, NOT, FOLLOW_NOT_in_value1238);
					match(input, LPAR, FOLLOW_LPAR_in_value1240);
					pushFollow(FOLLOW_exp_in_value1244);
					e = exp();
					state._fsp--;
					
					match(input, RPAR, FOLLOW_RPAR_in_value1246);
					ast = new NotNode(e);
				}
					break;
			
			}
		} catch (RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return ast;
	}
	
	// $ANTLR end "value"
	
	// $ANTLR start "type"
	// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:247:1: type returns [Node ast] : (typ= basic |arw= arrow );
	public final Node type() throws RecognitionException {
		Node ast = null;
		
		Node typ = null;
		Node arw = null;
		
		try {
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:248:3: (typ= basic |arw= arrow )
			int alt28 = 2;
			int LA28_0 = input.LA(1);
			if ((LA28_0 == BOOL || LA28_0 == ID || LA28_0 == INT)) {
				alt28 = 1;
			} else if ((LA28_0 == LPAR)) {
				alt28 = 2;
			}
			
			else {
				NoViableAltException nvae = new NoViableAltException("", 28, 0, input);
				throw nvae;
			}
			
			switch (alt28) {
				case 1:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:248:5: typ= basic
				{
					pushFollow(FOLLOW_basic_in_type1269);
					typ = basic();
					state._fsp--;
					
					ast = typ;
				}
					break;
				case 2:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:249:5: arw= arrow
				{
					pushFollow(FOLLOW_arrow_in_type1279);
					arw = arrow();
					state._fsp--;
					
					ast = arw;
				}
					break;
			
			}
		} catch (RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return ast;
	}
	
	// $ANTLR end "type"
	
	// $ANTLR start "basic"
	// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:253:1: basic returns [Node ast] : ( INT | BOOL |i= ID );
	public final Node basic() throws RecognitionException {
		Node ast = null;
		
		Token i = null;
		
		try {
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:254:3: ( INT | BOOL |i= ID )
			int alt29 = 3;
			switch (input.LA(1)) {
				case INT: {
					alt29 = 1;
				}
					break;
				case BOOL: {
					alt29 = 2;
				}
					break;
				case ID: {
					alt29 = 3;
				}
					break;
				default:
					NoViableAltException nvae = new NoViableAltException("", 29, 0, input);
					throw nvae;
			}
			switch (alt29) {
				case 1:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:254:5: INT
				{
					match(input, INT, FOLLOW_INT_in_basic1300);
					ast = new IntTypeNode();
				}
					break;
				case 2:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:255:5: BOOL
				{
					match(input, BOOL, FOLLOW_BOOL_in_basic1309);
					ast = new BoolTypeNode();
				}
					break;
				case 3:
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:256:5: i= ID
				{
					i = (Token) match(input, ID, FOLLOW_ID_in_basic1319);
					ast = new IdType((i != null ? i.getText() : null));
				}
					break;
			
			}
		} catch (RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return ast;
	}
	
	// $ANTLR end "basic"
	
	// $ANTLR start "arrow"
	// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:260:1: arrow returns [Node ast] : LPAR (fty= type ( COMMA ty= type )* )? RPAR ARROW returnType= basic ;
	public final Node arrow() throws RecognitionException {
		Node ast = null;
		
		Node fty = null;
		Node ty = null;
		Node returnType = null;
		
		try {
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:261:3: ( LPAR (fty= type ( COMMA ty= type )* )? RPAR ARROW returnType= basic )
			// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:261:5: LPAR (fty= type ( COMMA ty= type )* )? RPAR ARROW returnType= basic
			{
				match(input, LPAR, FOLLOW_LPAR_in_arrow1340);
				ArrayList<Node> parTypes = new ArrayList<Node>();
				// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:262:5: (fty= type ( COMMA ty= type )* )?
				int alt31 = 2;
				int LA31_0 = input.LA(1);
				if ((LA31_0 == BOOL || LA31_0 == ID || LA31_0 == INT || LA31_0 == LPAR)) {
					alt31 = 1;
				}
				switch (alt31) {
					case 1:
					// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:262:6: fty= type ( COMMA ty= type )*
					{
						pushFollow(FOLLOW_type_in_arrow1351);
						fty = type();
						state._fsp--;
						
						parTypes.add(fty);
						// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:262:43: ( COMMA ty= type )*
						loop30: while (true) {
							int alt30 = 2;
							int LA30_0 = input.LA(1);
							if ((LA30_0 == COMMA)) {
								alt30 = 1;
							}
							
							switch (alt30) {
								case 1:
								// C:\\Users\\teo\\Desktop\\FOOLtemp\\FOOL.g:262:44: COMMA ty= type
								{
									match(input, COMMA, FOLLOW_COMMA_in_arrow1356);
									pushFollow(FOLLOW_type_in_arrow1360);
									ty = type();
									state._fsp--;
									
									parTypes.add(ty);
								}
									break;
								
								default:
									break loop30;
							}
						}
						
					}
						break;
				
				}
				
				match(input, RPAR, FOLLOW_RPAR_in_arrow1371);
				match(input, ARROW, FOLLOW_ARROW_in_arrow1373);
				pushFollow(FOLLOW_basic_in_arrow1377);
				returnType = basic();
				state._fsp--;
				
				ast = new ArrowTypeNode(parTypes, returnType);
			}
			
		} catch (RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return ast;
	}
	
	// $ANTLR end "arrow"
	
	// Delegated rules
	
	public static final BitSet	FOLLOW_exp_in_prog38			= new BitSet(new long[] { 0x0000080000000000L });
	public static final BitSet	FOLLOW_SEMIC_in_prog40			= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_LET_in_prog48			= new BitSet(new long[] { 0x0000400008200100L });
	public static final BitSet	FOLLOW_cllist_in_prog62			= new BitSet(new long[] { 0x0000400008200000L });
	public static final BitSet	FOLLOW_declist_in_prog70		= new BitSet(new long[] { 0x0000000008000000L });
	public static final BitSet	FOLLOW_IN_in_prog72				= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_exp_in_prog76			= new BitSet(new long[] { 0x0000080000000000L });
	public static final BitSet	FOLLOW_SEMIC_in_prog78			= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_CLASS_in_cllist116		= new BitSet(new long[] { 0x0000000002000000L });
	public static final BitSet	FOLLOW_ID_in_cllist120			= new BitSet(new long[] { 0x0000000200080000L });
	public static final BitSet	FOLLOW_EXTENDS_in_cllist133		= new BitSet(new long[] { 0x0000000002000000L });
	public static final BitSet	FOLLOW_ID_in_cllist137			= new BitSet(new long[] { 0x0000000200000000L });
	public static final BitSet	FOLLOW_LPAR_in_cllist152		= new BitSet(new long[] { 0x0000040002000000L });
	public static final BitSet	FOLLOW_ID_in_cllist157			= new BitSet(new long[] { 0x0000000000000400L });
	public static final BitSet	FOLLOW_COLON_in_cllist159		= new BitSet(new long[] { 0x0000000012000080L });
	public static final BitSet	FOLLOW_basic_in_cllist163		= new BitSet(new long[] { 0x0000040000000800L });
	public static final BitSet	FOLLOW_COMMA_in_cllist172		= new BitSet(new long[] { 0x0000000002000000L });
	public static final BitSet	FOLLOW_ID_in_cllist176			= new BitSet(new long[] { 0x0000000000000400L });
	public static final BitSet	FOLLOW_COLON_in_cllist178		= new BitSet(new long[] { 0x0000000012000080L });
	public static final BitSet	FOLLOW_basic_in_cllist182		= new BitSet(new long[] { 0x0000040000000800L });
	public static final BitSet	FOLLOW_RPAR_in_cllist197		= new BitSet(new long[] { 0x0000000000000200L });
	public static final BitSet	FOLLOW_CLPAR_in_cllist202		= new BitSet(new long[] { 0x0000000000202000L });
	public static final BitSet	FOLLOW_FUN_in_cllist219			= new BitSet(new long[] { 0x0000000002000000L });
	public static final BitSet	FOLLOW_ID_in_cllist223			= new BitSet(new long[] { 0x0000000000000400L });
	public static final BitSet	FOLLOW_COLON_in_cllist225		= new BitSet(new long[] { 0x0000000012000080L });
	public static final BitSet	FOLLOW_basic_in_cllist229		= new BitSet(new long[] { 0x0000000200000000L });
	public static final BitSet	FOLLOW_LPAR_in_cllist243		= new BitSet(new long[] { 0x0000040002000000L });
	public static final BitSet	FOLLOW_ID_in_cllist253			= new BitSet(new long[] { 0x0000000000000400L });
	public static final BitSet	FOLLOW_COLON_in_cllist255		= new BitSet(new long[] { 0x0000000212000080L });
	public static final BitSet	FOLLOW_type_in_cllist259		= new BitSet(new long[] { 0x0000040000000800L });
	public static final BitSet	FOLLOW_COMMA_in_cllist275		= new BitSet(new long[] { 0x0000000002000000L });
	public static final BitSet	FOLLOW_ID_in_cllist279			= new BitSet(new long[] { 0x0000000000000400L });
	public static final BitSet	FOLLOW_COLON_in_cllist281		= new BitSet(new long[] { 0x0000000212000080L });
	public static final BitSet	FOLLOW_type_in_cllist285		= new BitSet(new long[] { 0x0000040000000800L });
	public static final BitSet	FOLLOW_RPAR_in_cllist303		= new BitSet(new long[] { 0x0000227326100000L });
	public static final BitSet	FOLLOW_LET_in_cllist311			= new BitSet(new long[] { 0x0000400008000000L });
	public static final BitSet	FOLLOW_VAR_in_cllist327			= new BitSet(new long[] { 0x0000000002000000L });
	public static final BitSet	FOLLOW_ID_in_cllist331			= new BitSet(new long[] { 0x0000000000000400L });
	public static final BitSet	FOLLOW_COLON_in_cllist333		= new BitSet(new long[] { 0x0000000012000080L });
	public static final BitSet	FOLLOW_basic_in_cllist337		= new BitSet(new long[] { 0x0000000000000040L });
	public static final BitSet	FOLLOW_ASS_in_cllist339			= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_exp_in_cllist343			= new BitSet(new long[] { 0x0000080000000000L });
	public static final BitSet	FOLLOW_SEMIC_in_cllist345		= new BitSet(new long[] { 0x0000400008000000L });
	public static final BitSet	FOLLOW_IN_in_cllist369			= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_exp_in_cllist376			= new BitSet(new long[] { 0x0000080000000000L });
	public static final BitSet	FOLLOW_SEMIC_in_cllist378		= new BitSet(new long[] { 0x0000000000202000L });
	public static final BitSet	FOLLOW_CRPAR_in_cllist397		= new BitSet(new long[] { 0x0000000000000102L });
	public static final BitSet	FOLLOW_VAR_in_declist430		= new BitSet(new long[] { 0x0000000002000000L });
	public static final BitSet	FOLLOW_ID_in_declist434			= new BitSet(new long[] { 0x0000000000000400L });
	public static final BitSet	FOLLOW_COLON_in_declist436		= new BitSet(new long[] { 0x0000000212000080L });
	public static final BitSet	FOLLOW_type_in_declist440		= new BitSet(new long[] { 0x0000000000000040L });
	public static final BitSet	FOLLOW_ASS_in_declist442		= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_exp_in_declist446		= new BitSet(new long[] { 0x0000080000000000L });
	public static final BitSet	FOLLOW_SEMIC_in_declist448		= new BitSet(new long[] { 0x0000400000200002L });
	public static final BitSet	FOLLOW_FUN_in_declist466		= new BitSet(new long[] { 0x0000000002000000L });
	public static final BitSet	FOLLOW_ID_in_declist470			= new BitSet(new long[] { 0x0000000000000400L });
	public static final BitSet	FOLLOW_COLON_in_declist472		= new BitSet(new long[] { 0x0000000212000080L });
	public static final BitSet	FOLLOW_type_in_declist476		= new BitSet(new long[] { 0x0000000200000000L });
	public static final BitSet	FOLLOW_LPAR_in_declist496		= new BitSet(new long[] { 0x0000040002000000L });
	public static final BitSet	FOLLOW_ID_in_declist520			= new BitSet(new long[] { 0x0000000000000400L });
	public static final BitSet	FOLLOW_COLON_in_declist522		= new BitSet(new long[] { 0x0000000212000080L });
	public static final BitSet	FOLLOW_type_in_declist526		= new BitSet(new long[] { 0x0000040000000800L });
	public static final BitSet	FOLLOW_COMMA_in_declist552		= new BitSet(new long[] { 0x0000000002000000L });
	public static final BitSet	FOLLOW_ID_in_declist556			= new BitSet(new long[] { 0x0000000000000400L });
	public static final BitSet	FOLLOW_COLON_in_declist558		= new BitSet(new long[] { 0x0000000212000080L });
	public static final BitSet	FOLLOW_type_in_declist562		= new BitSet(new long[] { 0x0000040000000800L });
	public static final BitSet	FOLLOW_RPAR_in_declist611		= new BitSet(new long[] { 0x0000227326100000L });
	public static final BitSet	FOLLOW_LET_in_declist625		= new BitSet(new long[] { 0x0000400008200000L });
	public static final BitSet	FOLLOW_declist_in_declist629	= new BitSet(new long[] { 0x0000000008000000L });
	public static final BitSet	FOLLOW_IN_in_declist631			= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_exp_in_declist639		= new BitSet(new long[] { 0x0000080000000000L });
	public static final BitSet	FOLLOW_SEMIC_in_declist641		= new BitSet(new long[] { 0x0000400000200002L });
	public static final BitSet	FOLLOW_term_in_exp677			= new BitSet(new long[] { 0x0000018400000002L });
	public static final BitSet	FOLLOW_PLUS_in_exp689			= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_term_in_exp694			= new BitSet(new long[] { 0x0000018400000002L });
	public static final BitSet	FOLLOW_MINUS_in_exp706			= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_term_in_exp710			= new BitSet(new long[] { 0x0000018400000002L });
	public static final BitSet	FOLLOW_OR_in_exp722				= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_term_in_exp729			= new BitSet(new long[] { 0x0000018400000002L });
	public static final BitSet	FOLLOW_factor_in_term758		= new BitSet(new long[] { 0x0000000800004012L });
	public static final BitSet	FOLLOW_MULT_in_term769			= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_factor_in_term773		= new BitSet(new long[] { 0x0000000800004012L });
	public static final BitSet	FOLLOW_DIV_in_term785			= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_factor_in_term790		= new BitSet(new long[] { 0x0000000800004012L });
	public static final BitSet	FOLLOW_AND_in_term801			= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_factor_in_term806		= new BitSet(new long[] { 0x0000000800004012L });
	public static final BitSet	FOLLOW_value_in_factor833		= new BitSet(new long[] { 0x00000000C0C20002L });
	public static final BitSet	FOLLOW_EQ_in_factor844			= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_value_in_factor848		= new BitSet(new long[] { 0x00000000C0C20002L });
	public static final BitSet	FOLLOW_GR_in_factor859			= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_value_in_factor863		= new BitSet(new long[] { 0x00000000C0C20002L });
	public static final BitSet	FOLLOW_LE_in_factor874			= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_value_in_factor878		= new BitSet(new long[] { 0x00000000C0C20002L });
	public static final BitSet	FOLLOW_GREAT_in_factor889		= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_value_in_factor893		= new BitSet(new long[] { 0x00000000C0C20002L });
	public static final BitSet	FOLLOW_LESS_in_factor904		= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_value_in_factor908		= new BitSet(new long[] { 0x00000000C0C20002L });
	public static final BitSet	FOLLOW_INTEGER_in_value937		= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_TRUE_in_value947			= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_FALSE_in_value957		= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_LPAR_in_value967			= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_exp_in_value973			= new BitSet(new long[] { 0x0000040000000000L });
	public static final BitSet	FOLLOW_RPAR_in_value975			= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_IF_in_value985			= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_exp_in_value989			= new BitSet(new long[] { 0x0000100000000000L });
	public static final BitSet	FOLLOW_THEN_in_value991			= new BitSet(new long[] { 0x0000000000000200L });
	public static final BitSet	FOLLOW_CLPAR_in_value993		= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_exp_in_value997			= new BitSet(new long[] { 0x0000000000002000L });
	public static final BitSet	FOLLOW_CRPAR_in_value999		= new BitSet(new long[] { 0x0000000000010002L });
	public static final BitSet	FOLLOW_ELSE_in_value1004		= new BitSet(new long[] { 0x0000000000000200L });
	public static final BitSet	FOLLOW_CLPAR_in_value1006		= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_exp_in_value1010			= new BitSet(new long[] { 0x0000000000002000L });
	public static final BitSet	FOLLOW_CRPAR_in_value1012		= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_PRINT_in_value1024		= new BitSet(new long[] { 0x0000000200000000L });
	public static final BitSet	FOLLOW_LPAR_in_value1026		= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_exp_in_value1030			= new BitSet(new long[] { 0x0000040000000000L });
	public static final BitSet	FOLLOW_RPAR_in_value1032		= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_ID_in_value1044			= new BitSet(new long[] { 0x0000000200008002L });
	public static final BitSet	FOLLOW_LPAR_in_value1056		= new BitSet(new long[] { 0x0000267226100000L });
	public static final BitSet	FOLLOW_exp_in_value1067			= new BitSet(new long[] { 0x0000040000000800L });
	public static final BitSet	FOLLOW_COMMA_in_value1072		= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_exp_in_value1076			= new BitSet(new long[] { 0x0000040000000800L });
	public static final BitSet	FOLLOW_RPAR_in_value1096		= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_DOT_in_value1104			= new BitSet(new long[] { 0x0000000002000000L });
	public static final BitSet	FOLLOW_ID_in_value1108			= new BitSet(new long[] { 0x0000000200000002L });
	public static final BitSet	FOLLOW_LPAR_in_value1116		= new BitSet(new long[] { 0x0000267226100000L });
	public static final BitSet	FOLLOW_exp_in_value1133			= new BitSet(new long[] { 0x0000040000000800L });
	public static final BitSet	FOLLOW_COMMA_in_value1138		= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_exp_in_value1142			= new BitSet(new long[] { 0x0000040000000800L });
	public static final BitSet	FOLLOW_RPAR_in_value1159		= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_NULL_in_value1179		= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_NEW_in_value1190			= new BitSet(new long[] { 0x0000000002000000L });
	public static final BitSet	FOLLOW_ID_in_value1194			= new BitSet(new long[] { 0x0000000200000000L });
	public static final BitSet	FOLLOW_LPAR_in_value1200		= new BitSet(new long[] { 0x0000267226100000L });
	public static final BitSet	FOLLOW_exp_in_value1205			= new BitSet(new long[] { 0x0000040000000800L });
	public static final BitSet	FOLLOW_COMMA_in_value1210		= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_exp_in_value1214			= new BitSet(new long[] { 0x0000040000000800L });
	public static final BitSet	FOLLOW_RPAR_in_value1233		= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_NOT_in_value1238			= new BitSet(new long[] { 0x0000000200000000L });
	public static final BitSet	FOLLOW_LPAR_in_value1240		= new BitSet(new long[] { 0x0000227226100000L });
	public static final BitSet	FOLLOW_exp_in_value1244			= new BitSet(new long[] { 0x0000040000000000L });
	public static final BitSet	FOLLOW_RPAR_in_value1246		= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_basic_in_type1269		= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_arrow_in_type1279		= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_INT_in_basic1300			= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_BOOL_in_basic1309		= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_ID_in_basic1319			= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet	FOLLOW_LPAR_in_arrow1340		= new BitSet(new long[] { 0x0000040212000080L });
	public static final BitSet	FOLLOW_type_in_arrow1351		= new BitSet(new long[] { 0x0000040000000800L });
	public static final BitSet	FOLLOW_COMMA_in_arrow1356		= new BitSet(new long[] { 0x0000000212000080L });
	public static final BitSet	FOLLOW_type_in_arrow1360		= new BitSet(new long[] { 0x0000040000000800L });
	public static final BitSet	FOLLOW_RPAR_in_arrow1371		= new BitSet(new long[] { 0x0000000000000020L });
	public static final BitSet	FOLLOW_ARROW_in_arrow1373		= new BitSet(new long[] { 0x0000000012000080L });
	public static final BitSet	FOLLOW_basic_in_arrow1377		= new BitSet(new long[] { 0x0000000000000002L });
}
