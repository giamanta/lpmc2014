package it.unibo.lpemc.implementation.virtualmachine;

// $ANTLR 3.2 Sep 23, 2009 12:02:23 S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g 2014-12-12 15:35:29

import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.DFA;
import org.antlr.runtime.EarlyExitException;
import org.antlr.runtime.IntStream;
import org.antlr.runtime.Lexer;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.RecognizerSharedState;

public class SVMLexer extends Lexer {
	class DFA6 extends DFA {
		
		public DFA6(final BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 6;
			this.eot = SVMLexer.DFA6_eot;
			this.eof = SVMLexer.DFA6_eof;
			this.min = SVMLexer.DFA6_min;
			this.max = SVMLexer.DFA6_max;
			this.accept = SVMLexer.DFA6_accept;
			this.special = SVMLexer.DFA6_special;
			this.transition = SVMLexer.DFA6_transition;
		}
		
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( PUSH | POP | ADD | SUB | MULT | DIV | STOREW | LOADW | BRANCH | BRANCHEQ | BRANCHLESS | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT | COL | LABEL | NUMBER | WHITESP | ERR );";
		}
		
		@Override
		public int specialStateTransition(int s, final IntStream _input) throws NoViableAltException {
			final IntStream input = _input;
			final int _s = s;
			switch (s) {
				case 0:
					final int LA6_0 = input.LA(1);
					
					s = -1;
					if ((LA6_0 == 'p')) {
						s = 1;
					}
					
					else if ((LA6_0 == 'a')) {
						s = 2;
					}
					
					else if ((LA6_0 == 's')) {
						s = 3;
					}
					
					else if ((LA6_0 == 'm')) {
						s = 4;
					}
					
					else if ((LA6_0 == 'd')) {
						s = 5;
					}
					
					else if ((LA6_0 == 'l')) {
						s = 6;
					}
					
					else if ((LA6_0 == 'b')) {
						s = 7;
					}
					
					else if ((LA6_0 == 'j')) {
						s = 8;
					}
					
					else if ((LA6_0 == 'c')) {
						s = 9;
					}
					
					else if ((LA6_0 == 'h')) {
						s = 10;
					}
					
					else if ((LA6_0 == ':')) {
						s = 11;
					}
					
					else if ((((LA6_0 >= 'A') && (LA6_0 <= 'Z')) || ((LA6_0 >= 'e') && (LA6_0 <= 'g')) || (LA6_0 == 'i') || (LA6_0 == 'k') || ((LA6_0 >= 'n') && (LA6_0 <= 'o'))
							|| ((LA6_0 >= 'q') && (LA6_0 <= 'r')) || ((LA6_0 >= 't') && (LA6_0 <= 'z')))) {
						s = 12;
					}
					
					else if ((LA6_0 == '0')) {
						s = 13;
					}
					
					else if ((LA6_0 == '-')) {
						s = 14;
					}
					
					else if ((((LA6_0 >= '1') && (LA6_0 <= '9')))) {
						s = 15;
					}
					
					else if ((((LA6_0 >= '\t') && (LA6_0 <= '\n')) || (LA6_0 == '\r') || (LA6_0 == ' '))) {
						s = 16;
					}
					
					else if ((((LA6_0 >= '\u0000') && (LA6_0 <= '\b')) || ((LA6_0 >= '\u000B') && (LA6_0 <= '\f')) || ((LA6_0 >= '\u000E') && (LA6_0 <= '\u001F'))
							|| ((LA6_0 >= '!') && (LA6_0 <= ',')) || ((LA6_0 >= '.') && (LA6_0 <= '/')) || ((LA6_0 >= ';') && (LA6_0 <= '@')) || ((LA6_0 >= '[') && (LA6_0 <= '`')) || ((LA6_0 >= '{') && (LA6_0 <= '\uFFFF')))) {
						s = 17;
					}
					
					if (s >= 0) {
						return s;
					}
					break;
			}
			final NoViableAltException nvae = new NoViableAltException(this.getDescription(), 6, _s, input);
			this.error(nvae);
			throw nvae;
		}
	}
	
	public static final int	ADD					= 8;
	public static final int	BRANCH				= 15;
	public static final int	BRANCHEQ			= 16;
	public static final int	BRANCHLESS			= 17;
	public static final int	COL					= 14;
	public static final int	COPYFP				= 25;
	static final short[]	DFA6_accept			= DFA.unpackEncodedString(SVMLexer.DFA6_acceptS);
	static final String		DFA6_acceptS		= "\13\uffff\1\30\1\31\1\32\1\uffff\1\32\1\33\1\34\3\uffff\1\31\16" + "\uffff\1\11\3\uffff\1\30\1\32\1\33\5\uffff\1\7\6\uffff\1\10\6\uffff"
			+ "\1\14\3\uffff\1\2\1\uffff\1\3\1\4\1\16\1\20\1\22\1\25\1\uffff\1" + "\6\1\15\1\17\1\21\1\24\1\12\1\uffff\1\23\1\uffff\1\1\1\uffff\1\5"
			+ "\1\uffff\1\27\1\26\1\13";
	static final short[]	DFA6_eof			= DFA.unpackEncodedString(SVMLexer.DFA6_eofS);
	static final String		DFA6_eofS			= "\133\uffff";
	static final short[]	DFA6_eot			= DFA.unpackEncodedString(SVMLexer.DFA6_eotS);
	static final String		DFA6_eotS			= "\1\uffff\6\25\1\44\3\25\3\uffff\1\21\3\uffff\3\25\1\uffff\2\25" + "\1\60\5\25\1\67\5\25\1\uffff\1\76\2\25\3\uffff\1\25\1\102\1\25\1"
			+ "\104\1\105\1\uffff\1\106\1\107\1\110\1\111\1\25\1\113\1\uffff\1" + "\114\1\115\1\116\1\117\1\120\1\25\1\uffff\1\122\1\25\1\124\1\uffff"
			+ "\1\25\6\uffff\1\126\6\uffff\1\25\1\uffff\1\130\1\uffff\1\131\1\uffff" + "\1\132\3\uffff";
	static final char[]		DFA6_max			= DFA.unpackEncodedStringToUnsignedChars(SVMLexer.DFA6_maxS);
	static final String		DFA6_maxS			= "\1\uffff\1\165\1\144\1\167\1\165\1\151\1\167\1\172\1\163\1\146" + "\1\141\3\uffff\1\71\3\uffff\1\163\1\160\1\151\1\uffff\1\144\1\142"
			+ "\1\172\1\166\2\160\1\154\1\166\1\172\1\166\2\160\1\161\1\145\1\uffff" + "\1\172\1\160\1\154\3\uffff\1\150\1\172\1\156\2\172\1\uffff\4\172"
			+ "\1\164\1\172\1\uffff\5\172\1\163\1\uffff\1\172\1\164\1\172\1\uffff" + "\1\164\6\uffff\1\172\6\uffff\1\163\1\uffff\1\172\1\uffff\1\172\1"
			+ "\uffff\1\172\3\uffff";
	static final char[]		DFA6_min			= DFA.unpackEncodedStringToUnsignedChars(SVMLexer.DFA6_minS);
	static final String		DFA6_minS			= "\1\0\1\157\1\144\1\146\1\165\1\151\1\146\1\60\1\163\1\146\1\141" + "\3\uffff\1\61\3\uffff\1\163\1\160\1\151\1\uffff\1\144\1\142\1\60"
			+ "\1\141\2\160\1\154\1\166\1\60\1\141\2\160\1\161\1\145\1\uffff\1" + "\60\1\160\1\154\3\uffff\1\150\1\60\1\156\2\60\1\uffff\4\60\1\164"
			+ "\1\60\1\uffff\5\60\1\163\1\uffff\1\60\1\164\1\60\1\uffff\1\164\6" + "\uffff\1\60\6\uffff\1\163\1\uffff\1\60\1\uffff\1\60\1\uffff\1\60"
			+ "\3\uffff";
	static final short[]	DFA6_special		= DFA.unpackEncodedString(SVMLexer.DFA6_specialS);
	static final String		DFA6_specialS		= "\1\0\132\uffff}>";
	static final short[][]	DFA6_transition;
	static final String[]	DFA6_transitionS	= {
			"\11\21\2\20\2\21\1\20\22\21\1\20\14\21\1\16\2\21\1\15\11\17" + "\1\13\6\21\32\14\6\21\1\2\1\7\1\11\1\5\3\14\1\12\1\14\1\10\1" + "\14\1\6\1\4\2\14\1\1\2\14\1\3\7\14\uff85\21",
			"\1\23\2\uffff\1\24\2\uffff\1\22", "\1\26", "\1\32\1\uffff\1\33\11\uffff\1\31\2\uffff\1\27\1\uffff\1\30", "\1\34", "\1\35", "\1\40\1\uffff\1\41\11\uffff\1\37\4\uffff\1\36",
			"\12\25\7\uffff\32\25\6\uffff\4\25\1\42\6\25\1\43\16\25", "\1\45", "\1\46", "\1\47", "", "", "", "\11\51", "", "", "", "\1\53", "\1\54", "\1\55", "", "\1\56", "\1\57",
			"\12\25\7\uffff\32\25\6\uffff\32\25", "\1\61\24\uffff\1\62", "\1\63", "\1\64", "\1\65", "\1\66", "\12\25\7\uffff\32\25\6\uffff\32\25", "\1\70\24\uffff\1\71", "\1\72", "\1\73", "\1\74",
			"\1\75", "", "\12\25\7\uffff\32\25\6\uffff\32\25", "\1\77", "\1\100", "", "", "", "\1\101", "\12\25\7\uffff\32\25\6\uffff\32\25", "\1\103", "\12\25\7\uffff\32\25\6\uffff\32\25",
			"\12\25\7\uffff\32\25\6\uffff\32\25", "", "\12\25\7\uffff\32\25\6\uffff\32\25", "\12\25\7\uffff\32\25\6\uffff\32\25", "\12\25\7\uffff\32\25\6\uffff\32\25",
			"\12\25\7\uffff\32\25\6\uffff\32\25", "\1\112", "\12\25\7\uffff\32\25\6\uffff\32\25", "", "\12\25\7\uffff\32\25\6\uffff\32\25", "\12\25\7\uffff\32\25\6\uffff\32\25",
			"\12\25\7\uffff\32\25\6\uffff\32\25", "\12\25\7\uffff\32\25\6\uffff\32\25", "\12\25\7\uffff\32\25\6\uffff\32\25", "\1\121", "", "\12\25\7\uffff\32\25\6\uffff\32\25", "\1\123",
			"\12\25\7\uffff\32\25\6\uffff\32\25", "", "\1\125", "", "", "", "", "", "", "\12\25\7\uffff\32\25\6\uffff\32\25", "", "", "", "", "", "", "\1\127", "",
			"\12\25\7\uffff\32\25\6\uffff\32\25", "", "\12\25\7\uffff\32\25\6\uffff\32\25", "", "\12\25\7\uffff\32\25\6\uffff\32\25", "", "", "" };
	public static final int	DIV					= 11;
	public static final int	EOF					= -1;
	public static final int	ERR					= 31;
	public static final int	HALT				= 29;
	public static final int	JS					= 18;
	public static final int	LABEL				= 6;
	public static final int	LOADFP				= 23;
	public static final int	LOADHP				= 26;
	
	// delegates
	// delegators
	
	public static final int	LOADRA				= 19;
	public static final int	LOADRV				= 21;
	public static final int	LOADW				= 13;
	public static final int	MULT				= 10;
	
	public static final int	NUMBER				= 5;
	
	public static final int	POP					= 7;
	
	public static final int	PRINT				= 28;
	
	public static final int	PUSH				= 4;
	
	public static final int	STOREFP				= 24;
	
	public static final int	STOREHP				= 27;
	
	public static final int	STORERA				= 20;
	
	public static final int	STORERV				= 22;
	
	public static final int	STOREW				= 12;
	
	public static final int	SUB					= 9;
	
	public static final int	WHITESP				= 30;
	
	static {
		final int numStates = SVMLexer.DFA6_transitionS.length;
		DFA6_transition = new short[numStates][];
		for (int i = 0; i < numStates; i++) {
			SVMLexer.DFA6_transition[i] = DFA.unpackEncodedString(SVMLexer.DFA6_transitionS[i]);
		}
	}
	
	protected DFA6			dfa6				= new DFA6(this);
	
	public SVMLexer() {
		;
	}
	
	public SVMLexer(final CharStream input) {
		this(input, new RecognizerSharedState());
	}
	
	public SVMLexer(final CharStream input, final RecognizerSharedState state) {
		super(input, state);
		
	}
	
	@Override
	public String getGrammarFileName() {
		return "S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g";
	}
	
	// $ANTLR start "ADD"
	public final void mADD() throws RecognitionException {
		try {
			final int _type = SVMLexer.ADD;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:79:6: ( 'add' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:79:8: 'add'
			{
				this.match("add");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "ADD"
	
	// $ANTLR start "BRANCH"
	public final void mBRANCH() throws RecognitionException {
		try {
			final int _type = SVMLexer.BRANCH;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:85:9: ( 'b' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:85:11: 'b'
			{
				this.match('b');
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "BRANCH"
	
	// $ANTLR start "BRANCHEQ"
	public final void mBRANCHEQ() throws RecognitionException {
		try {
			final int _type = SVMLexer.BRANCHEQ;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:86:10: ( 'beq' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:86:12: 'beq'
			{
				this.match("beq");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "BRANCHEQ"
	
	// $ANTLR start "BRANCHLESS"
	public final void mBRANCHLESS() throws RecognitionException {
		try {
			final int _type = SVMLexer.BRANCHLESS;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:87:11: ( 'bless' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:87:12: 'bless'
			{
				this.match("bless");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "BRANCHLESS"
	
	// $ANTLR start "COL"
	public final void mCOL() throws RecognitionException {
		try {
			final int _type = SVMLexer.COL;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:101:6: ( ':' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:101:8: ':'
			{
				this.match(':');
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "COL"
	
	// $ANTLR start "COPYFP"
	public final void mCOPYFP() throws RecognitionException {
		try {
			final int _type = SVMLexer.COPYFP;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:95:10: ( 'cfp' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:95:12: 'cfp'
			{
				this.match("cfp");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "COPYFP"
	
	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			final int _type = SVMLexer.DIV;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:82:6: ( 'div' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:82:8: 'div'
			{
				this.match("div");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "DIV"
	
	// $ANTLR start "ERR"
	public final void mERR() throws RecognitionException {
		try {
			final int _type = SVMLexer.ERR;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:107:9: ( . )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:107:11: .
			{
				this.matchAny();
				System.out.println("Invalid char: " + this.getText());
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "ERR"
	
	// $ANTLR start "HALT"
	public final void mHALT() throws RecognitionException {
		try {
			final int _type = SVMLexer.HALT;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:99:7: ( 'halt' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:99:9: 'halt'
			{
				this.match("halt");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "HALT"
	
	// $ANTLR start "JS"
	public final void mJS() throws RecognitionException {
		try {
			final int _type = SVMLexer.JS;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:88:5: ( 'js' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:88:7: 'js'
			{
				this.match("js");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "JS"
	
	// $ANTLR start "LABEL"
	public final void mLABEL() throws RecognitionException {
		try {
			final int _type = SVMLexer.LABEL;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:102:8: ( ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )* )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:102:10: ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			{
				if (((this.input.LA(1) >= 'A') && (this.input.LA(1) <= 'Z')) || ((this.input.LA(1) >= 'a') && (this.input.LA(1) <= 'z'))) {
					this.input.consume();
					
				} else {
					final MismatchedSetException mse = new MismatchedSetException(null, this.input);
					this.recover(mse);
					throw mse;
				}
				
				// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:102:29: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
				loop1: do {
					int alt1 = 2;
					final int LA1_0 = this.input.LA(1);
					
					if ((((LA1_0 >= '0') && (LA1_0 <= '9')) || ((LA1_0 >= 'A') && (LA1_0 <= 'Z')) || ((LA1_0 >= 'a') && (LA1_0 <= 'z')))) {
						alt1 = 1;
					}
					
					switch (alt1) {
						case 1:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:
						{
							if (((this.input.LA(1) >= '0') && (this.input.LA(1) <= '9')) || ((this.input.LA(1) >= 'A') && (this.input.LA(1) <= 'Z'))
									|| ((this.input.LA(1) >= 'a') && (this.input.LA(1) <= 'z'))) {
								this.input.consume();
								
							} else {
								final MismatchedSetException mse = new MismatchedSetException(null, this.input);
								this.recover(mse);
								throw mse;
							}
							
						}
							break;
						
						default:
							break loop1;
					}
				} while (true);
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "LABEL"
	
	// $ANTLR start "LOADFP"
	public final void mLOADFP() throws RecognitionException {
		try {
			final int _type = SVMLexer.LOADFP;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:93:9: ( 'lfp' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:93:11: 'lfp'
			{
				this.match("lfp");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "LOADFP"
	
	// $ANTLR start "LOADHP"
	public final void mLOADHP() throws RecognitionException {
		try {
			final int _type = SVMLexer.LOADHP;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:96:9: ( 'lhp' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:96:11: 'lhp'
			{
				this.match("lhp");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "LOADHP"
	// $ANTLR start "LOADRA"
	public final void mLOADRA() throws RecognitionException {
		try {
			final int _type = SVMLexer.LOADRA;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:89:9: ( 'lra' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:89:11: 'lra'
			{
				this.match("lra");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "LOADRA"
	// $ANTLR start "LOADRV"
	public final void mLOADRV() throws RecognitionException {
		try {
			final int _type = SVMLexer.LOADRV;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:91:9: ( 'lrv' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:91:11: 'lrv'
			{
				this.match("lrv");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "LOADRV"
	// $ANTLR start "LOADW"
	public final void mLOADW() throws RecognitionException {
		try {
			final int _type = SVMLexer.LOADW;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:84:8: ( 'lw' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:84:10: 'lw'
			{
				this.match("lw");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "LOADW"
	// $ANTLR start "MULT"
	public final void mMULT() throws RecognitionException {
		try {
			final int _type = SVMLexer.MULT;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:81:7: ( 'mult' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:81:9: 'mult'
			{
				this.match("mult");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "MULT"
	// $ANTLR start "NUMBER"
	public final void mNUMBER() throws RecognitionException {
		try {
			final int _type = SVMLexer.NUMBER;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:103:9: ( '0' | ( '-' )? ( ( '1' .. '9' ) ( '0' .. '9' )* ) )
			int alt4 = 2;
			final int LA4_0 = this.input.LA(1);
			
			if ((LA4_0 == '0')) {
				alt4 = 1;
			} else if (((LA4_0 == '-') || ((LA4_0 >= '1') && (LA4_0 <= '9')))) {
				alt4 = 2;
			} else {
				final NoViableAltException nvae = new NoViableAltException("", 4, 0, this.input);
				
				throw nvae;
			}
			switch (alt4) {
				case 1:
				// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:103:11: '0'
				{
					this.match('0');
					
				}
					break;
				case 2:
				// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:103:17: ( '-' )? ( ( '1' .. '9' ) ( '0' .. '9' )* )
				{
					// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:103:17: ( '-' )?
					int alt2 = 2;
					final int LA2_0 = this.input.LA(1);
					
					if ((LA2_0 == '-')) {
						alt2 = 1;
					}
					switch (alt2) {
						case 1:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:103:18: '-'
						{
							this.match('-');
							
						}
							break;
					
					}
					
					// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:103:23: ( ( '1' .. '9' ) ( '0' .. '9' )* )
					// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:103:24: ( '1' .. '9' ) ( '0' .. '9' )*
					{
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:103:24: ( '1' .. '9' )
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:103:25: '1' .. '9'
						{
							this.matchRange('1', '9');
							
						}
						
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:103:34: ( '0' .. '9' )*
						loop3: do {
							int alt3 = 2;
							final int LA3_0 = this.input.LA(1);
							
							if ((((LA3_0 >= '0') && (LA3_0 <= '9')))) {
								alt3 = 1;
							}
							
							switch (alt3) {
								case 1:
								// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:103:35: '0' .. '9'
								{
									this.matchRange('0', '9');
									
								}
									break;
								
								default:
									break loop3;
							}
						} while (true);
						
					}
					
				}
					break;
			
			}
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "NUMBER"
	// $ANTLR start "POP"
	public final void mPOP() throws RecognitionException {
		try {
			final int _type = SVMLexer.POP;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:78:6: ( 'pop' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:78:8: 'pop'
			{
				this.match("pop");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "POP"
	// $ANTLR start "PRINT"
	public final void mPRINT() throws RecognitionException {
		try {
			final int _type = SVMLexer.PRINT;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:98:8: ( 'print' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:98:10: 'print'
			{
				this.match("print");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "PRINT"
	
	// $ANTLR start "PUSH"
	public final void mPUSH() throws RecognitionException {
		try {
			final int _type = SVMLexer.PUSH;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:77:9: ( 'push' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:77:11: 'push'
			{
				this.match("push");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "PUSH"
	// $ANTLR start "STOREFP"
	public final void mSTOREFP() throws RecognitionException {
		try {
			final int _type = SVMLexer.STOREFP;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:94:10: ( 'sfp' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:94:12: 'sfp'
			{
				this.match("sfp");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "STOREFP"
	// $ANTLR start "STOREHP"
	public final void mSTOREHP() throws RecognitionException {
		try {
			final int _type = SVMLexer.STOREHP;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:97:10: ( 'shp' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:97:12: 'shp'
			{
				this.match("shp");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "STOREHP"
	// $ANTLR start "STORERA"
	public final void mSTORERA() throws RecognitionException {
		try {
			final int _type = SVMLexer.STORERA;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:90:10: ( 'sra' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:90:12: 'sra'
			{
				this.match("sra");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "STORERA"
	// $ANTLR start "STORERV"
	public final void mSTORERV() throws RecognitionException {
		try {
			final int _type = SVMLexer.STORERV;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:92:10: ( 'srv' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:92:12: 'srv'
			{
				this.match("srv");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "STORERV"
	// $ANTLR start "STOREW"
	public final void mSTOREW() throws RecognitionException {
		try {
			final int _type = SVMLexer.STOREW;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:83:9: ( 'sw' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:83:11: 'sw'
			{
				this.match("sw");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "STOREW"
	// $ANTLR start "SUB"
	public final void mSUB() throws RecognitionException {
		try {
			final int _type = SVMLexer.SUB;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:80:6: ( 'sub' )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:80:8: 'sub'
			{
				this.match("sub");
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	
	// $ANTLR end "SUB"
	
	@Override
	public void mTokens() throws RecognitionException {
		// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:8: ( PUSH | POP | ADD | SUB | MULT | DIV | STOREW | LOADW | BRANCH | BRANCHEQ | BRANCHLESS | JS | LOADRA | STORERA | LOADRV
		// | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT | COL | LABEL | NUMBER | WHITESP | ERR )
		int alt6 = 28;
		alt6 = this.dfa6.predict(this.input);
		switch (alt6) {
			case 1:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:10: PUSH
			{
				this.mPUSH();
				
			}
				break;
			case 2:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:15: POP
			{
				this.mPOP();
				
			}
				break;
			case 3:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:19: ADD
			{
				this.mADD();
				
			}
				break;
			case 4:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:23: SUB
			{
				this.mSUB();
				
			}
				break;
			case 5:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:27: MULT
			{
				this.mMULT();
				
			}
				break;
			case 6:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:32: DIV
			{
				this.mDIV();
				
			}
				break;
			case 7:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:36: STOREW
			{
				this.mSTOREW();
				
			}
				break;
			case 8:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:43: LOADW
			{
				this.mLOADW();
				
			}
				break;
			case 9:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:49: BRANCH
			{
				this.mBRANCH();
				
			}
				break;
			case 10:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:56: BRANCHEQ
			{
				this.mBRANCHEQ();
				
			}
				break;
			case 11:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:65: BRANCHLESS
			{
				this.mBRANCHLESS();
				
			}
				break;
			case 12:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:76: JS
			{
				this.mJS();
				
			}
				break;
			case 13:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:79: LOADRA
			{
				this.mLOADRA();
				
			}
				break;
			case 14:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:86: STORERA
			{
				this.mSTORERA();
				
			}
				break;
			case 15:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:94: LOADRV
			{
				this.mLOADRV();
				
			}
				break;
			case 16:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:101: STORERV
			{
				this.mSTORERV();
				
			}
				break;
			case 17:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:109: LOADFP
			{
				this.mLOADFP();
				
			}
				break;
			case 18:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:116: STOREFP
			{
				this.mSTOREFP();
				
			}
				break;
			case 19:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:124: COPYFP
			{
				this.mCOPYFP();
				
			}
				break;
			case 20:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:131: LOADHP
			{
				this.mLOADHP();
				
			}
				break;
			case 21:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:138: STOREHP
			{
				this.mSTOREHP();
				
			}
				break;
			case 22:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:146: PRINT
			{
				this.mPRINT();
				
			}
				break;
			case 23:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:152: HALT
			{
				this.mHALT();
				
			}
				break;
			case 24:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:157: COL
			{
				this.mCOL();
				
			}
				break;
			case 25:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:161: LABEL
			{
				this.mLABEL();
				
			}
				break;
			case 26:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:167: NUMBER
			{
				this.mNUMBER();
				
			}
				break;
			case 27:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:174: WHITESP
			{
				this.mWHITESP();
				
			}
				break;
			case 28:
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:1:182: ERR
			{
				this.mERR();
				
			}
				break;
		
		}
		
	}
	
	// $ANTLR start "WHITESP"
	public final void mWHITESP() throws RecognitionException {
		try {
			final int _type = SVMLexer.WHITESP;
			final int _channel = BaseRecognizer.DEFAULT_TOKEN_CHANNEL;
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:105:10: ( ( '\\t' | ' ' | '\\r' | '\\n' )+ )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:105:12: ( '\\t' | ' ' | '\\r' | '\\n' )+
			{
				// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:105:12: ( '\\t' | ' ' | '\\r' | '\\n' )+
				int cnt5 = 0;
				loop5: do {
					int alt5 = 2;
					final int LA5_0 = this.input.LA(1);
					
					if ((((LA5_0 >= '\t') && (LA5_0 <= '\n')) || (LA5_0 == '\r') || (LA5_0 == ' '))) {
						alt5 = 1;
					}
					
					switch (alt5) {
						case 1:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:
						{
							if (((this.input.LA(1) >= '\t') && (this.input.LA(1) <= '\n')) || (this.input.LA(1) == '\r') || (this.input.LA(1) == ' ')) {
								this.input.consume();
								
							} else {
								final MismatchedSetException mse = new MismatchedSetException(null, this.input);
								this.recover(mse);
								throw mse;
							}
							
						}
							break;
						
						default:
							if (cnt5 >= 1) {
								break loop5;
							}
							final EarlyExitException eee = new EarlyExitException(5, this.input);
							throw eee;
					}
					cnt5++;
				} while (true);
				
				this.skip();
				
			}
			
			this.state.type = _type;
			this.state.channel = _channel;
		} finally {
		}
	}
	// $ANTLR end "WHITESP"
	
}