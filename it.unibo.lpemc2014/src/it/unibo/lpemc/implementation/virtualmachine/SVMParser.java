package it.unibo.lpemc.implementation.virtualmachine;

// $ANTLR 3.2 Sep 23, 2009 12:02:23 S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g 2014-12-12 15:35:29

import java.util.ArrayList;
import java.util.HashMap;

import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.BitSet;
import org.antlr.runtime.DFA;
import org.antlr.runtime.Parser;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenStream;

public class SVMParser extends Parser {
	class DFA1 extends DFA {
		
		public DFA1(final BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 1;
			this.eot = SVMParser.DFA1_eot;
			this.eof = SVMParser.DFA1_eof;
			this.min = SVMParser.DFA1_min;
			this.max = SVMParser.DFA1_max;
			this.accept = SVMParser.DFA1_accept;
			this.special = SVMParser.DFA1_special;
			this.transition = SVMParser.DFA1_transition;
		}
		
		@Override
		public String getDescription() {
			return "()* loopback of 27:11: ( PUSH e= NUMBER | PUSH e= LABEL | POP | ADD | SUB | MULT | DIV | STOREW | LOADW | e= LABEL COL | BRANCH e= LABEL | BRANCHEQ e= LABEL | BRANCHLESS e= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )*";
		}
	}
	
	public static final int					ADD									= 8;
	public static final int					BRANCH								= 15;
	public static final int					BRANCHEQ							= 16;
	public static final int					BRANCHLESS							= 17;
	public static final int					COL									= 14;
	public static final int					COPYFP								= 25;
	static final short[]					DFA1_accept							= DFA.unpackEncodedString(SVMParser.DFA1_acceptS);
	static final String						DFA1_acceptS						= "\1\uffff\1\32\1\uffff\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12\1\13\1"
			+ "\14\1\15\1\16\1\17\1\20\1\21\1\22\1\23\1\24\1\25\1\26\1\27\1\30" + "\1\31\1\1\1\2";
	static final short[]					DFA1_eof							= DFA.unpackEncodedString(SVMParser.DFA1_eofS);
	static final String						DFA1_eofS							= "\1\1\33\uffff";
	static final short[]					DFA1_eot							= DFA.unpackEncodedString(SVMParser.DFA1_eotS);
	static final String						DFA1_eotS							= "\34\uffff";
	static final char[]						DFA1_max							= DFA.unpackEncodedStringToUnsignedChars(SVMParser.DFA1_maxS);
	static final String						DFA1_maxS							= "\1\35\1\uffff\1\6\31\uffff";
	static final char[]						DFA1_min							= DFA.unpackEncodedStringToUnsignedChars(SVMParser.DFA1_minS);
	static final String						DFA1_minS							= "\1\4\1\uffff\1\5\31\uffff";
	static final short[]					DFA1_special						= DFA.unpackEncodedString(SVMParser.DFA1_specialS);
	static final String						DFA1_specialS						= "\34\uffff}>";
	static final short[][]					DFA1_transition;
	static final String[]					DFA1_transitionS					= {
			"\1\2\1\uffff\1\12\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\uffff\1\13" + "\1\14\1\15\1\16\1\17\1\20\1\21\1\22\1\23\1\24\1\25\1\26\1\27" + "\1\30\1\31", "", "\1\32\1\33", "", "", "", "", "", "",
			"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
	public static final int					DIV									= 11;
	public static final int					EOF									= -1;
	public static final int					ERR									= 31;
	public static final BitSet				FOLLOW_ADD_in_assembly80			= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_BRANCH_in_assembly184		= new BitSet(new long[] { 0x0000000000000040L });
	public static final BitSet				FOLLOW_BRANCHEQ_in_assembly198		= new BitSet(new long[] { 0x0000000000000040L });
	public static final BitSet				FOLLOW_BRANCHLESS_in_assembly211	= new BitSet(new long[] { 0x0000000000000040L });
	public static final BitSet				FOLLOW_COL_in_assembly171			= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_COPYFP_in_assembly360		= new BitSet(new long[] { 0x000000003FFFBFD2L });
	
	// delegates
	// delegators
	
	public static final BitSet				FOLLOW_DIV_in_assembly121			= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_HALT_in_assembly432			= new BitSet(new long[] { 0x000000003FFFBFD2L });
	
	public static final BitSet				FOLLOW_JS_in_assembly224			= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_LABEL_in_assembly169			= new BitSet(new long[] { 0x0000000000004000L });
	
	public static final BitSet				FOLLOW_LABEL_in_assembly188			= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_LABEL_in_assembly202			= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_LABEL_in_assembly215			= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_LABEL_in_assembly45			= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_LOADFP_in_assembly325		= new BitSet(new long[] { 0x000000003FFFBFD2L });
	
	public static final BitSet				FOLLOW_LOADHP_in_assembly378		= new BitSet(new long[] { 0x000000003FFFBFD2L });
	
	public static final BitSet				FOLLOW_LOADRA_in_assembly253		= new BitSet(new long[] { 0x000000003FFFBFD2L });
	
	// Delegated rules
	
	public static final BitSet				FOLLOW_LOADRV_in_assembly289		= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_LOADW_in_assembly148			= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_MULT_in_assembly108			= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_NUMBER_in_assembly30			= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_POP_in_assembly65			= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_PRINT_in_assembly413			= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_PUSH_in_assembly26			= new BitSet(new long[] { 0x0000000000000020L });
	public static final BitSet				FOLLOW_PUSH_in_assembly41			= new BitSet(new long[] { 0x0000000000000040L });
	
	public static final BitSet				FOLLOW_STOREFP_in_assembly343		= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_STOREHP_in_assembly396		= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_STORERA_in_assembly271		= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_STORERV_in_assembly307		= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_STOREW_in_assembly135		= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final BitSet				FOLLOW_SUB_in_assembly94			= new BitSet(new long[] { 0x000000003FFFBFD2L });
	public static final int					HALT								= 29;
	
	public static final int					JS									= 18;
	
	public static final int					LABEL								= 6;
	
	public static final int					LOADFP								= 23;
	public static final int					LOADHP								= 26;
	public static final int					LOADRA								= 19;
	public static final int					LOADRV								= 21;
	public static final int					LOADW								= 13;
	public static final int					MULT								= 10;
	public static final int					NUMBER								= 5;
	public static final int					POP									= 7;
	public static final int					PRINT								= 28;
	public static final int					PUSH								= 4;
	public static final int					STOREFP								= 24;
	public static final int					STOREHP								= 27;
	public static final int					STORERA								= 20;
	public static final int					STORERV								= 22;
	public static final int					STOREW								= 12;
	public static final int					SUB									= 9;
	public static final String[]			tokenNames							= new String[] { "<invalid>", "<EOR>", "<DOWN>", "<UP>", "PUSH", "NUMBER", "LABEL", "POP", "ADD", "SUB", "MULT", "DIV",
			"STOREW", "LOADW", "COL", "BRANCH", "BRANCHEQ", "BRANCHLESS", "JS", "LOADRA", "STORERA", "LOADRV", "STORERV", "LOADFP", "STOREFP", "COPYFP", "LOADHP", "STOREHP", "PRINT", "HALT",
			"WHITESP", "ERR"													};
	public static final int					WHITESP								= 30;
	static {
		final int numStates = SVMParser.DFA1_transitionS.length;
		DFA1_transition = new short[numStates][];
		for (int i = 0; i < numStates; i++) {
			SVMParser.DFA1_transition[i] = DFA.unpackEncodedString(SVMParser.DFA1_transitionS[i]);
		}
	}
	private final ArrayList<Integer>		addresses							= new ArrayList<Integer>();
	private final int[]						code								= new int[ExecuteVM.CODESIZE];
	protected DFA1							dfa1								= new DFA1(this);
	private int								i									= 0;
	private final HashMap<String, Integer>	labelAddress						= new HashMap<String, Integer>();
	private final ArrayList<String>			labels								= new ArrayList<String>();
	
	public SVMParser(final TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	
	public SVMParser(final TokenStream input, final RecognizerSharedState state) {
		super(input, state);
		
	}
	
	// $ANTLR start "assembly"
	// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:27:1: assembly : ( PUSH e= NUMBER | PUSH e= LABEL | POP | ADD | SUB | MULT | DIV | STOREW | LOADW | e= LABEL COL | BRANCH e=
	// LABEL | BRANCHEQ e= LABEL | BRANCHLESS e= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )* ;
	public final void assembly() throws RecognitionException {
		Token e = null;
		
		try {
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:27:9: ( ( PUSH e= NUMBER | PUSH e= LABEL | POP | ADD | SUB | MULT | DIV | STOREW | LOADW | e= LABEL COL | BRANCH e= LABEL
			// | BRANCHEQ e= LABEL | BRANCHLESS e= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )* )
			// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:27:11: ( PUSH e= NUMBER | PUSH e= LABEL | POP | ADD | SUB | MULT | DIV | STOREW | LOADW | e= LABEL COL | BRANCH e= LABEL
			// | BRANCHEQ e= LABEL | BRANCHLESS e= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )*
			{
				// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:27:11: ( PUSH e= NUMBER | PUSH e= LABEL | POP | ADD | SUB | MULT | DIV | STOREW | LOADW | e= LABEL COL | BRANCH e=
				// LABEL | BRANCHEQ e= LABEL | BRANCHLESS e= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )*
				loop1: do {
					int alt1 = 26;
					alt1 = this.dfa1.predict(this.input);
					switch (alt1) {
						case 1:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:27:13: PUSH e= NUMBER
						{
							this.match(this.input, SVMParser.PUSH, SVMParser.FOLLOW_PUSH_in_assembly26);
							e = (Token) this.match(this.input, SVMParser.NUMBER, SVMParser.FOLLOW_NUMBER_in_assembly30);
							this.code[this.i++] = SVMParser.PUSH;
							this.code[this.i++] = Integer.parseInt((e != null ? e.getText() : null));
							
						}
							break;
						case 2:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:29:6: PUSH e= LABEL
						{
							this.match(this.input, SVMParser.PUSH, SVMParser.FOLLOW_PUSH_in_assembly41);
							e = (Token) this.match(this.input, SVMParser.LABEL, SVMParser.FOLLOW_LABEL_in_assembly45);
							this.code[this.i++] = SVMParser.PUSH;
							this.labels.add((e != null ? e.getText() : null));
							this.addresses.add(new Integer(this.i));
							this.code[this.i++] = 0;
							
						}
							break;
						case 3:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:33:6: POP
						{
							this.match(this.input, SVMParser.POP, SVMParser.FOLLOW_POP_in_assembly65);
							this.code[this.i++] = SVMParser.POP;
							
						}
							break;
						case 4:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:34:6: ADD
						{
							this.match(this.input, SVMParser.ADD, SVMParser.FOLLOW_ADD_in_assembly80);
							this.code[this.i++] = SVMParser.ADD;
							
						}
							break;
						case 5:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:35:6: SUB
						{
							this.match(this.input, SVMParser.SUB, SVMParser.FOLLOW_SUB_in_assembly94);
							this.code[this.i++] = SVMParser.SUB;
							
						}
							break;
						case 6:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:36:6: MULT
						{
							this.match(this.input, SVMParser.MULT, SVMParser.FOLLOW_MULT_in_assembly108);
							this.code[this.i++] = SVMParser.MULT;
							
						}
							break;
						case 7:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:37:6: DIV
						{
							this.match(this.input, SVMParser.DIV, SVMParser.FOLLOW_DIV_in_assembly121);
							this.code[this.i++] = SVMParser.DIV;
							
						}
							break;
						case 8:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:38:6: STOREW
						{
							this.match(this.input, SVMParser.STOREW, SVMParser.FOLLOW_STOREW_in_assembly135);
							this.code[this.i++] = SVMParser.STOREW;
							
						}
							break;
						case 9:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:39:6: LOADW
						{
							this.match(this.input, SVMParser.LOADW, SVMParser.FOLLOW_LOADW_in_assembly148);
							this.code[this.i++] = SVMParser.LOADW;
							
						}
							break;
						case 10:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:40:6: e= LABEL COL
						{
							e = (Token) this.match(this.input, SVMParser.LABEL, SVMParser.FOLLOW_LABEL_in_assembly169);
							this.match(this.input, SVMParser.COL, SVMParser.FOLLOW_COL_in_assembly171);
							this.labelAddress.put((e != null ? e.getText() : null), new Integer(this.i));
							
						}
							break;
						case 11:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:41:6: BRANCH e= LABEL
						{
							this.match(this.input, SVMParser.BRANCH, SVMParser.FOLLOW_BRANCH_in_assembly184);
							e = (Token) this.match(this.input, SVMParser.LABEL, SVMParser.FOLLOW_LABEL_in_assembly188);
							this.code[this.i++] = SVMParser.BRANCH;
							this.labels.add((e != null ? e.getText() : null));
							this.addresses.add(new Integer(this.i));
							this.code[this.i++] = 0;
							
						}
							break;
						case 12:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:45:6: BRANCHEQ e= LABEL
						{
							this.match(this.input, SVMParser.BRANCHEQ, SVMParser.FOLLOW_BRANCHEQ_in_assembly198);
							e = (Token) this.match(this.input, SVMParser.LABEL, SVMParser.FOLLOW_LABEL_in_assembly202);
							this.code[this.i++] = SVMParser.BRANCHEQ;
							this.labels.add((e != null ? e.getText() : null));
							this.addresses.add(new Integer(this.i));
							this.code[this.i++] = 0;
							
						}
							break;
						case 13:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:49:6: BRANCHLESS e= LABEL
						{
							this.match(this.input, SVMParser.BRANCHLESS, SVMParser.FOLLOW_BRANCHLESS_in_assembly211);
							e = (Token) this.match(this.input, SVMParser.LABEL, SVMParser.FOLLOW_LABEL_in_assembly215);
							this.code[this.i++] = SVMParser.BRANCHLESS;
							this.labels.add((e != null ? e.getText() : null));
							this.addresses.add(new Integer(this.i));
							this.code[this.i++] = 0;
							
						}
							break;
						case 14:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:53:6: JS
						{
							this.match(this.input, SVMParser.JS, SVMParser.FOLLOW_JS_in_assembly224);
							this.code[this.i++] = SVMParser.JS;
							
						}
							break;
						case 15:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:54:6: LOADRA
						{
							this.match(this.input, SVMParser.LOADRA, SVMParser.FOLLOW_LOADRA_in_assembly253);
							this.code[this.i++] = SVMParser.LOADRA;
							
						}
							break;
						case 16:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:55:6: STORERA
						{
							this.match(this.input, SVMParser.STORERA, SVMParser.FOLLOW_STORERA_in_assembly271);
							this.code[this.i++] = SVMParser.STORERA;
							
						}
							break;
						case 17:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:56:6: LOADRV
						{
							this.match(this.input, SVMParser.LOADRV, SVMParser.FOLLOW_LOADRV_in_assembly289);
							this.code[this.i++] = SVMParser.LOADRV;
							
						}
							break;
						case 18:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:57:6: STORERV
						{
							this.match(this.input, SVMParser.STORERV, SVMParser.FOLLOW_STORERV_in_assembly307);
							this.code[this.i++] = SVMParser.STORERV;
							
						}
							break;
						case 19:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:58:6: LOADFP
						{
							this.match(this.input, SVMParser.LOADFP, SVMParser.FOLLOW_LOADFP_in_assembly325);
							this.code[this.i++] = SVMParser.LOADFP;
							
						}
							break;
						case 20:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:59:6: STOREFP
						{
							this.match(this.input, SVMParser.STOREFP, SVMParser.FOLLOW_STOREFP_in_assembly343);
							this.code[this.i++] = SVMParser.STOREFP;
							
						}
							break;
						case 21:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:60:6: COPYFP
						{
							this.match(this.input, SVMParser.COPYFP, SVMParser.FOLLOW_COPYFP_in_assembly360);
							this.code[this.i++] = SVMParser.COPYFP;
							
						}
							break;
						case 22:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:61:6: LOADHP
						{
							this.match(this.input, SVMParser.LOADHP, SVMParser.FOLLOW_LOADHP_in_assembly378);
							this.code[this.i++] = SVMParser.LOADHP;
							
						}
							break;
						case 23:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:62:6: STOREHP
						{
							this.match(this.input, SVMParser.STOREHP, SVMParser.FOLLOW_STOREHP_in_assembly396);
							this.code[this.i++] = SVMParser.STOREHP;
							
						}
							break;
						case 24:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:63:6: PRINT
						{
							this.match(this.input, SVMParser.PRINT, SVMParser.FOLLOW_PRINT_in_assembly413);
							this.code[this.i++] = SVMParser.PRINT;
							
						}
							break;
						case 25:
						// S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g:64:6: HALT
						{
							this.match(this.input, SVMParser.HALT, SVMParser.FOLLOW_HALT_in_assembly432);
							this.code[this.i++] = SVMParser.HALT;
							
						}
							break;
						
						default:
							break loop1;
					}
				} while (true);
				
				for (int ind = 0; ind < this.labels.size(); ind++) {
					final Integer t = this.labelAddress.get(this.labels.get(ind));
					final Integer a = this.addresses.get(ind);
					this.code[a.intValue()] = t.intValue();
				}
				
			}
			
		} catch (final RecognitionException re) {
			this.reportError(re);
			this.recover(this.input, re);
		} finally {
		}
		return;
	}
	
	// $ANTLR end "assembly"
	public int[] createCode() throws Exception {
		this.assembly();
		return this.code;
	}
	
	@Override
	public String getGrammarFileName() {
		return "S:\\LPeMC\\MaterialeDidattico\\Esercitazione_12_12\\FOOL\\SVM.g";
	}
	
	@Override
	public String[] getTokenNames() {
		return SVMParser.tokenNames;
	}
	
}