package it.unibo.lpemc.implementation.virtualmachine;

import it.unibo.lpemc.implementation.Syskb;
import it.unibo.lpemc.interfaces.IOutput;

public class ExecuteVM {
	
	// DEFINE
	public static final int	CODESIZE		= 10000;
	public static final int	DEFAULTVALUE	= Integer.MAX_VALUE;
	public static final int	MEMSIZE			= 10000;
	// MACHINE
	private final int[]		code;
	// REGISTERS
	private int				fp				= ExecuteVM.MEMSIZE;			// Frame Pointer
	
	private int				hp				= 0;							// Heap Pointer
	/*
	 * HP serve per puntare alla prossima locazione disponobile dello heap; assumendo di usare gli indirizzi bassi per lo heap, HP contiene inizialmente il valore 0.
	 */
	private int				ip				= 0;							// Instruction Pointer
	// OUTPUT
	private IOutput<String>	log;
	private final int[]		memory			= new int[ExecuteVM.MEMSIZE];
	private int				ra;
	private int				rv;
	
	private int				sp				= ExecuteVM.MEMSIZE;			// Stack Pointer
	private boolean			verbose;
	
	public ExecuteVM(final int[] code) {
		this(code, null);
	}
	
	public ExecuteVM(final int[] code, final IOutput<String> log) {
		this.code = code;
		this.log = log;
		
		for (int i = 0; i < this.memory.length; i++) {
			this.memory[i] = Integer.MAX_VALUE;
		}
		if (this.log == null) {
			this.verbose = false;
		} else {
			this.verbose = true;
		}
	}
	
	private void checkHp() throws Exception {
		if (this.sp < this.hp) {
			throw new Exception("SP < HP!!!");
		}
	}
	
	public String cpu() throws Exception {
		String ret = "";
		while (this.ip < this.code.length) {
			final int bytecode = this.code[this.ip++]; // fetch
			int v1, v2;
			switch (bytecode) {
				case SVMParser.PUSH:
					if (this.verbose) {
						this.log.addOutput(Syskb.PUSH + this.code[this.ip] + Syskb.CR);
					}
					this.push(this.code[this.ip++]);
					break;
				case SVMParser.POP:
					if (this.verbose) {
						this.log.addOutput(Syskb.POP);
					}
					this.pop();
					break;
				case SVMParser.ADD:
					if (this.verbose) {
						this.log.addOutput(Syskb.ADD);
					}
					v2 = this.pop();
					v1 = this.pop();
					this.push(v1 + v2);
					break;
				case SVMParser.MULT:
					if (this.verbose) {
						this.log.addOutput(Syskb.MULT);
					}
					v2 = this.pop();
					v1 = this.pop();
					this.push(v1 * v2);
					break;
				case SVMParser.DIV:
					if (this.verbose) {
						this.log.addOutput(Syskb.DIV);
					}
					v2 = this.pop();
					v1 = this.pop();
					this.push(v1 / v2);
					break;
				case SVMParser.SUB:
					if (this.verbose) {
						this.log.addOutput(Syskb.SUB);
					}
					v2 = this.pop();
					v1 = this.pop();
					this.push(v1 - v2);
					break;
				case SVMParser.STOREW:
					if (this.verbose) {
						this.log.addOutput(Syskb.STOREW);
					}
					int address = this.pop();
					this.memory[address] = this.pop();
					break;
				case SVMParser.LOADW:
					if (this.verbose) {
						this.log.addOutput(Syskb.LOADW);
					}
					this.push(this.memory[this.pop()]);
					break;
				case SVMParser.BRANCH:
					if (this.verbose) {
						this.log.addOutput(Syskb.BRANCH);
					}
					address = this.code[this.ip++];
					this.ip = address;
					break;
				case SVMParser.BRANCHEQ:
					if (this.verbose) {
						this.log.addOutput(Syskb.BRANCHEQ);
					}
					address = this.code[this.ip++];
					if (this.pop() == this.pop()) {
						this.ip = address;
					}
					break;
				case SVMParser.BRANCHLESS:
					if (this.verbose) {
						this.log.addOutput(Syskb.BRANCHLESS);
					}
					address = this.code[this.ip++];
					v2 = this.pop();
					v1 = this.pop();
					if (v1 <= v2) {
						this.ip = address;
					}
					break;
				case SVMParser.JS:
					if (this.verbose) {
						this.log.addOutput(Syskb.JS);
					}
					address = this.pop();
					this.ra = this.ip;
					this.ip = address;
					break;
				case SVMParser.STORERA:
					if (this.verbose) {
						this.log.addOutput(Syskb.STORERA);
					}
					this.ra = this.pop();
					break;
				case SVMParser.LOADRA:
					if (this.verbose) {
						this.log.addOutput(Syskb.LOADRA);
					}
					this.push(this.ra);
					break;
				case SVMParser.STORERV:
					if (this.verbose) {
						this.log.addOutput(Syskb.STORERV);
					}
					this.rv = this.pop();
					break;
				case SVMParser.LOADRV:
					if (this.verbose) {
						this.log.addOutput(Syskb.LOADRV);
					}
					this.push(this.rv);
					break;
				case SVMParser.LOADFP:
					if (this.verbose) {
						this.log.addOutput(Syskb.LOADFP);
					}
					this.push(this.fp);
					break;
				case SVMParser.STOREFP:
					if (this.verbose) {
						this.log.addOutput(Syskb.STOREFP);
					}
					this.fp = this.pop();
					break;
				case SVMParser.COPYFP:
					if (this.verbose) {
						this.log.addOutput(Syskb.COPYFP);
					}
					this.fp = this.sp;
					break;
				case SVMParser.STOREHP:
					if (this.verbose) {
						this.log.addOutput(Syskb.STOREHP);
					}
					this.hp = this.pop();
					this.checkHp();
					break;
				case SVMParser.LOADHP:
					if (this.verbose) {
						this.log.addOutput(Syskb.LOADHP);
					}
					this.push(this.hp);
					break;
				case SVMParser.PRINT:
					if (this.verbose) {
						this.log.addOutput(Syskb.PRINT + "    ");
					}
					ret = this.memory[this.sp] + "";
					System.out.println(ret);
					break;
				case SVMParser.HALT:
					if (this.verbose) {
						this.log.addOutput(Syskb.HALT);
					}
					return ret;
			}
		}
		return ret;
	}
	
	private int pop() {
		int ret = this.memory[this.sp++];
		this.memory[this.sp-1] = DEFAULTVALUE;
		return ret;
	}
	
	private void push(final int b) throws Exception {
		this.memory[--this.sp] = b;
		this.checkHp();
	}
	
}
