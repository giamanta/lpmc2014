package it.unibo.lpemc.implementation;

import it.unibo.lpemc.interfaces.Node;

public class ParNode extends Node {
	
	private final Node	type;
	
//	public ParNode(final Node t) {
//		this("dummyParNode", t);
//	}
	
	public ParNode(final String i, final Node t) {
		super(i);
		this.type = t;
	}
	
	@Override
	public Node typeCheck() {
		return this.type;
	}
}