package it.unibo.lpemc.test;

import it.unibo.lpemc.implementation.FOOLLexer;
import it.unibo.lpemc.implementation.FOOLParser;
import it.unibo.lpemc.implementation.virtualmachine.ExecuteVM;
import it.unibo.lpemc.implementation.virtualmachine.SVMLexer;
import it.unibo.lpemc.implementation.virtualmachine.SVMParser;
import it.unibo.lpemc.interfaces.Node;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;

public class Test {
	public static void main(final String[] args) throws Exception {
//		final IOutput<String> log = new IOutput<String>() {
//			@Override
//			public void addOutput(final String o) {
//				System.out.println(o);
//			}
//		};
//		Syskb.setLog(log);
//		
		final String fileName = new String(Files.readAllBytes(Paths.get("config.txt"))).trim().split("\n")[0];
		final ANTLRFileStream input = new ANTLRFileStream("tests/" + fileName);
		final FOOLLexer lexer = new FOOLLexer(input);
		final CommonTokenStream tokens = new CommonTokenStream(lexer);
		final FOOLParser parser = new FOOLParser(tokens);
		System.out.println("Print AST _________________________________________________");
		final Node ast = parser.prog();
		System.out.println();
		System.out.println(ast.toPrint("| "));
		System.out.println();
		System.out.println("Type check _________________________________________________");
		System.out.println((ast.typeCheck()).toPrint(""));
//		System.out.println();
//		System.out.println("Code generation ____________________________________________");
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.close();
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode()
//				, log
		);
		System.out.println();
		System.out.println("RESULT _____________________________________________________");
		vm.cpu();
	}
}
