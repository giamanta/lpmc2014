package it.unibo.lpemc.test;

import it.unibo.lpemc.implementation.virtualmachine.ExecuteVM;
import it.unibo.lpemc.implementation.virtualmachine.SVMLexer;
import it.unibo.lpemc.implementation.virtualmachine.SVMParser;
import it.unibo.lpemc.interfaces.IOutput;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;

public class TestAsmCode {
	public static void main(final String[] args) throws Exception {

		final String fileName = "__stackVerification.asm";
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream(fileName));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode()
		 , new IOutput<String>() {
		 @Override public void addOutput(final String o) 
				{ System.out.print("[code] -> " + o); } }
		);
		System.out.println();
		System.out.println("RESULT _____________________________________________________");
		vm.cpu();
	}
}
