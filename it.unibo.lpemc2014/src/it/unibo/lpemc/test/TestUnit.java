package it.unibo.lpemc.test;

import static org.junit.Assert.fail;
import it.unibo.lpemc.implementation.FOOLLexer;
import it.unibo.lpemc.implementation.FOOLParser;
import it.unibo.lpemc.implementation.FOOLlib;
import it.unibo.lpemc.implementation.virtualmachine.ExecuteVM;
import it.unibo.lpemc.implementation.virtualmachine.SVMLexer;
import it.unibo.lpemc.implementation.virtualmachine.SVMParser;
import it.unibo.lpemc.interfaces.Node;

import java.io.BufferedWriter;
import java.io.FileWriter;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestUnit {
	
	@After
	public void after() {
		FOOLlib.resetLabel();
		try {
			Thread.sleep(10);
		} catch (final InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Before
	public void config() {
		System.out.println("\nTest__________________________");
	}
	
	@Test
	public void first_require() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "1_operators.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("31337"));
	}
	
	@Test
	public void second_require() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "2_functional.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("20"));
	}
	
	@Test
	public void second_require_2() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "2_functionalVariance.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("1"));
	}
	
	@Test
	public void second_require_3() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "2_functionalFunInVar.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("11"));
	}
	
	@Test
	public void third_require1() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "3_1classField.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("31337"));
	}
	
	@Test
	public void third_require2() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "3_2classFieldMet.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("62677"));
	}
	
	@Test
	public void third_require3() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "3_3classExtends.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("28"));
	}
	
	@Test
	public void third_require4() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "3_4classWithClassField.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("15"));
	}
	
	@Test
	public void third_require5() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "3_5classWithClassMethod.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("22"));
	}
	
	@Test
	public void third_require6() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "3_6classNested.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("25"));
	}
	
	@Test
	public void third_require7() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "3_7quicksort.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("6"));
	}
	
	@Test
	public void quicksort() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "quicksort.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("6"));
	}
	
	@Test
	public void testExtendsCheck() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "extendsCheck.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("1"));
	}
	
	@Test
	public void test00() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "prova0.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("2"));
	}
	
	@Test
	public void test01() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "prova1.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("0"));
	}
	
	@Test
	public void test02() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "prova2.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("0"));
	}
	
	@Test
	public void test03() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "prova3.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("22"));
	}
	
	@Test
	public void test04() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "prova4.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("2"));
	}
	
	@Test
	public void test05() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "prova5.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("8"));
	}
	
	@Test
	public void test06() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "prova6.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("22"));
	}
	
	@Test
	public void testand() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "provaAND.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("1"));
	}
	
	@Test
	public void testciatto() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "ciatto.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("128"));
	}
	
	@Test
	public void testciatto2() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "ciatto2.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("16"));
	}
	
	@Test
	public void testciatto3() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "ciatto3.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("16"));
	}
	
	@Test
	public void testciatto4() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "ciatto4.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("0"));
	}
	
	@Test
	public void testfor() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "for.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("4"));
	}
	
	@Test
	public void testfunctional01() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "functional01.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("31348"));
	}
	
	@Test
	public void testfunctional02() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "functional02.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("31349"));
	}
	
	@Test
	public void testfunctional03() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "functional03.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("22"));
	}
	
	@Test
	public void testfunctional04() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "functional04.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("28"));
	}
	
	@Test
	public void testG() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "provaG.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("1"));
	}
	
	@Test
	public void testge() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "provaGE.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("1"));
	}
	
	@Test
	public void testL() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "provaL.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("1"));
	}
	
	@Test
	public void testle() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "provaLE.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("1"));
	}
	
	@Test
	public void testnot() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "provaNOT.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("1"));
	}
	
	@Test
	public void testoop01() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop01.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("5"));
	}
	
	@Test
	public void testoop02() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop02.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("13"));
	}
	
	@Test
	public void testoop03() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop03.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("13"));
	}
	
	@Test
	public void testoop03_variation1() throws Exception {
		
		try {
			ANTLRFileStream input;
			FOOLLexer lexer;
			CommonTokenStream tokens;
			FOOLParser parser;
			
			final String fileName = "oop03_variation1.fool";
			System.out.println(fileName);
			input = new ANTLRFileStream("tests/" + fileName);
			lexer = new FOOLLexer(input);
			tokens = new CommonTokenStream(lexer);
			parser = new FOOLParser(tokens);
			final Node ast = parser.prog();
			
			final String code = ast.codeGeneration(); // ora qui troviamo il codice
			final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
			final BufferedWriter out = new BufferedWriter(fstream);
			out.write(code);
			out.flush();
			out.close();
			fstream.close();
			System.out.println((ast.typeCheck()).toPrint(""));
			
			final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
			final CommonTokenStream tokensVM = new CommonTokenStream(lex);
			final SVMParser parserVM = new SVMParser(tokensVM);
			
			final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
			vm.cpu();
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}
	
	@Test
	public void testoop03_variation2() throws Exception {
		
		try {
			ANTLRFileStream input;
			FOOLLexer lexer;
			CommonTokenStream tokens;
			FOOLParser parser;
			
			final String fileName = "oop03_variation2.fool";
			System.out.println(fileName);
			input = new ANTLRFileStream("tests/" + fileName);
			lexer = new FOOLLexer(input);
			tokens = new CommonTokenStream(lexer);
			parser = new FOOLParser(tokens);
			final Node ast = parser.prog();
			
			final String code = ast.codeGeneration(); // ora qui troviamo il codice
			final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
			final BufferedWriter out = new BufferedWriter(fstream);
			out.write(code);
			out.flush();
			out.close();
			fstream.close();
			System.out.println((ast.typeCheck()).toPrint(""));
			
			final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
			final CommonTokenStream tokensVM = new CommonTokenStream(lex);
			final SVMParser parserVM = new SVMParser(tokensVM);
			
			final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
			vm.cpu();
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}
	
	@Test
	public void testoop03_variation3() throws Exception {
		
		try {
			ANTLRFileStream input;
			FOOLLexer lexer;
			CommonTokenStream tokens;
			FOOLParser parser;
			
			final String fileName = "oop03_variation3.fool";
			System.out.println(fileName);
			input = new ANTLRFileStream("tests/" + fileName);
			lexer = new FOOLLexer(input);
			tokens = new CommonTokenStream(lexer);
			parser = new FOOLParser(tokens);
			final Node ast = parser.prog();
			
			final String code = ast.codeGeneration(); // ora qui troviamo il codice
			final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
			final BufferedWriter out = new BufferedWriter(fstream);
			out.write(code);
			out.flush();
			out.close();
			fstream.close();
			System.out.println((ast.typeCheck()).toPrint(""));
			
			final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
			final CommonTokenStream tokensVM = new CommonTokenStream(lex);
			final SVMParser parserVM = new SVMParser(tokensVM);
			
			final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
			vm.cpu();
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}
	
	@Test
	public void testoop03_variation4() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop03_variation4.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("1"));
	}
	
	@Test
	public void testoop03_variation5() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop03_variation5.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("1"));
	}
	
	@Test
	public void testoop03_variation6() throws Exception {
		
		try {
			ANTLRFileStream input;
			FOOLLexer lexer;
			CommonTokenStream tokens;
			FOOLParser parser;
			
			final String fileName = "oop03_variation6.fool";
			System.out.println(fileName);
			input = new ANTLRFileStream("tests/" + fileName);
			lexer = new FOOLLexer(input);
			tokens = new CommonTokenStream(lexer);
			parser = new FOOLParser(tokens);
			final Node ast = parser.prog();
			
			final String code = ast.codeGeneration(); // ora qui troviamo il codice
			final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
			final BufferedWriter out = new BufferedWriter(fstream);
			out.write(code);
			out.flush();
			out.close();
			fstream.close();
			System.out.println((ast.typeCheck()).toPrint(""));
			
			final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
			final CommonTokenStream tokensVM = new CommonTokenStream(lex);
			final SVMParser parserVM = new SVMParser(tokensVM);
			
			final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
			vm.cpu();
			fail();
		} catch (Exception e) {
			assert (true);
		}
	}
	
	@Test
	public void testoop04() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop04.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("13"));
	}
	
	@Test
	public void testoop05() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop05.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("10"));
	}
	
	@Test
	public void testoop06() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop06.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("20"));
	}
	
	@Test
	public void testoop07() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop07.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("37"));
	}
	
	@Test
	public void testoop08() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop08.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("18"));
	}
	
	@Test
	public void testoop09() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop09.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("3"));
	}
	
	@Test
	public void testoop10() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop10.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("7"));
	}
	
	@Test
	public void testoop11() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop11.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("11"));
	}
	
	@Test
	public void testoop12() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop12.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("45"));
	}
	
	@Test
	public void testoop13() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop13.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("3"));
	}
	
	@Test
	public void testoop14() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop14.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("15"));
	}
	
	@Test
	public void testoop15() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop15.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("12"));
	}
	
	@Test
	public void testoop16() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop16.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("12"));
	}
	
	@Test
	public void testoop17() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop17.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("22"));
	}
	
	@Test
	public void testoop18() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop18.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("22"));
	}
	
	@Test
	public void testoop19() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop19.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("48"));
	}
	
	@Test
	public void testoop20() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop20.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("22"));
	}
	
	@Test
	public void testoop21() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop21.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("22"));
	}
	
	@Test
	public void oop21_variation1() throws Exception {
		try {
			ANTLRFileStream input;
			FOOLLexer lexer;
			CommonTokenStream tokens;
			FOOLParser parser;
			
			final String fileName = "oop21_variation1.fool";
			System.out.println(fileName);
			input = new ANTLRFileStream("tests/" + fileName);
			lexer = new FOOLLexer(input);
			tokens = new CommonTokenStream(lexer);
			parser = new FOOLParser(tokens);
			final Node ast = parser.prog();
			
			final String code = ast.codeGeneration(); // ora qui troviamo il codice
			final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
			final BufferedWriter out = new BufferedWriter(fstream);
			out.write(code);
			out.flush();
			out.close();
			fstream.close();
			System.out.println((ast.typeCheck()).toPrint(""));
			
			final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
			final CommonTokenStream tokensVM = new CommonTokenStream(lex);
			final SVMParser parserVM = new SVMParser(tokensVM);
			
			final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
			vm.cpu();
			fail();
		} catch (Exception ex) {
			Assert.assertTrue(true);
		}
	}
	
	@Test
	public void oop21_variation2() throws Exception {
		try {
			ANTLRFileStream input;
			FOOLLexer lexer;
			CommonTokenStream tokens;
			FOOLParser parser;
			
			final String fileName = "oop21_variation2.fool";
			System.out.println(fileName);
			input = new ANTLRFileStream("tests/" + fileName);
			lexer = new FOOLLexer(input);
			tokens = new CommonTokenStream(lexer);
			parser = new FOOLParser(tokens);
			final Node ast = parser.prog();
			
			final String code = ast.codeGeneration(); // ora qui troviamo il codice
			final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
			final BufferedWriter out = new BufferedWriter(fstream);
			out.write(code);
			out.flush();
			out.close();
			fstream.close();
			System.out.println((ast.typeCheck()).toPrint(""));
			
			final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
			final CommonTokenStream tokensVM = new CommonTokenStream(lex);
			final SVMParser parserVM = new SVMParser(tokensVM);
			
			final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
			vm.cpu();
			fail();
		} catch (Exception ex) {
			Assert.assertTrue(true);
		}
	}
	
	@Test
	public void oop21_variation3() throws Exception {
		try {
			ANTLRFileStream input;
			FOOLLexer lexer;
			CommonTokenStream tokens;
			FOOLParser parser;
			
			final String fileName = "oop21_variation3.fool";
			System.out.println(fileName);
			input = new ANTLRFileStream("tests/" + fileName);
			lexer = new FOOLLexer(input);
			tokens = new CommonTokenStream(lexer);
			parser = new FOOLParser(tokens);
			final Node ast = parser.prog();
			
			final String code = ast.codeGeneration(); // ora qui troviamo il codice
			final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
			final BufferedWriter out = new BufferedWriter(fstream);
			out.write(code);
			out.flush();
			out.close();
			fstream.close();
			System.out.println((ast.typeCheck()).toPrint(""));
			
			final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
			final CommonTokenStream tokensVM = new CommonTokenStream(lex);
			final SVMParser parserVM = new SVMParser(tokensVM);
			
			final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
			vm.cpu();
			fail();
		} catch (Exception ex) {
			Assert.assertTrue(true);
		}
	}
	
	@Test
	public void oop21_variation4() throws Exception {
		try {
			ANTLRFileStream input;
			FOOLLexer lexer;
			CommonTokenStream tokens;
			FOOLParser parser;
			
			final String fileName = "oop21_variation4.fool";
			System.out.println(fileName);
			input = new ANTLRFileStream("tests/" + fileName);
			lexer = new FOOLLexer(input);
			tokens = new CommonTokenStream(lexer);
			parser = new FOOLParser(tokens);
			final Node ast = parser.prog();
			
			final String code = ast.codeGeneration(); // ora qui troviamo il codice
			final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
			final BufferedWriter out = new BufferedWriter(fstream);
			out.write(code);
			out.flush();
			out.close();
			fstream.close();
			System.out.println((ast.typeCheck()).toPrint(""));
			
			final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
			final CommonTokenStream tokensVM = new CommonTokenStream(lex);
			final SVMParser parserVM = new SVMParser(tokensVM);
			
			final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
			vm.cpu();
			fail();
		} catch (Exception ex) {
			Assert.assertTrue(true);
		}
	}
	
	@Test
	public void testoop22() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop22.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("1"));
	}
	
	@Test
	public void testoop23() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop23.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("25"));
	}
	
	@Test
	public void testoop24() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop24.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("7"));
	}
	
	@Test
	public void testoop25() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop25.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("2"));
	}
	
	@Test
	public void testoop26() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop26.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("7"));
	}
	
	@Test
	public void testoop27() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop27.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("5"));
	}
	
	@Test
	public void testoop28() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop28.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("4"));
	}
	
	@Test
	public void testoop30() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop30.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("5"));
	}
	
	@Test
	public void testoop31() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "oop31.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("3"));
	}
	
	@Test
	public void testor() throws Exception {
		ANTLRFileStream input;
		FOOLLexer lexer;
		CommonTokenStream tokens;
		FOOLParser parser;
		
		final String fileName = "provaOR.fool";
		System.out.println(fileName);
		input = new ANTLRFileStream("tests/" + fileName);
		lexer = new FOOLLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new FOOLParser(tokens);
		final Node ast = parser.prog();
		
		final String code = ast.codeGeneration(); // ora qui troviamo il codice
		final FileWriter fstream = new FileWriter("build/" + fileName + ".asm");
		final BufferedWriter out = new BufferedWriter(fstream);
		out.write(code);
		out.flush();
		out.close();
		fstream.close();
		System.out.println((ast.typeCheck()).toPrint(""));
		
		final SVMLexer lex = new SVMLexer(new ANTLRFileStream("build/" + fileName + ".asm"));
		final CommonTokenStream tokensVM = new CommonTokenStream(lex);
		final SVMParser parserVM = new SVMParser(tokensVM);
		
		final ExecuteVM vm = new ExecuteVM(parserVM.createCode());
		Assert.assertTrue(vm.cpu().equals("1"));
	}
	
}
