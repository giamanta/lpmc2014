package it.unibo.lpemc.interfaces;

import it.unibo.lpemc.implementation.Syskb;

import java.util.ArrayList;

public class MergeNode extends Node {
	private final String	name;
	private final Node[]	nodes;
	
	public MergeNode(final String name, final ArrayList<Node> nodes) {
		super();
		this.nodes = nodes.toArray(new Node[nodes.size()]);
		this.name = name;
	}
	
	public MergeNode(final String name, final Node... nodes) {
		this.nodes = nodes;
		this.name = name;
	}
	
	@Override
	public String toPrint(final String indent) {
		String ret = indent + this.name + "\n" /* + indent + "->\n" */;
		for (final Node n : this.nodes) {
			ret += n!=null? n.toPrint(indent + Syskb.INDENT) + "\n" : "";
		}
		return ret;
	}
	
	@Override
	public Node typeCheck() {
		return null;
	}
}
