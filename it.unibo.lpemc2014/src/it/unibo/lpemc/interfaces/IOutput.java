package it.unibo.lpemc.interfaces;

/**
 * @author Matteo Francia
 * @param <T>
 *            Output type
 */
public interface IOutput<T> {
	/**
	 * Add a generic output
	 * 
	 * @param o
	 *            output value
	 */
	public void addOutput(T o);
}
