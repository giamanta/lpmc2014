package it.unibo.lpemc.interfaces;

import it.unibo.lpemc.implementation.Syskb;

public class ValueNode extends NodePrintable {
	
	public ValueNode(final Node type, final Object content) {
		super(type, content);
	}
	
	public ValueNode(final Object content) {
		super(content);
	}
	
	@Override
	protected String codeGen() {
		return Syskb.PUSH + this.getValue() + Syskb.CR;
	}
}
