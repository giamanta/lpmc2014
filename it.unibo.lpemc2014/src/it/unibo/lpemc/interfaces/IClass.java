package it.unibo.lpemc.interfaces;

import it.unibo.lpemc.implementation.MethodNode;
import it.unibo.lpemc.implementation.oop.FieldNode;

import java.util.ArrayList;

/**
 * Interfaccia che realizza il concetto di classe OOP
 */
public interface IClass extends INode {
	/**
	 * Aggiunge un nuovo campo alla classe
	 * 
	 * @param field
	 */
	void addField(Node field);
	
	// int getTotalFieldsNumber();
	//
	// int getFieldsEreditatiNumber();
	//
	// int getTotalMethodsNumber();
	//
	// int getMethodsEreditatiNumber();
	
	/**
	 * Aggiunge un nuovo metodo alla classe
	 * 
	 * @param field
	 */
	void addMethod(MethodNode method);
	
	public FieldNode getFieldNode(final String fieldId);
	
	/**
	 * Ritorna l'offset del campo
	 * 
	 * @param fieldId
	 * @return
	 */
	int getFieldOffset(String fieldId);
	
	ArrayList<Node> getStruct();
	
	// public ArrayList<FieldNode> getFieldsNode();
	
	int getStuctSize();
	
	/**
	 * Ritorna true se la classe contiene il field
	 * 
	 * @param fieldId
	 * @return
	 */
	boolean hasField(String fieldId);
	
	/**
	 * Imposta la super classe da cui la classe corrente eredita
	 * 
	 * @param superClass
	 */
	void setSuperClass(/* IClass */String superClass);
	
}
