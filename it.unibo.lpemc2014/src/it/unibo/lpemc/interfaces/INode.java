package it.unibo.lpemc.interfaces;

import it.unibo.lpemc.implementation.STentry;

public interface INode {
	public String codeGeneration();
	
	public INode getFather();
	
	public String getId();
	
	public int getSize();
	
	public STentry getSTEntry();
	
	public void setFather(INode father);
	
	void setSTEntry(STentry entry);
	
	public String toPrint(String indent);
	
	public Node typeCheck();
}
