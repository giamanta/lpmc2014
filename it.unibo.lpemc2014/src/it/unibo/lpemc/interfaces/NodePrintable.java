package it.unibo.lpemc.interfaces;

public class NodePrintable extends Node {
	private final Object	content;
	private final String	name;
	private final Node		type;
	
	public NodePrintable(final Node type, final Object content) {
		this(null, type, content);
	}
	
	public NodePrintable(final Object content) {
		this(null, null, content);
	}
	
	public NodePrintable(final String name, final Node type, final Object content) {
		super(name);
		this.type = type;
		this.name = name;
		this.content = content;
	}
	
	protected Object getValue() {
		return this.content;
	}
	
	@Override
	public String toPrint(final String indent) {
		return indent + (this.name != null ? this.name + ":" : "") + (this.type != null ? this.type.typeCheck().toPrint("") + " " : "") + this.content.toString();
	}
	
	@Override
	public Node typeCheck() {
		return this.type;
	}
}
