package it.unibo.lpemc.interfaces;

import it.unibo.lpemc.implementation.STentry;
import it.unibo.lpemc.implementation.Syskb;

public abstract class Node implements INode {
	private STentry		entry;
	private INode		father;
	protected String	id;
	
	public Node() {
		this("");
	}
	
	public Node(final String id) {
		this.id = id;
	}
	
	public Node(final String id, STentry entry) {
		this.id = id;
		setSTEntry(entry);
	}
	
	protected String codeGen() {
		return this.toString() + " dummyCodeGen";
	}
	
	@Override
	public String codeGeneration() {
		final String s = this.codeGen();
		Syskb.log(this.toString() + "\nid:" + this.id + "\n" + s);
		return s;
	}
	
	@Override
	public INode getFather() {
		return father;
	}
	
	@Override
	public String getId() {
		return this.id;
	}
	
	@Override
	public int getSize() {
		return typeCheck().getSize();
	}
	
	@Override
	public STentry getSTEntry() {
		return this.entry;
	}
	
	@Override
	public void setFather(INode father) {
		this.father = father;
	}
	
	@Override
	public void setSTEntry(final STentry entry) {
		this.entry = entry;
	}
	
	@Override
	public String toPrint(final String indent) {
		return this.getId();
	}
	
	@Override
	abstract public Node typeCheck();
}