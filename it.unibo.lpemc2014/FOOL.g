grammar FOOL;

@header{ // {{{
	package it.unibo.lpemc.implementation;
	import java.util.ArrayList;
	import java.util.HashMap;
}
@lexer::header {package it.unibo.lpemc.implementation;}
@members{
	private ArrayList<HashMap<String,STentry>>  symTable = new ArrayList<HashMap<String,STentry>>();
	private int nestingLevel = -1;
}
// }}}

// * PARSER RULES * {{{

prog	returns [Node ast] // let clist declist {{{
	: e=exp SEMIC {$ast = new ProgNode($e.ast);}
  | LET
    { nestingLevel++;
      HashMap<String,STentry> hm = new HashMap<String,STentry> ();
      symTable.add(hm);
    }
    c=cllist
    d=declist IN e=exp SEMIC
    { symTable.remove(nestingLevel--);
      $ast = new LetInNode($d.astlist,$e.ast);
    }
  ;
// }}}

cllist returns [ArrayList<Node> classList] // classi {{{
  :
  // creo la lista delle dichiarazione delle classi
  {$classList = new ArrayList<Node>();}
  // creo la nuova classe e la aggiungo alla lista
  (CLASS className=ID
  	{
  		ClassNode classNode = new ClassNode($className.text);
  		classList.add(classNode);
      // ora inserisco un nuovo nesting level per i campi e i metodi interni alla classe
      nestingLevel++;
      // creo una nuova hashmap e la inserisco nella lista
      HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
      symTable.add(hmn);
  	}
  // e.g. : class Double extends Integer
  (EXTENDS extClassName=ID {classNode.setSuperClass($extClassName.text);})?
  // class MyClass (field0:type, field1:type, ..., fieldN:type)
  {ArrayList<Node> fieldTypes = new ArrayList<Node>(); Offset fieldOffset = new Offset(1);}
  LPAR (field=ID COLON fieldType=basic
  { fieldTypes.add($fieldType.ast);
    FOOLlib.factory.createFieldInClass($field.line, classNode, $field.text, $fieldType.ast, hmn, nestingLevel, fieldOffset);
  }
  (COMMA otherField=ID COLON otherFieldType=basic
	{ fieldTypes.add($otherFieldType.ast);
		FOOLlib.factory.createFieldInClass($otherField.line, classNode, $otherField.text, $otherFieldType.ast, hmn, nestingLevel, fieldOffset);
	}
  )*
  )? RPAR //fine campi della classe
  CLPAR {Offset classMemberOffset = new Offset(ClassNode.indexStart);} // body {
  	(
  	// inizio metodi della classe
  	FUN function=ID COLON functionReturnType=basic
  	{
		  MethodNode method = FOOLlib.factory.createMethodInClass($function.line, classNode, $function.text, $functionReturnType.ast, symTable, hmn, nestingLevel, fieldOffset);
          	// creo un nuovo nesting level per la funzione
          	nestingLevel++;
   	}
  	// inizio parametri del metodo
  	LPAR {
  		Offset parOffset= new Offset(1);
  		ArrayList<Node> parTypes = new ArrayList<Node>();

  		// il primo argomento di una funzione � sempre un id type
  		IdType idType = new IdType($className.text);
  		parTypes.add(idType);

        	FOOLlib.factory.addParamInFunction(-1, method, "__this___", idType, nestingLevel, parOffset);
  	}
  	(parameter=ID COLON parameterType=type
  	  { parTypes.add($parameterType.ast);
  	  	FOOLlib.factory.addParamInFunction($parameter.line, method, $parameter.text, $parameterType.ast, nestingLevel, parOffset);
      }
      (COMMA otherParameter=ID COLON otherParameterType=type
  	  { parTypes.add($otherParameterType.ast);
        FOOLlib.factory.addParamInFunction($otherParameter.line, method, $otherParameter.text, $otherParameterType.ast, nestingLevel, parOffset);
      })* // fine parametri del metodo... inizio gestione del corpo del metodo
  	)? RPAR {
  		method.getSTEntry().setType(new ArrowTypeNode(parTypes, $functionReturnType.ast));
  		ArrayList<Node> decList = new ArrayList<Node>();
  		Offset varOffset= new Offset(-2);
  		}
	  (LET
		  (
        VAR varId=ID COLON varType=basic ASS varValue=exp SEMIC
        {decList.add(FOOLlib.factory.addVarInFunction($varId.line, method, $varId.text, $varType.ast, $varValue.ast, nestingLevel, varOffset)); }
      )*
	  IN )? expression=exp SEMIC
	  {	//chiudere scope
	  	symTable.remove(nestingLevel--); // rimuovo il nl della funzione
	  	method.addDecBody(decList,$expression.ast);
	    classNode.addMethod(method);
	  }
	  )*
   // fine metodi della classe
  CRPAR { symTable.remove(nestingLevel--); /* rimuovo nl della classe */ }
  )* // posso avere una o pi� classi
  ;
// }}}

declist	returns [ArrayList<Node> astlist] // declist {{{
	: {$astlist= new ArrayList<Node>(); Offset varOffset = new Offset(-2);}
	  ( VAR i=ID COLON t=type ASS e=exp SEMIC
      {$astlist.add(FOOLlib.factory.createVariable($i.line, $i.text, $t.ast, $e.ast, symTable, nestingLevel, varOffset));}
      | FUN i=ID COLON t=type
        { FunNode f = FOOLlib.factory.createFunction($i.line, $i.text, $t.ast, symTable, nestingLevel, varOffset);
        	$astlist.add(f);
        	nestingLevel++;
        }
        LPAR
        { ArrayList<Node> parTypes = new ArrayList<Node>();
  		    Offset parOffset = new Offset(1);
        }
        ( fid=ID COLON fty=type
          { parTypes.add($fty.ast);
        	  FOOLlib.factory.addParamInFunction($fid.line, f, $fid.text, $fty.ast, nestingLevel, parOffset);
          }
          ( COMMA id=ID COLON ty=type // Ripeto sopra per klenee
            { parTypes.add($ty.ast);
              FOOLlib.factory.addParamInFunction($id.line, f, $id.text, $ty.ast, nestingLevel, parOffset);
            }
          )*
        )?
        RPAR  {	f.getSTEntry().setType(new ArrowTypeNode(parTypes, $t.ast));
        	      ArrayList<Node> decList = new ArrayList<Node>();}
        (LET d=declist IN {decList=$d.astlist;})? e=exp SEMIC
        	{	//chiudere scope
        		symTable.remove(nestingLevel--);
        		f.addDecBody(decList,$e.ast);
        	}
    )*;
// }}}

exp	returns [Node ast] // + - || {{{
 	: f=term {$ast= $f.ast;}
 	  (   PLUS  l=term {$ast= new AddNode($ast,$l.ast);}
 	    | MINUS l=term {$ast= new SubNode($ast,$l.ast);}
      | OR    l=term {$ast= new OrNode($ast,$l.ast);}
 	  )*
 	;
// }}}

term	returns [Node ast] // * / && {{{
	: f=factor {$ast= $f.ast;}
	  (   MULT l=factor {$ast= new MultNode($ast,$l.ast);}
      | DIV  l=factor {$ast= new DivNode($ast,$l.ast);}
  	  | AND  l=factor {$ast= new AndNode($ast,$l.ast);}
	  )*
	;
// }}}

factor	returns [Node ast] // == >= <= > < {{{
	: f=value {$ast= $f.ast;}
	  (   EQ l=value {$ast= new EqualNode ($ast,$l.ast);}
	    | GR l=value {$ast= new GreaterEqualNode ($ast,$l.ast);}
	    | LE l=value {$ast= new LessEqualNode ($ast,$l.ast);}
	    | GREAT l=value {$ast= new GreatNode ($ast,$l.ast);}
	    | LESS l=value {$ast= new LessNode ($ast,$l.ast);}
    )*
 	;
// }}}

value	returns [Node ast] // value {{{
	: n=INTEGER
	  { 	Integer integer = Integer.parseInt($n.text);
	  	if (integer == 0) $ast = new BoolNode(false);
	  	else if (integer == 1) $ast = new BoolNode(true);
	  	else $ast = new NatNode(integer);}
	| TRUE
	  {$ast = new BoolNode(true);}
	| FALSE
	  {$ast = new BoolNode(false);}
	| LPAR e = exp RPAR
	  {$ast= $e.ast;}
	| IF x=exp THEN CLPAR y=exp CRPAR
		(ELSE CLPAR z=exp CRPAR)?
	  {$ast= new IfNode($x.ast,$y.ast,$z.ast);}
	| PRINT LPAR e=exp RPAR
	  {$ast= new PrintNode($e.ast);}
	| i=ID
		{ //cercare la dichiarazione
			STentry entry = null;
			int j = nestingLevel;

			while (j >= 0 && entry == null)
				entry = symTable.get(j--).get($i.text);

		  $ast = FOOLlib.factory.checkId($i.line, $i.text, entry, nestingLevel - j - 1);
	  }
    ( LPAR { ArrayList<Node> argList = new ArrayList<Node>(); }
		  (fa=exp {argList.add($fa.ast);} (COMMA a=exp {argList.add($a.ast);})* )?
		  { $ast = FOOLlib.factory.checkCall($i.text, argList, entry, nestingLevel-j-1); }
	    RPAR
		  | DOT identifier=ID { $ast = FOOLlib.factory.checkObjectField($i.line, $i.text, $identifier.text, entry, nestingLevel - j - 1); }
			(LPAR
      	{ ArrayList<Node> argList = new ArrayList<Node>();
					// stiamo chiamando il metodo di un field
					// class Integer(i: int) {
					// fun getInt: int() i;
					// }
					// class A(i: Integer) {
					// fun getInt: int() i.getInt();
					// }
					// Devo recuperare il riferimento a i (field)
				  if(entry.isInClass())
      		  argList.add(Syskb.getClass(entry.getClassName()).getFieldNode($i.text));
      		else
      		  argList.add(new IdNode($i.text,entry,nestingLevel-j-1));
      		// Non sono dentro alla classe
					// class Integer(i: int) {
					// fun getInt: int() i;
					// }
					// print(i.getInt());
					// devo recuperare il riferimento a i
			  }
			(firstArgument=exp {argList.add($firstArgument.ast);} (COMMA otherArgument=exp {argList.add($otherArgument.ast);})* )?
			{ // invoca il metodo della classe (classMethodNode)
			  $ast = ((ClassNode) entry.getType().typeCheck()).callMethod($identifier.text, entry, argList, nestingLevel - j - 1);
			}
			RPAR)?
			{ if ($ast == null) {
      	 	FOOLlib.exit($i.text+"."+$identifier.text+" @ line "+$identifier.line+", "+$identifier.text+" not declared");
      	}
			}
	   )?
  | NULL {$ast = new NullNode();}
	| {ArrayList<Node> argList = new ArrayList<Node>();}
		NEW className=ID
    LPAR (paramater=exp {argList.add($paramater.ast);} (COMMA otherParameter=exp {argList.add($otherParameter.ast);})* )?
		  {$ast=new ObjectNode($className.text,argList);}
    RPAR
	| NOT LPAR e=exp RPAR {$ast= new NotNode($e.ast);}
 	;
// }}}

type returns [Node ast] // basic arrow {{{
  : typ=basic {$ast=$typ.ast;}
  | arw=arrow {$ast=$arw.ast;}
  ;
// }}}

basic	returns [Node ast] // int bool id  {{{
  : INT  {$ast=new IntTypeNode();}
  | BOOL {$ast=new BoolTypeNode();}
  | i=ID {$ast=new IdType($i.text);}
  ;
// }}}

arrow returns [Node ast] // -> {{{
  : LPAR { ArrayList<Node> parTypes = new ArrayList<Node>(); }
  		(fty=type { parTypes.add($fty.ast); } (COMMA ty=type { parTypes.add($ty.ast); })*)?
  	RPAR ARROW returnType=basic { $ast = new ArrowTypeNode(parTypes , $returnType.ast);};
// }}}

// }}}

// * LEXER RULES */ {{{

PLUS    : '+' ;
MINUS   : '-' ;
MULT    : '*' ;
DIV     : '/' ;
LPAR    : '(' ;
RPAR    : ')' ;
CLPAR   : '{' ;
CRPAR   : '}' ;
SEMIC   : ';' ;
COLON   : ':' ;
COMMA   : ',' ;
DOT     : '.' ;
OR      : '||';
AND     : '&&';
NOT     : 'not' ;
GR      : '>=' ;
GREAT   : '>' ;
LE      : '<=' ;
LESS    : '<' ;
EQ      : '==' ;
ASS     : '=' ;
TRUE    : 'true' ;
FALSE   : 'false' ;
IF      : 'if' ;
THEN    : 'then';
ELSE    : 'else' ;
PRINT   : 'print' ;
LET     : 'let' ;
IN      : 'in' ;
VAR     : 'var' ;
FUN     : 'fun' ;
CLASS   : 'class' ;
EXTENDS : 'extends' ;
NEW     : 'new' ;
NULL    : 'null' ;
INT     : 'int' ;
BOOL    : 'bool' ;
ARROW   : '->' ;
INTEGER : (('1'..'9')('0'..'9')*) | '0' ;
ID      : ('a'..'z'|'A'..'Z')('a'..'z' | 'A'..'Z' | '0'..'9')* ;
WHITESP : ( '\t' | ' ' | '\r' | '\n' )+    { skip(); } ;

COMMENT : '/*' (options {greedy=false;} : .)* '*/' { skip(); } ;
ICOMMENT: '//' (options {greedy=false;} : .)* '\r'? '\n' { skip(); } ;
ERR   	: . {System.err.println("Invalid char: " + $text);};
// }}}
