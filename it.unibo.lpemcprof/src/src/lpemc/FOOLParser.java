package src.lpemc;
// $ANTLR 3.2 Sep 23, 2009 12:02:23 /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g 2015-01-08 13:48:13

import java.util.ArrayList;
import java.util.HashMap;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class FOOLParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "SEMIC", "LET", "IN", "VAR", "ID", "COLON", "ASS", "FUN", "LPAR", "COMMA", "RPAR", "INT", "BOOL", "PLUS", "TIMES", "EQ", "NAT", "TRUE", "FALSE", "IF", "THEN", "CLPAR", "CRPAR", "ELSE", "PRINT", "WHITESP", "COMMENT", "ERR"
    };
    public static final int ELSE=27;
    public static final int BOOL=16;
    public static final int CRPAR=26;
    public static final int INT=15;
    public static final int NAT=20;
    public static final int ID=8;
    public static final int WHITESP=29;
    public static final int EOF=-1;
    public static final int ERR=31;
    public static final int TRUE=21;
    public static final int PRINT=28;
    public static final int IF=23;
    public static final int COLON=9;
    public static final int THEN=24;
    public static final int IN=6;
    public static final int LPAR=12;
    public static final int CLPAR=25;
    public static final int COMMA=13;
    public static final int RPAR=14;
    public static final int PLUS=17;
    public static final int VAR=7;
    public static final int ASS=10;
    public static final int EQ=19;
    public static final int COMMENT=30;
    public static final int SEMIC=4;
    public static final int FUN=11;
    public static final int TIMES=18;
    public static final int FALSE=22;
    public static final int LET=5;

    // delegates
    // delegators


        public FOOLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public FOOLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return FOOLParser.tokenNames; }
    public String getGrammarFileName() { return "/Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g"; }


    private ArrayList<HashMap<String,STentry>>  symTable = new ArrayList<HashMap<String,STentry>>();
    private int nestingLevel = -1;



    // $ANTLR start "prog"
    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:17:1: prog returns [Node ast] : (e= exp SEMIC | LET d= dec IN e= exp SEMIC );
    public final Node prog() throws RecognitionException {
        Node ast = null;

        Node e = null;

        ArrayList<Node> d = null;


        try {
            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:18:2: (e= exp SEMIC | LET d= dec IN e= exp SEMIC )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==ID||LA1_0==LPAR||(LA1_0>=NAT && LA1_0<=IF)||LA1_0==PRINT) ) {
                alt1=1;
            }
            else if ( (LA1_0==LET) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:18:4: e= exp SEMIC
                    {
                    pushFollow(FOLLOW_exp_in_prog30);
                    e=exp();

                    state._fsp--;

                    match(input,SEMIC,FOLLOW_SEMIC_in_prog32); 
                    ast = new ProgNode(e);

                    }
                    break;
                case 2 :
                    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:20:11: LET d= dec IN e= exp SEMIC
                    {
                    match(input,LET,FOLLOW_LET_in_prog57); 
                    nestingLevel++;
                                 HashMap<String,STentry> hm = new HashMap<String,STentry> ();
                                 symTable.add(hm);
                                
                    pushFollow(FOLLOW_dec_in_prog86);
                    d=dec();

                    state._fsp--;

                    match(input,IN,FOLLOW_IN_in_prog88); 
                    pushFollow(FOLLOW_exp_in_prog92);
                    e=exp();

                    state._fsp--;

                    match(input,SEMIC,FOLLOW_SEMIC_in_prog94); 
                    symTable.remove(nestingLevel--);
                                 ast = new LetInNode(d,e) ;

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ast;
    }
    // $ANTLR end "prog"


    // $ANTLR start "dec"
    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:30:1: dec returns [ArrayList<Node> astlist] : ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )* ;
    public final ArrayList<Node> dec() throws RecognitionException {
        ArrayList<Node> astlist = null;

        Token i=null;
        Token fid=null;
        Token id=null;
        Node t = null;

        Node e = null;

        Node fty = null;

        Node ty = null;

        ArrayList<Node> d = null;


        try {
            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:31:2: ( ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )* )
            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:31:4: ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )*
            {
            astlist = new ArrayList<Node>() ;
            	   int offset=-2;
            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:33:4: ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )*
            loop5:
            do {
                int alt5=3;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==VAR) ) {
                    alt5=1;
                }
                else if ( (LA5_0==FUN) ) {
                    alt5=2;
                }


                switch (alt5) {
            	case 1 :
            	    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:34:13: VAR i= ID COLON t= type ASS e= exp SEMIC
            	    {
            	    match(input,VAR,FOLLOW_VAR_in_dec144); 
            	    i=(Token)match(input,ID,FOLLOW_ID_in_dec148); 
            	    match(input,COLON,FOLLOW_COLON_in_dec150); 
            	    pushFollow(FOLLOW_type_in_dec154);
            	    t=type();

            	    state._fsp--;

            	    match(input,ASS,FOLLOW_ASS_in_dec156); 
            	    pushFollow(FOLLOW_exp_in_dec160);
            	    e=exp();

            	    state._fsp--;

            	    match(input,SEMIC,FOLLOW_SEMIC_in_dec162); 
            	    VarNode v = new VarNode((i!=null?i.getText():null),t,e);
            	                 astlist.add(v);
            	                 HashMap<String,STentry> hm = symTable.get(nestingLevel);
            	                 if ( hm.put((i!=null?i.getText():null),new STentry(v,nestingLevel,t,offset--)) != null  )
            	                 {System.out.println("Var id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared");
            	                  System.exit(0);}  
            	                

            	    }
            	    break;
            	case 2 :
            	    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:43:13: FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC
            	    {
            	    match(input,FUN,FOLLOW_FUN_in_dec205); 
            	    i=(Token)match(input,ID,FOLLOW_ID_in_dec209); 
            	    match(input,COLON,FOLLOW_COLON_in_dec211); 
            	    pushFollow(FOLLOW_type_in_dec215);
            	    t=type();

            	    state._fsp--;

            	    //inserimento di ID nella symtable
            	                   FunNode f = new FunNode((i!=null?i.getText():null),t);
            	                   astlist.add(f);
            	                   HashMap<String,STentry> hm = symTable.get(nestingLevel);
            	                   STentry entry = new STentry(f,nestingLevel,offset--);
            	                   if ( hm.put((i!=null?i.getText():null),entry) != null  )
            	                   {System.out.println("Fun id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared");
            	                    System.exit(0);}
            	                    //creare una nuova hashmap per la symTable
            	                    nestingLevel++;
            	                    HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
            	                    symTable.add(hmn);
            	                    
            	    match(input,LPAR,FOLLOW_LPAR_in_dec247); 
            	    ArrayList<Node> parTypes = new ArrayList<Node>();
            	                        int paroffset=1;
            	    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:59:17: (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )?
            	    int alt3=2;
            	    int LA3_0 = input.LA(1);

            	    if ( (LA3_0==ID) ) {
            	        alt3=1;
            	    }
            	    switch (alt3) {
            	        case 1 :
            	            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:59:18: fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )*
            	            {
            	            fid=(Token)match(input,ID,FOLLOW_ID_in_dec270); 
            	            match(input,COLON,FOLLOW_COLON_in_dec272); 
            	            pushFollow(FOLLOW_type_in_dec276);
            	            fty=type();

            	            state._fsp--;


            	                              parTypes.add(fty);
            	                              ParNode fpar = new ParNode((fid!=null?fid.getText():null),fty);
            	                              f.addPar(fpar);
            	                              if ( hmn.put((fid!=null?fid.getText():null),new STentry(fpar,nestingLevel,fty,paroffset++)) != null  )
            	                              {System.out.println("Parameter id "+(fid!=null?fid.getText():null)+" at line "+(fid!=null?fid.getLine():0)+" already declared");
            	                               System.exit(0);}
            	                              
            	            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:68:19: ( COMMA id= ID COLON ty= type )*
            	            loop2:
            	            do {
            	                int alt2=2;
            	                int LA2_0 = input.LA(1);

            	                if ( (LA2_0==COMMA) ) {
            	                    alt2=1;
            	                }


            	                switch (alt2) {
            	            	case 1 :
            	            	    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:68:20: COMMA id= ID COLON ty= type
            	            	    {
            	            	    match(input,COMMA,FOLLOW_COMMA_in_dec317); 
            	            	    id=(Token)match(input,ID,FOLLOW_ID_in_dec321); 
            	            	    match(input,COLON,FOLLOW_COLON_in_dec323); 
            	            	    pushFollow(FOLLOW_type_in_dec327);
            	            	    ty=type();

            	            	    state._fsp--;


            	            	                        parTypes.add(ty);
            	            	                        ParNode par = new ParNode((id!=null?id.getText():null),ty);
            	            	                        f.addPar(par);
            	            	                        if ( hmn.put((id!=null?id.getText():null),new STentry(par,nestingLevel,ty,paroffset++)) != null  )
            	            	                        {System.out.println("Parameter id "+(id!=null?id.getText():null)+" at line "+(id!=null?id.getLine():0)+" already declared");
            	            	                         System.exit(0);}
            	            	                        

            	            	    }
            	            	    break;

            	            	default :
            	            	    break loop2;
            	                }
            	            } while (true);


            	            }
            	            break;

            	    }

            	    match(input,RPAR,FOLLOW_RPAR_in_dec406); 
            	    entry.addType( new ArrowTypeNode(parTypes , t) );
            	                        ArrayList<Node> localDec = new ArrayList<Node> ();
            	    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:81:15: ( LET d= dec IN )?
            	    int alt4=2;
            	    int LA4_0 = input.LA(1);

            	    if ( (LA4_0==LET) ) {
            	        alt4=1;
            	    }
            	    switch (alt4) {
            	        case 1 :
            	            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:81:16: LET d= dec IN
            	            {
            	            match(input,LET,FOLLOW_LET_in_dec425); 
            	            pushFollow(FOLLOW_dec_in_dec429);
            	            d=dec();

            	            state._fsp--;

            	            match(input,IN,FOLLOW_IN_in_dec431); 
            	            localDec=d;

            	            }
            	            break;

            	    }

            	    pushFollow(FOLLOW_exp_in_dec440);
            	    e=exp();

            	    state._fsp--;

            	    match(input,SEMIC,FOLLOW_SEMIC_in_dec442); 
            	    //chiudere scope
            	                   symTable.remove(nestingLevel--);
            	                   f.addDecBody(localDec,e);
            	                  

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return astlist;
    }
    // $ANTLR end "dec"


    // $ANTLR start "type"
    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:89:1: type returns [Node ast] : ( INT | BOOL );
    public final Node type() throws RecognitionException {
        Node ast = null;

        try {
            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:90:9: ( INT | BOOL )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==INT) ) {
                alt6=1;
            }
            else if ( (LA6_0==BOOL) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:90:11: INT
                    {
                    match(input,INT,FOLLOW_INT_in_type504); 
                    ast =new IntTypeNode();

                    }
                    break;
                case 2 :
                    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:91:11: BOOL
                    {
                    match(input,BOOL,FOLLOW_BOOL_in_type519); 
                    ast =new BoolTypeNode();

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ast;
    }
    // $ANTLR end "type"


    // $ANTLR start "exp"
    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:94:1: exp returns [Node ast] : f= term ( PLUS l= term )* ;
    public final Node exp() throws RecognitionException {
        Node ast = null;

        Node f = null;

        Node l = null;


        try {
            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:95:3: (f= term ( PLUS l= term )* )
            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:95:5: f= term ( PLUS l= term )*
            {
            pushFollow(FOLLOW_term_in_exp544);
            f=term();

            state._fsp--;

            ast = f;
            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:96:7: ( PLUS l= term )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==PLUS) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:96:8: PLUS l= term
            	    {
            	    match(input,PLUS,FOLLOW_PLUS_in_exp555); 
            	    pushFollow(FOLLOW_term_in_exp559);
            	    l=term();

            	    state._fsp--;

            	    ast = new PlusNode (ast,l);

            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ast;
    }
    // $ANTLR end "exp"


    // $ANTLR start "term"
    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:101:1: term returns [Node ast] : f= value ( TIMES l= value )* ;
    public final Node term() throws RecognitionException {
        Node ast = null;

        Node f = null;

        Node l = null;


        try {
            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:102:2: (f= value ( TIMES l= value )* )
            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:102:4: f= value ( TIMES l= value )*
            {
            pushFollow(FOLLOW_value_in_term597);
            f=value();

            state._fsp--;

            ast = f;
            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:103:6: ( TIMES l= value )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==TIMES) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:103:7: TIMES l= value
            	    {
            	    match(input,TIMES,FOLLOW_TIMES_in_term607); 
            	    pushFollow(FOLLOW_value_in_term611);
            	    l=value();

            	    state._fsp--;

            	    ast = new MultNode (ast,l);

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ast;
    }
    // $ANTLR end "term"


    // $ANTLR start "value"
    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:108:1: value returns [Node ast] : f= fatt ( EQ l= fatt )* ;
    public final Node value() throws RecognitionException {
        Node ast = null;

        Node f = null;

        Node l = null;


        try {
            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:109:2: (f= fatt ( EQ l= fatt )* )
            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:109:4: f= fatt ( EQ l= fatt )*
            {
            pushFollow(FOLLOW_fatt_in_value645);
            f=fatt();

            state._fsp--;

            ast = f;
            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:110:6: ( EQ l= fatt )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==EQ) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:110:7: EQ l= fatt
            	    {
            	    match(input,EQ,FOLLOW_EQ_in_value655); 
            	    pushFollow(FOLLOW_fatt_in_value659);
            	    l=fatt();

            	    state._fsp--;

            	    ast = new EqualNode (ast,l);

            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ast;
    }
    // $ANTLR end "value"


    // $ANTLR start "fatt"
    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:115:1: fatt returns [Node ast] : (n= NAT | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | PRINT LPAR e= exp RPAR | i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )? );
    public final Node fatt() throws RecognitionException {
        Node ast = null;

        Token n=null;
        Token i=null;
        Node e = null;

        Node x = null;

        Node y = null;

        Node z = null;

        Node fa = null;

        Node a = null;


        try {
            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:116:2: (n= NAT | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | PRINT LPAR e= exp RPAR | i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )? )
            int alt13=7;
            switch ( input.LA(1) ) {
            case NAT:
                {
                alt13=1;
                }
                break;
            case TRUE:
                {
                alt13=2;
                }
                break;
            case FALSE:
                {
                alt13=3;
                }
                break;
            case LPAR:
                {
                alt13=4;
                }
                break;
            case IF:
                {
                alt13=5;
                }
                break;
            case PRINT:
                {
                alt13=6;
                }
                break;
            case ID:
                {
                alt13=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:116:4: n= NAT
                    {
                    n=(Token)match(input,NAT,FOLLOW_NAT_in_fatt699); 
                    ast = new NatNode(Integer.parseInt((n!=null?n.getText():null)));

                    }
                    break;
                case 2 :
                    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:118:4: TRUE
                    {
                    match(input,TRUE,FOLLOW_TRUE_in_fatt714); 
                    ast = new BoolNode(true);

                    }
                    break;
                case 3 :
                    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:120:4: FALSE
                    {
                    match(input,FALSE,FOLLOW_FALSE_in_fatt727); 
                    ast = new BoolNode(false);

                    }
                    break;
                case 4 :
                    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:122:4: LPAR e= exp RPAR
                    {
                    match(input,LPAR,FOLLOW_LPAR_in_fatt739); 
                    pushFollow(FOLLOW_exp_in_fatt743);
                    e=exp();

                    state._fsp--;

                    match(input,RPAR,FOLLOW_RPAR_in_fatt745); 
                    ast = e;

                    }
                    break;
                case 5 :
                    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:124:4: IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR
                    {
                    match(input,IF,FOLLOW_IF_in_fatt757); 
                    pushFollow(FOLLOW_exp_in_fatt761);
                    x=exp();

                    state._fsp--;

                    match(input,THEN,FOLLOW_THEN_in_fatt763); 
                    match(input,CLPAR,FOLLOW_CLPAR_in_fatt765); 
                    pushFollow(FOLLOW_exp_in_fatt769);
                    y=exp();

                    state._fsp--;

                    match(input,CRPAR,FOLLOW_CRPAR_in_fatt771); 
                    match(input,ELSE,FOLLOW_ELSE_in_fatt779); 
                    match(input,CLPAR,FOLLOW_CLPAR_in_fatt781); 
                    pushFollow(FOLLOW_exp_in_fatt785);
                    z=exp();

                    state._fsp--;

                    match(input,CRPAR,FOLLOW_CRPAR_in_fatt787); 
                    ast = new IfNode(x,y,z);

                    }
                    break;
                case 6 :
                    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:127:4: PRINT LPAR e= exp RPAR
                    {
                    match(input,PRINT,FOLLOW_PRINT_in_fatt800); 
                    match(input,LPAR,FOLLOW_LPAR_in_fatt802); 
                    pushFollow(FOLLOW_exp_in_fatt806);
                    e=exp();

                    state._fsp--;

                    match(input,RPAR,FOLLOW_RPAR_in_fatt808); 
                    ast = new PrintNode(e);

                    }
                    break;
                case 7 :
                    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:129:4: i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )?
                    {
                    i=(Token)match(input,ID,FOLLOW_ID_in_fatt821); 
                    //cercare la dichiarazione
                               int j=nestingLevel;
                               STentry entry=null; 
                               while (j>=0 && entry==null)
                                 entry=(symTable.get(j--)).get((i!=null?i.getText():null));
                               if (entry==null)
                               {System.out.println("Id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" not declared");
                                System.exit(0);}               
                    	   ast = new IdNode((i!=null?i.getText():null),entry,nestingLevel-(j+1));
                    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:139:5: ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )?
                    int alt12=2;
                    int LA12_0 = input.LA(1);

                    if ( (LA12_0==LPAR) ) {
                        alt12=1;
                    }
                    switch (alt12) {
                        case 1 :
                            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:139:6: LPAR (fa= exp ( COMMA a= exp )* )? RPAR
                            {
                            match(input,LPAR,FOLLOW_LPAR_in_fatt836); 
                            ArrayList<Node> argList = new ArrayList<Node>();
                            // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:141:8: (fa= exp ( COMMA a= exp )* )?
                            int alt11=2;
                            int LA11_0 = input.LA(1);

                            if ( (LA11_0==ID||LA11_0==LPAR||(LA11_0>=NAT && LA11_0<=IF)||LA11_0==PRINT) ) {
                                alt11=1;
                            }
                            switch (alt11) {
                                case 1 :
                                    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:141:9: fa= exp ( COMMA a= exp )*
                                    {
                                    pushFollow(FOLLOW_exp_in_fatt856);
                                    fa=exp();

                                    state._fsp--;

                                    argList.add(fa);
                                    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:142:9: ( COMMA a= exp )*
                                    loop10:
                                    do {
                                        int alt10=2;
                                        int LA10_0 = input.LA(1);

                                        if ( (LA10_0==COMMA) ) {
                                            alt10=1;
                                        }


                                        switch (alt10) {
                                    	case 1 :
                                    	    // /Volumes/LPeMC/MaterialeDidattico/Esercitazione_12_19/FOOL/FOOL.g:142:10: COMMA a= exp
                                    	    {
                                    	    match(input,COMMA,FOLLOW_COMMA_in_fatt869); 
                                    	    pushFollow(FOLLOW_exp_in_fatt873);
                                    	    a=exp();

                                    	    state._fsp--;

                                    	    argList.add(a);

                                    	    }
                                    	    break;

                                    	default :
                                    	    break loop10;
                                        }
                                    } while (true);


                                    }
                                    break;

                            }

                            ast =new CallNode((i!=null?i.getText():null),entry,argList,nestingLevel-(j+1));
                            match(input,RPAR,FOLLOW_RPAR_in_fatt895); 

                            }
                            break;

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ast;
    }
    // $ANTLR end "fatt"

    // Delegated rules


 

    public static final BitSet FOLLOW_exp_in_prog30 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_SEMIC_in_prog32 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LET_in_prog57 = new BitSet(new long[]{0x00000000000008C0L});
    public static final BitSet FOLLOW_dec_in_prog86 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_IN_in_prog88 = new BitSet(new long[]{0x0000000010F01100L});
    public static final BitSet FOLLOW_exp_in_prog92 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_SEMIC_in_prog94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VAR_in_dec144 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_ID_in_dec148 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_COLON_in_dec150 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_type_in_dec154 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_ASS_in_dec156 = new BitSet(new long[]{0x0000000010F01100L});
    public static final BitSet FOLLOW_exp_in_dec160 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_SEMIC_in_dec162 = new BitSet(new long[]{0x0000000000000882L});
    public static final BitSet FOLLOW_FUN_in_dec205 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_ID_in_dec209 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_COLON_in_dec211 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_type_in_dec215 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_LPAR_in_dec247 = new BitSet(new long[]{0x0000000000004100L});
    public static final BitSet FOLLOW_ID_in_dec270 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_COLON_in_dec272 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_type_in_dec276 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_COMMA_in_dec317 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_ID_in_dec321 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_COLON_in_dec323 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_type_in_dec327 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_RPAR_in_dec406 = new BitSet(new long[]{0x0000000010F01120L});
    public static final BitSet FOLLOW_LET_in_dec425 = new BitSet(new long[]{0x00000000000008C0L});
    public static final BitSet FOLLOW_dec_in_dec429 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_IN_in_dec431 = new BitSet(new long[]{0x0000000010F01100L});
    public static final BitSet FOLLOW_exp_in_dec440 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_SEMIC_in_dec442 = new BitSet(new long[]{0x0000000000000882L});
    public static final BitSet FOLLOW_INT_in_type504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_BOOL_in_type519 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_term_in_exp544 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_PLUS_in_exp555 = new BitSet(new long[]{0x0000000010F01100L});
    public static final BitSet FOLLOW_term_in_exp559 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_value_in_term597 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_TIMES_in_term607 = new BitSet(new long[]{0x0000000010F01100L});
    public static final BitSet FOLLOW_value_in_term611 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_fatt_in_value645 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_EQ_in_value655 = new BitSet(new long[]{0x0000000010F01100L});
    public static final BitSet FOLLOW_fatt_in_value659 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_NAT_in_fatt699 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TRUE_in_fatt714 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FALSE_in_fatt727 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LPAR_in_fatt739 = new BitSet(new long[]{0x0000000010F01100L});
    public static final BitSet FOLLOW_exp_in_fatt743 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_RPAR_in_fatt745 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IF_in_fatt757 = new BitSet(new long[]{0x0000000010F01100L});
    public static final BitSet FOLLOW_exp_in_fatt761 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_THEN_in_fatt763 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_CLPAR_in_fatt765 = new BitSet(new long[]{0x0000000010F01100L});
    public static final BitSet FOLLOW_exp_in_fatt769 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_CRPAR_in_fatt771 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_ELSE_in_fatt779 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_CLPAR_in_fatt781 = new BitSet(new long[]{0x0000000010F01100L});
    public static final BitSet FOLLOW_exp_in_fatt785 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_CRPAR_in_fatt787 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PRINT_in_fatt800 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_LPAR_in_fatt802 = new BitSet(new long[]{0x0000000010F01100L});
    public static final BitSet FOLLOW_exp_in_fatt806 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_RPAR_in_fatt808 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_fatt821 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_LPAR_in_fatt836 = new BitSet(new long[]{0x0000000010F05100L});
    public static final BitSet FOLLOW_exp_in_fatt856 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_COMMA_in_fatt869 = new BitSet(new long[]{0x0000000010F01100L});
    public static final BitSet FOLLOW_exp_in_fatt873 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_RPAR_in_fatt895 = new BitSet(new long[]{0x0000000000000002L});

}