package src.lpemc;
import java.util.ArrayList;

public class FunNode extends Node {

  private String id;
  private Node type; 
  private ArrayList<Node> par; 
  private ArrayList<Node> def; 
  private Node body;
  
  public FunNode (String i, Node t) {
   id=i;
   type=t;
   par = new ArrayList<Node>();
  }
  
  public void addDecBody (ArrayList<Node> d, Node b) {
   def=d;
   body=b;
  }  
  
  public void addPar (Node p) {
   par.add(p);
  }  
  
  public String codeGeneration() {
    
    String funl=FOOLlib.freshLabel();
    
    String decl="";
    for (int i=0; i<def.size(); i++)
      decl+=(def.get(i)).codeGeneration();
    
    String popDec="";
    for (int i=0; i<def.size(); i++)
      popDec+="pop\n";
    
    String popPar="";
    for (int i=0; i<par.size(); i++)
      popPar+="pop\n";
    
    FOOLlib.putCode(
    funl+":\n"+
    "cfp\n"+
    "lra\n"+
    decl+ //dichiarazioni locali
    body.codeGeneration()+                  
    "srv\n"+
    popDec+ //pop per dic locali
    "sra\n"+
    "pop\n"+
    popPar+ //pop per parametri                    
    "sfp\n"+
    "lrv\n"+
    "lra\n"+
    "js\n"
    );
    
    return "push "+funl+"\n";
  }
  
  public String toPrint(String s) {
     return "";
  }
  
  public Node typeCheck () {
    ArrayList<Node> pt = new ArrayList<Node>();
    for (int i=0; i<par.size(); i++) 
      pt.add( (par.get(i)).typeCheck() );
    for (int j=0; j<def.size(); j++) 
      (def.get(j)).typeCheck();
    if ( !(FOOLlib.isSubtype(body.typeCheck(),type)) ){
      System.out.println("Wrong return type for function "+id);
      System.exit(0);
    }  
    return new ArrowTypeNode (pt, type);
  }
    
}  