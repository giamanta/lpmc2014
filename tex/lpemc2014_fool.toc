\select@language {italian}
\contentsline {section}{\numberline {1}Specifiche}{3}
\contentsline {section}{\numberline {2}Stuttura del progetto}{4}
\contentsline {subsection}{\numberline {2.1}Interfacce: Everything is ... a Node}{4}
\contentsline {subsection}{\numberline {2.2}Tipi}{5}
\contentsline {subsection}{\numberline {2.3}Valori}{5}
\contentsline {subsection}{\numberline {2.4}Implementazione}{6}
\contentsline {section}{\numberline {3}Struttura Activation Record}{7}
\contentsline {section}{\numberline {4}Parte 1: Operatori}{8}
\contentsline {subsection}{\numberline {4.1}Struttura}{8}
\contentsline {section}{\numberline {5}Parte 2: Programmazione Funzionale}{9}
\contentsline {subsection}{\numberline {5.1}Covarianza e Controvarianza}{10}
\contentsline {subsection}{\numberline {5.2}Funzioni in variabili}{10}
\contentsline {section}{\numberline {6}Parte 3: Oggetti}{11}
\contentsline {subsection}{\numberline {6.1}Ereditariet\IeC {\`a} e varianza}{12}
\contentsline {subsection}{\numberline {6.2}Oggetti nested}{12}
\contentsline {subsection}{\numberline {6.3}Una piccola estensione}{12}
